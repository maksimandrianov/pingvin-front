import tornado.httpserver, tornado.escape, tornado.ioloop, tornado.web
import tornado.wsgi, os
from tornado.options import define, options, parse_command_line
from lib import BaseListHandler, BaseNewHandler, BaseDetailHandler, \
    BaseUpdateHandler, BaseDeleteHandler, BaseHandler
from static import get_index
from config import INDEX_NAME, IMG_PATH, STATIC_PATH, \
    STATIC_DIR, IS_DEBUG, BASE_PATH
from authvk import LoginVkHandler
from authfb import LoginFbHandler
from authgoogle import LoginGoogleHandler
from upload import UploadHandler, ResizeImgHandler
from auth import MeHandler, SignupHandler, ActivateHundler, \
    LoginHandler, LogoutHandler, ResetPasswordHundler, ConfirmPasswordHundler,\
    FakeConfirmPasswordHundler


define('port', default=8888, help='run on the given port', type=int)
define('host', default='0.0.0.0', help='run on the given host')
define('api', default='http://localhost:8800', help='api address')
define('my_url', default='http://localhost:8888', help='my address')
define('max_buffer_size', default=10*1024*1024, help='max buffer size', type=int)


class Admin:
    class PostsModerationHandler(BaseListHandler):
        api_url = '/admin-posts-moderation/'
        site_url = '/api/admin-posts-moderation-get/'

    class PostsClosedHandler(BaseListHandler):
        api_url = '/admin-posts-closed/'
        site_url = '/api/admin-posts-closed-get/'


class BlogPosts:
    class ListHandler(BaseListHandler, BaseNewHandler):
        api_url = '/blog-posts/'
        site_url = '/api/blog-posts-get/'

    class DeleteHandler(BaseDeleteHandler):
        url = '/blog-post/'

    class DetailHandler(BaseDetailHandler):
        url = '/blog-post/'

class Posts:
    class ListHandler(BaseListHandler, BaseNewHandler):
        api_url = '/posts/'
        site_url = '/api/posts-get/'

    class DetailHandler(BaseDetailHandler):
        url = '/post/'


class Events:
    class ListHandler(BaseListHandler, BaseNewHandler):
        api_url = '/events/'
        site_url = '/api/events-get/'

    class DetailHandler(BaseDetailHandler):
        url = '/event/'


class Users:
    class ListHandler(BaseListHandler):
        api_url = '/users/'
        site_url = '/api/users-get/'

    class DetailHandler(BaseDetailHandler, BaseUpdateHandler):
        url = '/user/'

    class MethodHandler(BaseHandler):
        api_url = '/api/user-method/'
        site_url = '/user/'

        async def post(self, id):
            try:
                response = await self.http_client.fetch(self.make_post(
                        self.request.uri.replace(self.api_url, self.site_url),
                        body=self.request.body)
                )
                self.write(response.body)
            except tornado.httpclient.HTTPError as e:
                self.write({'error': e.code})


class Replies:
    class ListHandler(BaseListHandler):
        api_url = '/replies/'
        site_url = '/api/replies-get/'

    class CountHandler(BaseDetailHandler):
        url = '/replies/count/'

    class MyPostsHandler(BaseListHandler):
        api_url = '/my-posts/'
        site_url = '/api/my-posts-get/'

    class MyHelpPostsHandler(BaseListHandler):
        api_url = '/my-help-posts/'
        site_url = '/api/my-help-posts-get/'


class UserSettingsHandler(BaseDetailHandler, BaseUpdateHandler):
    url = '/settings/'


class CommentUserHandler(BaseListHandler, BaseNewHandler):
    api_url = '/user-comments/'
    site_url = '/api/ucomments-get/'


class CommentPostHandler(BaseListHandler, BaseNewHandler):
    api_url = '/post-comments/'
    site_url = '/api/pcomments-get/'


class HelpersHandler(BaseListHandler):
    api_url = '/helpers/'
    site_url = '/api/helpers-get/'


class UsersSearchHandler(BaseListHandler):
    api_url = '/users/search/'
    site_url = '/api/users-search/'


class PostsSearchHandler(BaseListHandler):
    api_url = '/posts/search/'
    site_url = '/api/posts-search/'


class FeedbackHandler(BaseListHandler, BaseNewHandler):
    api_url = '/feedback/'
    site_url = '/api/feedback-get/'


class ErrorHandler(tornado.web.RequestHandler):
    def __init__(self,application, request,**kwargs):
        super(ErrorHandler,self).__init__(application, request)

    def write_error(self, status_code, **kwargs):
        self.set_status(status_code)
        error = 'Ошибка {}'.format(status_code)
        description = ''
        if status_code == 404:
            description = 'Страница не найдена'
        self.render('error-page.html', error=error, description=description)

    def prepare(self):
        raise tornado.web.HTTPError(404)


def main():
    parse_command_line()

    if not os.path.exists(IMG_PATH):
        os.makedirs(IMG_PATH)

    app = tornado.web.Application(
            [
                (r'^/$', get_index(INDEX_NAME)),
                (r'^/api/me/$', MeHandler),
                (r'^/api/signup/$', SignupHandler),
                (r'^/api/login/$', LoginHandler),
                (r'^/api/logout/$', LogoutHandler),
                (r'^/api/activate/(?P<uid>[\w-]+)/(?P<token>[\w-]+)/$', ActivateHundler),
                (r'^/api/password/reset/$', ResetPasswordHundler),
                (r'^/api/password/reset/confirm/(?P<uid>[\w-]+)/(?P<token>[\w-]+)/$', FakeConfirmPasswordHundler),
                (r'^/api/password/reset/confirm/$', ConfirmPasswordHundler),
                (r'^/api/posts-get/$', Posts.ListHandler),
                (r'^/api/post-new/$', Posts.ListHandler),
                (r'^/api/post-get/(?P<id>[0-9]+)/$', Posts.DetailHandler),
                (r'^/api/events-get/$', Events.ListHandler),
                (r'^/api/event-new/$', Events.ListHandler),
                (r'^/api/event-get/(?P<id>[0-9]+)/$', Events.DetailHandler),
                (r'^/api/users-get/$', Users.ListHandler),
                (r'^/api/settings-get/(?P<id>[0-9]+)/$', UserSettingsHandler),
                (r'^/api/settings-update/(?P<id>[0-9]+)/$', UserSettingsHandler),
                (r'^/api/user-get/(?P<id>[0-9]+)/$', Users.DetailHandler),
                (r'^/api/user-update/(?P<id>[0-9]+)/$', Users.DetailHandler),
                (r'^/api/ucomments-get/(?P<id>[0-9]+)/$', CommentUserHandler),
                (r'^/api/ucomment-new-finish-help/(?P<id>[0-9]+)/$', CommentUserHandler),
                (r'^/api/pcomments-get/(?P<id>[0-9]+)/$', CommentPostHandler),
                (r'^/api/pcomment-new/(?P<id>[0-9]+)/$', CommentPostHandler),
                (r'^/api/helpers-get/(?P<id>[0-9]+)/$', HelpersHandler),
                (r'^/api/replies/count/$', Replies.CountHandler),
                (r'^/api/replies-get/$', Replies.ListHandler),
                (r'^/api/my-help-posts-get/$', Replies.MyHelpPostsHandler),
                (r'^/api/my-posts-get/$', Replies.MyPostsHandler),
                (r'^/api/user-method/(.*)', Users.MethodHandler),
                (r'^/api/feedback-new/$',FeedbackHandler),
                (r'^/api/feedback-get/$',FeedbackHandler),
                (r'^/api/blog-posts-get/$', BlogPosts.ListHandler),
                (r'^/api/blog-posts-new/$', BlogPosts.ListHandler),
                (r'^/api/blog-post-del/(?P<id>[\w-]+)/$', BlogPosts.DeleteHandler),
                (r'^/api/blog-post-get/(?P<id>[\w-]+)/$', BlogPosts.DetailHandler),

                (r'^/api/posts-search/$', PostsSearchHandler),
                (r'^/api/users-search/$', UsersSearchHandler),

                (r'^/api/admin-posts-moderation-get/$', Admin.PostsModerationHandler),
                (r'^/api/admin-posts-closed-get/$', Admin.PostsClosedHandler),

                (r'^/social-login/gp/$', LoginGoogleHandler),
                (r'^/social-login/vk/$', LoginVkHandler),
                (r'^/social-login/fb/$', LoginFbHandler),
                (r'^/social-login/$', get_index('social-login-index.html')),

                (r'^/upload/$', UploadHandler),
                (r'^/img/(?P<context>[a-zA-Z]+)/(?P<detail>[a-zA-Z]+)/(?P<size>\w+)/(?P<name>[\w/.]+)', ResizeImgHandler),
                (r'^/img/(.*)', tornado.web.StaticFileHandler, {'path': os.path.join(STATIC_PATH, 'img')}),

                (r'^/posts/$', get_index(INDEX_NAME)),
                (r'^/users/$', get_index(INDEX_NAME)),
                (r'^/user/[0-9]+/$', get_index(INDEX_NAME)),
                (r'^/events/$', get_index(INDEX_NAME)),
                (r'^/about-me/$', get_index(INDEX_NAME)),
                (r'^/settings/$', get_index(INDEX_NAME)),
                (r'^/about-me-edit/$', get_index(INDEX_NAME)),
                (r'^/activity/$', get_index(INDEX_NAME)),
                (r'^/feedback/$', get_index(INDEX_NAME)),
                (r'^/feedback/$', get_index(INDEX_NAME)),
                (r'^/search/$', get_index(INDEX_NAME)),
                (r'^/help/$', get_index(INDEX_NAME)),
                (r'^/blog/$', get_index(INDEX_NAME)),
                (r'^/admin/$', get_index(INDEX_NAME)),
                (r'^/password/$', get_index(INDEX_NAME)),
                (r'^/privacy/$', get_index(INDEX_NAME)),
                (r'^/blog/new/$', get_index(INDEX_NAME)),
                (r'^/blog/[\w-]+/$', get_index(INDEX_NAME)),

            ],
            cookie_secret='__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__',
            static_path=STATIC_PATH,
            static_url_prefix='/'+STATIC_DIR+'/',
            template_path=os.path.join(BASE_PATH, 'templates'),
            xsrf_cookies=False,
            debug=IS_DEBUG,
            default_handler_class=ErrorHandler,
    )


    http_server = tornado.httpserver.HTTPServer(app, max_buffer_size=options.max_buffer_size)
    http_server.listen(options.port, address=options.host)
    print('***Run pingvin.online frontend is debug mode:', IS_DEBUG, '***', STATIC_PATH)
    tornado.ioloop.IOLoop.instance().start()



if __name__ == '__main__':
    main()
