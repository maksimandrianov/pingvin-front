import ujson as json
import tornado.httpclient as cl
from lib import BaseHandler, from_json, addr, rh_token


class MeHandler(BaseHandler):
    async def get(self, id=None):
        url = '/auth/me/'
        try:
            response = await self.http_client.fetch(self.make_get(url))
            self.write(response.body)
        except cl.HTTPError as e:
            if e.code == 401 and len(self.request.cookies):
                self.clear_all_cookies()
            self.write({'error': e.code})


class SignupHandler(BaseHandler):
    async def post(self):
        try:
            r_response = await self.http_client.fetch(
                    self.make_post('/auth/register/', body=self.request.body))
            self.write(r_response.body)
        except cl.HTTPError as e:
            self.write({'error': e.code})

class ActivateHundler(BaseHandler):
    async def get(self, uid=None, token=None):
        try:
            body = {'uid' : uid, 'token' : token}
            response = await self.http_client.fetch(
                    self.make_post( '/auth/activate/',
                                    json.dumps(body, ensure_ascii=False)))
            if response.code == 204:
                self.redirect('/posts/?login=activate')
            else:
                raise cl.HTTPError
        except cl.HTTPError as e:
            if e.code == 401 and len(self.request.cookies):
                self.clear_all_cookies()
            self.write({'error': e.code})


class ResetPasswordHundler(BaseHandler):
    async def post(self):
        try:
            response = await self.http_client.fetch(
                    self.make_post( '/auth/password/reset/',
                                    body=self.request.body))
            if response.code == 204:
                self.write({'code': response.code})
            else:
                raise cl.HTTPError
        except cl.HTTPError as e:
            if e.code == 401 and len(self.request.cookies):
                self.clear_all_cookies()
            self.write({'error': e.code})


class ConfirmPasswordHundler(BaseHandler):
    async def post(self):
        try:
            response = await self.http_client.fetch(
                    self.make_post( '/auth/password/reset/confirm/',
                                    body=self.request.body))
            if response.code == 204:
                self.write({'code': response.code})
            else:
                raise cl.HTTPError
        except cl.HTTPError as e:
            if e.code == 401 and len(self.request.cookies):
                self.clear_all_cookies()
            self.write({'error': e.code})


class FakeConfirmPasswordHundler(BaseHandler):
    async def get(self, uid=None, token=None):
            self.redirect('/password/?uid=' + str(uid) + '&token=' + str(token))


class LoginHandler(BaseHandler):
    async def post(self):
        try:
            l_response = await self.http_client.fetch(
                    self.make_post('/auth/login/', body=self.request.body))
            token = from_json(l_response.body)['auth_token']
            m_response = await self.http_client.fetch(
                    cl.HTTPRequest(addr('/auth/me/'), headers=rh_token(token)))
            self.set_cookie('token', token, expires_days=60)
            self.write(m_response.body)
        except cl.HTTPError as e:
            self.write({'error': e.code})


class LogoutHandler(BaseHandler):
    async def post(self):
        try:
            await self.http_client.fetch(self.make_post('/auth/logout/'))
            self.clear_all_cookies()
            self.write({})
        except cl.HTTPError as e:
            self.write({'error': e.code})