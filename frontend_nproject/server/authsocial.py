import json
import tornado.httpclient as cl
from lib import BaseHandler, from_json, addr, \
    rh_token, gen_pswd, addrm


class SocialHandler(BaseHandler):

    async def get(self):
        try:
            user = await self.qet_user()
            req = await self.qapi_social(user)
            if from_json(req.body)['status'] > 0:
                token = await self.qlogin(user['fake_email'])
                await self.qsave_token(token)
            else:
                self.write({'status': -1})
            self.write({'status': 1})
        except cl.HTTPError as e:
            print(e.code, e.response.body, dir(e))
            self.write({'error': e.code})

    async def qlogin(self, email):
        data = {'email': email,'password': gen_pswd(email)}
        body = json.dumps(data)
        response = await self.http_client.fetch(
                self.make_post('/auth/login/', body=body))
        return from_json(response.body)['auth_token']

    async def qsave_token(self, token):
        await self.http_client.fetch(
                cl.HTTPRequest(addr('/auth/me/'), headers=rh_token(token)))
        self.set_cookie('token', token, expires_days=60)

    async def qapi_social(self, user):
        header = {'Content-Type': 'application/json; charset=UTF-8'}
        request = cl.HTTPRequest(addr('/auth/social/'),
                                 method='POST', body=json.dumps(user),
                                 headers=header)
        return await self.http_client.fetch(request)

    def qet_user(self):
        raise NotImplementedError('Please Implement this method')

    def qet_email(self, d):
        raise NotImplementedError('Please Implement this method')

    def qet_id(self, d):
        raise NotImplementedError('Please Implement this method')

    def get_gender(self, d):
        raise NotImplementedError('Please Implement this method')

    def get_date(self, d):
        raise NotImplementedError('Please Implement this method')

    def get_photo(self, d):
        raise NotImplementedError('Please Implement this method')

    def get_first_name(self, d):
        raise NotImplementedError('Please Implement this method')

    def get_second_name(self, d):
        raise NotImplementedError('Please Implement this method')

