import os, time, logging, tornado.iostream, hashlib, binascii
import ujson as json
from tornado.options import options
import tornado.httpclient as cl
from config import USE_SLEEP, SLEEP_SECOND, SALT


def gen_pswd(email):
    dk = hashlib.pbkdf2_hmac('sha256',str.encode(email), SALT, 2048)
    return str(binascii.hexlify(dk))


def rh_token(str_tok):
    if str_tok is None:
        return None
    return {'Authorization': 'Token ' + str_tok}


def h_token(obj):
    return rh_token(obj.get_cookie('token'))


def from_json(body):
    body_unicode = body.decode('utf-8')
    return json.loads(body_unicode)


def fix_path(data, url_prefix):
    pdata = from_json(data)

    next = pdata.get('next')
    prev = pdata.get('previous')

    if next:
        n_next = next.split('?')[1]
        pdata['next'] = (url_prefix + '?').join(['', n_next])
    if prev:
        try:
            n_prev = prev.split('?')[1]
            pdata['previous'] = (url_prefix + '?').join([options.my_url, n_prev])
        except:
            pdata['previous'] = options.my_url + url_prefix

    return json.dumps(pdata)


def addr(url):
    return options.api + url

def addrm(url):
    return options.my_url + url


class SafeFileIOStream:
    def __init__(self, fname):
        self.fname = fname

    def __enter__(self):
        os.open(self.fname, os.O_CREAT)
        fd = os.open(self.fname, os.O_WRONLY)
        self.stream = tornado.iostream.PipeIOStream(fd)
        return self.stream

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stream.close()


class JsonRequestHandler(tornado.web.RequestHandler):
    h_json = {'Content-Type': 'application/json; charset=UTF-8'}

    def initialize(self):
        self.set_header('Content-Type', 'application/json; charset=UTF-8')

    def _token(self):
        h = h_token(self)
        return {} if h is None else h

    def make_post(self, url, body=''):
        if USE_SLEEP:
            time.sleep(SLEEP_SECOND)
        return cl.HTTPRequest(addr(url), method='POST',
                              headers={**self._token(), **self.h_json}, body=body)

    def make_patch(self, url, body=''):
        if USE_SLEEP:
            time.sleep(SLEEP_SECOND)
        return cl.HTTPRequest(addr(url), method='PATCH',
                              headers={**self._token(), **self.h_json}, body=body)

    def make_get(self, url):
        if USE_SLEEP:
            time.sleep(SLEEP_SECOND)
        return cl.HTTPRequest(addr(url), headers={**self._token(), **self.h_json})

    def make_delete(self, url):
        if USE_SLEEP:
            time.sleep(SLEEP_SECOND)
        return cl.HTTPRequest(addr(url), method='DELETE', headers={**self._token(), **self.h_json})


class BaseHandler(JsonRequestHandler):
    http_client = tornado.httpclient.AsyncHTTPClient()


class BaseListHandler(BaseHandler):
    api_url = ''
    site_url = ''

    async def get(self, id=None):
        url = self.site_url if id is None else self.site_url + id + '/'
        try:
            response = await self.http_client.fetch(
                    self.make_get(self.request.uri.replace(self.site_url,
                                                           self.api_url)))
            self.write(fix_path(response.body, url))
        except tornado.httpclient.HTTPError as e:
            self.write({'error': e.code})


class BaseNewHandler(BaseHandler):
    api_url = ''

    async def post(self, id=None):
        url = self.api_url if id is None else self.api_url + id + '/'
        try:
            response = await self.http_client.fetch(
                    self.make_post(url, body=self.request.body)
            )
            self.write(response.body)
        except tornado.httpclient.HTTPError as e:
            self.write({'error': e.code})


class BaseDetailHandler(BaseHandler):
    url = ''

    async def get(self, id=None):
        url = self.url if id is None else self.url + id + '/'
        try:
            response = await self.http_client.fetch(self.make_get(url))
            self.write(response.body)
        except tornado.httpclient.HTTPError as e:
            self.write({'error': e.code})


class BaseDeleteHandler(BaseHandler):
    url = ''

    async def post(self, id=None):
        url = self.url if id is None else self.url + id + '/'
        try:
            response = await self.http_client.fetch(self.make_delete(url))
            self.write(response.body)
        except tornado.httpclient.HTTPError as e:
            self.write({'error': e.code})


class BaseUpdateHandler(BaseHandler):
    url = ''

    async def post(self, id=None):
        url = self.url if id is None else self.url + id + '/'
        try:
            response = await self.http_client.fetch(
                    self.make_patch(url, body=self.request.body)
            )
            self.write(response.body)
        except tornado.httpclient.HTTPError as e:
            self.write({'error': e.code})