from mimetypes import guess_extension
from uuid import uuid4
from jpegtran import JPEGImage
from wand.image import Image
import os, random, sys, math, io
from datetime import date
import ujson as json
import tornado.web
import tornado.gen
import tornadoredis
import base64
from lib import JsonRequestHandler, SafeFileIOStream
from config import IMG_DIR, STATIC_DIR, IMG_PATH, STATIC_PATH, \
    REDIS_HOST, REDIS_PORT, USE_REDIS_CACHE


CONNECTION_POOL = tornadoredis.ConnectionPool(max_connections=500,
                                              wait_for_available=True,
                                              host=REDIS_HOST, port=int(REDIS_PORT))
CACHE_TIME = 12 * 60 * 60
MAX_IMG_CACHED_SIZE = 100 * 1000


def make_path():
    today = date.today()
    return os.path.join(str(today.year + 1), str(today.month), str(today.day))


def get_fill_name(file):
    fname = str(uuid4().hex) + str(random.randint(0, sys.maxsize))
    with_ext = fname + guess_extension(file.content_type)
    date_path = make_path()
    full_path = os.path.join(IMG_PATH, date_path)
    os.makedirs(full_path, exist_ok=True)
    return {
        'full_path': os.path.join(full_path, with_ext),
        'relative_path': os.path.join(IMG_DIR, date_path, with_ext)
    }


def get_min_max(num, delim):
    l = num % delim
    if l == 0:
        return l, 0,  l, 0
    w = num - l
    if w < 0:
        w = 0
    mm_w = num + delim - l
    return w, num-w, mm_w, mm_w-num


class UploadHandler(JsonRequestHandler):

    def get_fixed_coord(self, n_x, num):
        dn_x = 0
        n_x1, dn_x1, n_x2, dn_x2 = get_min_max(n_x, num)
        if math.fabs(dn_x1) > math.fabs(dn_x2):
            n_x = n_x2
            dn_x = dn_x2
        else:
            n_x = n_x1
            dn_x = dn_x1
        return n_x, dn_x

    def is_jpeg(self, file):
        ext = guess_extension(file.content_type)
        return 'jpeg' in ext or 'jpg' in ext or 'jpe' in ext

    async def post(self):
        file = self.request.files['file'][0]
        result = {}
        original = get_fill_name(file)
        with SafeFileIOStream(original['full_path']) as stream:
            await stream.write(file.body)
        result['original'] = os.sep + original['relative_path']

        jcrop = self.get_argument('crop', default=None)
        jsize = self.get_argument('size', default=None)

        #print(jcrop, jsize)

        if jcrop is None or jsize is None:
            self.write(result)
            return
        crop = json.loads(jcrop)
        size = json.loads(jsize)

        W = size['width']
        H = size['height']
        w = crop['w']
        h = crop['h']
        x = crop['x']
        y = crop['y']
        cropped = get_fill_name(file)
        img = None
        if self.is_jpeg(file):
            img = JPEGImage(blob=file.body)
        else:
            img = Image(blob=file.body)
        print('crop params:', crop, size, img.width, img.height)
        w_coff = float(img.width) / W
        h_coff = float(img.height) / H

        coff = 1
        if w_coff > 1 or h_coff > 1:
            coff = w_coff if w_coff > h_coff else h_coff

        n_x = int(math.ceil(coff*x))
        n_y = int(math.ceil(coff*y))

        n_x, dn_x = self.get_fixed_coord(n_x, 16)
        n_y, dn_y = self.get_fixed_coord(n_y, 16)

        n_width = int(math.ceil(coff*w)) + dn_y
        if n_width > img.width:
            n_width = img.width

        n_height = int(math.ceil(coff*h)) + dn_x
        if n_height > img.height:
            n_height = img.height

        print('crop:', n_x, n_y, n_width, n_height, img.width, img.height)
        blob = None
        if self.is_jpeg(file):
            img = img.crop(n_x, n_y, width=n_width, height=n_height)
            blob = img.as_blob()
        else:
            img.crop(n_x, n_y, width=n_width, height=n_height)
            blob = img.make_blob()
        with SafeFileIOStream(cropped['full_path']) as stream:
            await stream.write(blob)
        result['cropped'] = os.sep + cropped['relative_path']

        self.write(result)


class ResizeImgHandler(tornado.web.RequestHandler):

    SIZES = {
        'user': {
            'avatar': {
                'sm': (40, 40),
                'md': (200, 200),
                'lg': (250, 250)
            },
        },
        'post': {
            'preview': {
                'lg': (400, 250),
            }
        },
        'event': {
            'preview': {
                'lg': (700, 300),
            }
        },
        'blog': {
            'preview': {
                'lg': (700, 300),
            }
        },
    }

    def get_size(self, context, detail, size):
        return self.SIZES[context][detail][size]

    def is_jpeg(self, name):
        return 'jpeg' in name or 'jpg' in name or 'jpe' in name

    @tornado.web.gen.coroutine
    def get_image(self, context, detail, size, name):
        key = None
        c = None
        if USE_REDIS_CACHE:
            c = tornadoredis.Client(connection_pool=CONNECTION_POOL)
            c.connect()
            key = '-'.join([context, detail,  size, name])
            img_from_cache = yield tornado.gen.Task(c.get, key)

            if img_from_cache is not None:
                return base64.decodebytes(img_from_cache.encode('utf-8'))

        width, height = self.get_size(context, detail, size)
        img_blob = None
        if self.is_jpeg(name):
            img = JPEGImage(fname=os.path.join(STATIC_PATH, name))
            img = img.downscale(width=width, height=height)
            img_blob = img.as_blob()
        else:
            img = Image(filename=os.path.join(STATIC_PATH, name))
            img.resize(width=width, height=height)
            img_blob = img.make_blob()
        if len(img_blob) < MAX_IMG_CACHED_SIZE and USE_REDIS_CACHE:
            result = yield tornado.gen.Task(c.set, key,
                                            base64.encodebytes(img_blob).decode('utf-8'),
                                            expire=CACHE_TIME)
        return img_blob

    @tornado.web.gen.coroutine
    def get(self, context, detail, size, name):
        cache_time = 24 * 60 * 60

        img_blob = yield self.get_image(context, detail, size, name)

        self.set_header('Content-Type', 'image/jpg')
        self.set_header('Content-length', len(img_blob))
        self.set_header('Cache-Control', 'max-age=' + str(cache_time))
        self.write(img_blob)



