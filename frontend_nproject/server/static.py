import os, tornado.web
from pathlib import Path
from config import USE_HTTP_CACHE, STATIC_PATH


def get_index(fname):
    class MainHandler(tornado.web.RequestHandler):
        index = Path(os.path.join(STATIC_PATH, 'html', fname)).read_text()

        def get(self, slug=None):
            self.write(self.index)

        def set_extra_headers(self, path):
            if not USE_HTTP_CACHE:
                self.set_header('Cache-control', 'no-store, no-cache, must-revalidate, max-age=0')

    return MainHandler


class StaticFileHandler(tornado.web.StaticFileHandler):
    def set_extra_headers(self, path):
        if not USE_HTTP_CACHE:
            self.set_header('Cache-control', 'no-store, no-cache, must-revalidate, max-age=0')