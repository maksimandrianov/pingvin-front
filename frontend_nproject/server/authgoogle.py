import json
import tornado.httpclient as cl
import urllib.parse
from datetime import date
from authsocial import SocialHandler
from lib import BaseHandler, from_json, addr, \
    rh_token, gen_pswd, addrm


class LoginGoogleHandler(SocialHandler):
    CLIENT_ID = '668548278228-htf1num1hdo8kmtimhrno819ub0rl0qo.apps.googleusercontent.com'
    CLIENT_SECRET = 'S6UUsNsPhCbFpz-yp6N99k0Y'
    REDIRECT_URI = '/social-login/?sn=gp'

    async def qet_user(self):
        token_response = await self.get_token_query(self.get_argument('code'))
        token = from_json(token_response.body)['access_token']
        response_userinfo = await self.http_client.fetch(
                cl.HTTPRequest(self.userinfo_url(token)))
        body = from_json(response_userinfo.body)
        return {
            'user_id': self.get_id(body),
            'sn': 4,  # gp
            'first_name': self.get_first_name(body),
            'second_name': self.get_second_name(body),
            'bday': self.get_date(body),
            'gender': self.qet_gender(body),
            'fake_email': str(self.get_id(body)) + '@google.google',
            'email': self.get_email(body),
            'avatar': {
                'url': self.get_photo(body)
            }
        }

    def get_photo(self, d):
        img = d['image']['url']
        index = img.find('?sz=')
        return img[:index] + '?sz=400'

    def get_first_name(self, d):
        return d['name']['givenName']

    def get_second_name(self, d):
        return d['name']['familyName']

    def get_id(self, d):
        return d['id']

    def qet_gender(self, d):
        gender = d.get('gender')
        if gender == 'female':
            return 'f'
        if gender == 'male':
            return 'm'
        else:
            return 'x'

    def get_date(self, d):
        return None

    def get_email(self, d):
        for e in d['emails']:
            if e['type'] == 'account':
                return e['value']
        return ''

    def make_token_url(self):
        code = self.get_argument('code')
        url = 'https://accounts.google.com/o/oauth2/token'
        return url

    async def get_token_query(self, code):
        data = {
            'code': code,
            'client_id': self.CLIENT_ID,
            'client_secret': self.CLIENT_SECRET,
            'redirect_uri': addrm(self.REDIRECT_URI),
            'grant_type': 'authorization_code'
        }
        body = urllib.parse.urlencode(data)
        r = cl.HTTPRequest(self.make_token_url(), method='POST',
                           headers={'Content-Type': 'application/x-www-form-urlencoded'},
                           body=body)
        response = await self.http_client.fetch(r)
        return response

    def userinfo_url(self, token):
        url = 'https://www.googleapis.com/plus/v1/people/me?alt=json' + \
              '&access_token=' + str(token)
        return url