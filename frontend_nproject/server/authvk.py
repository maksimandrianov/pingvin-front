import json
import tornado.httpclient as cl
from datetime import date
from authsocial import SocialHandler
from lib import BaseHandler, from_json, addr, \
    rh_token, gen_pswd, addrm


class LoginVkHandler(SocialHandler):
    CLIENT_ID = '6097367'
    CLIENT_SECRET = '0l64SjHW0dJI6mTEr8Wu'
    REDIRECT_URI = '/social-login/?sn=vk'

    async def qet_user(self):
        response_tok = await self.http_client.fetch(
                cl.HTTPRequest(self.make_token_url()))
        body_tok = from_json(response_tok.body)
        user_id = body_tok.get('user_id')
        email = body_tok.get('email')
        token = body_tok.get('access_token')
        response_usrinfo = await self.http_client.fetch(
                cl.HTTPRequest(self.make_get_user_vk(user_id, token)))
        body = from_json(response_usrinfo.body)['response'][0]
        return {
            'user_id': user_id,
            'sn': 1,  # vk
            'first_name': self.get_first_name(body),
            'second_name': self.get_second_name(body),
            'bday': self.get_date(body),
            'gender': self.get_gender(body),
            'fake_email': str(user_id) + '@vk.vk',
            'email': email,
            'avatar': {
                'url': self.get_photo(body)
            }
        }

    def get_first_name(self, d):
        return d['first_name']

    def get_second_name(self, d):
        return d['last_name']

    def get_photo(self, d):
        return d['photo_400_orig']

    def get_gender(self, d):
        gender = d.get('sex')
        if gender == 1:
            return 'f'
        if gender == 2:
            return 'm'
        else:
            return 'x'

    def get_date(self, d):
        da = d.get('bdate')
        if da is None:
            return None
        c = da.split('.')
        if len(c) != 3:
            return None
        return date(int(c[2]), int(c[1]), int(c[0])).strftime('%Y-%m-%d')

    def make_token_url(self):
        code = self.get_argument('code')
        url = 'https://oauth.vk.com/access_token?client_id=' + self.CLIENT_ID + \
              '&client_secret=' + self.CLIENT_SECRET + \
              '&redirect_uri=' + addrm(self.REDIRECT_URI) + \
              '&code=' + code
        return url

    def make_get_user_vk(self, id, token):
        url = 'https://api.vk.com/method/users.get?uids=' + str(id) + \
              '&fields=uid,first_name,last_name,sex,bdate,city,country,photo_400_orig' + \
              '&access_token=' + str(token)
        return url
