import os


IS_DEBUG = False if os.environ.get('RELEASE') else True
USE_HTTP_CACHE = not IS_DEBUG
USE_SLEEP = False
STATIC_DIR = 'public'
IMG_DIR = 'img'
BASE_PATH = os.path.abspath(os.path.join(os.path.dirname( __file__), '..'))
STATIC_PATH = os.path.join(BASE_PATH, STATIC_DIR)
IMG_PATH = os.path.join(STATIC_PATH, IMG_DIR)
INDEX_NAME='index.html'
SLEEP_SECOND = 1

SALT = b'c7F65827&,jfs!'

USE_REDIS_CACHE = True
REDIS_HOST = os.environ.get('REDIS_HOST', 'localhost')
REDIS_PORT = os.environ.get('REDIS_PORT', 6379)

