import json
import tornado.httpclient as cl
import urllib.parse
from datetime import date
from authsocial import SocialHandler
from lib import BaseHandler, from_json, addr, \
    rh_token, gen_pswd, addrm


class LoginFbHandler(SocialHandler):
    CLIENT_ID = '1878413785741841'
    CLIENT_SECRET = 'a9ddf27e61379a971c5dfc8fc8a6bcf5'
    REDIRECT_URI = '/social-login/?sn=fb'

    async def qet_user(self):
        token_response = await self.get_token_query(self.get_argument('code'))
        token = from_json(token_response.body)['access_token']
        response_userinfo = await self.http_client.fetch(
                cl.HTTPRequest(self.userinfo_url(token)))
        body = from_json(response_userinfo.body)
        print(body)
        return {
            'user_id': self.get_id(body),
            'sn': 2,  # fb
            'first_name': self.get_first_name(body),
            'second_name': self.get_second_name(body),
            'bday': self.get_date(body),
            'gender': self.qet_gender(body),
            'fake_email': str(self.get_id(body)) + '@facebook.facebook',
            'email': self.get_email(body),
            'avatar': {
                'url': self.get_photo(body)
            }
        }

    def get_photo(self, d):
        user_id = d['id']
        return 'https://graph.facebook.com/' + user_id + '/picture?width=480&height=480'

    def get_first_name(self, d):
        return d['first_name']

    def get_second_name(self, d):
        return d['last_name']

    def get_id(self, d):
        return d['id']

    def qet_gender(self, d):
        gender = d.get('gender')
        if gender == 'female':
            return 'f'
        if gender == 'male':
            return 'm'
        else:
            return 'x'

    def get_date(self, d):
        return None

    def get_email(self, d):
        email = d.get('email')
        if email is None:
            return ''
        return email


    async def get_token_query(self, code):
        url = 'https://graph.facebook.com/oauth/access_token?client_id=' \
              + self.CLIENT_ID + '&redirect_uri=' + addrm(self.REDIRECT_URI) + \
              '&client_secret=' + self.CLIENT_SECRET + '&code=' + code
        request = cl.HTTPRequest(url)
        response = await self.http_client.fetch(request)
        return response

    def userinfo_url(self, token):
        url = 'https://graph.facebook.com/me?fields=first_name,last_name,gender,picture,email,id' + \
              '&access_token=' + str(token)
        return url