/**
 * Created by maks on 02.04.17.
 */

'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const minifycss = require('gulp-clean-css');
const rename = require('gulp-rename');
const del = require('del');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const mkdirp = require('mkdirp');
const ngAnnotate = require('gulp-ng-annotate');
const gulpif = require('gulp-if');
const htmlmin = require('gulp-htmlmin');
const remoteSrc = require('gulp-remote-src');
const fileinclude = require('gulp-file-include');

var IS_DEBUG = true;

function js(params) {
  return gulp.src(params.from)
    .pipe(gulpif(IS_DEBUG, gulp.dest(params.to)))
    .pipe(concat(params.name))
    .pipe(ngAnnotate())
    .pipe(gulpif(IS_DEBUG, gulp.dest(params.to)))
    .pipe(uglify())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(params.to));
}

function copyMinJs(params) {
  return gulp.src(params.from)
    .pipe(gulpif(IS_DEBUG, gulp.dest(params.to)))
    .pipe(uglify())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(params.to));
}

function copy(params) {
  return gulp.src(params.from)
    .pipe(gulp.dest(params.to));
}

gulp.task('clean', function () {
  return del('public');
});


gulp.task('img-dir', function () {
  return copy({
    from: ['static/frontend/img/*.*'],
    to: 'public/img'
  });
});

gulp.task('pingvin-styles', function () {
  return gulp.src('static/frontend/css/style.scss')
    .pipe(sass())
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('public/css'));
});

gulp.task('lib-styles', function () {
  return gulp.src(['static/frontend/css/lib/*.css',
      '!static/frontend/css/lib/angular-material.min.css'])
    .pipe(concat('ext-lib.css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('public/css/lib'));
});

gulp.task('copy-lib-styles', function () {
  return copy({
    from: ['static/frontend/css/lib/angular-material.min.css'],
    to: 'public/css/lib'
  });
});

gulp.task('pingvin-js', function () {
  return js({
    from: ['static/frontend/js/app.module.js', 'static/frontend/js/*.js'],
    to: 'public/js',
    name: 'pingvin.js'
  });
});

gulp.task('angular-lib', function () {
  return js({
    from: ['static/frontend/js/lib2/angular-lib/*js'],
    to: 'public/js/angular-lib',
    name: 'ext-angular-lib.js'
  });
});


gulp.task('ext-lib', function () {
  return js({
    from: [
      'static/frontend/js/lib2/jquery.min.js',
      'static/frontend/js/lib2/jquery-bridget.js',
      'static/frontend/js/lib2/ev-emitter.js',
      'static/frontend/js/lib2/matches-selector.js',
      'static/frontend/js/lib2/fizzy-ui-utils.js',
      'static/frontend/js/lib2/get-size.js',
      'static/frontend/js/lib2/outlayer-item.js',
      'static/frontend/js/lib2/outlayer.js',
      'static/frontend/js/lib2/masonry.js',
      'static/frontend/js/lib2/imagesloaded.js',
      'static/frontend/js/lib2/lodash.min.js',
      'static/frontend/js/lib2/platform.js',
      'static/frontend/js/lib2/slick.min.js',
      'static/frontend/js/lib2/emojionearea.min.js',
      'static/frontend/js/lib2/loader.min.js'
    ],
    to: 'public/js/lib',
    name: 'ext-lib.js'
  });
});

gulp.task('angular', function () {
  return copy({
    from: ['static/frontend/js/lib2/angular/*.js'],
    to: 'public/js/angular'
  });
});

gulp.task('copy-big-to-angular-lib', function () {
  return copy({
    from: ['static/frontend/js/lib2/big-lib/angular-google-maps.min.js'],
    to: 'public/js/angular-lib'
  });
});


gulp.task('copy-big-to-lib', function () {
  return copy({
    from: ['static/frontend/js/lib2/big-lib/emojione_2.2.6.min.js'],
    to: 'public/js/lib'
  });
});


gulp.task('copy-and-min-big-to-lib', function () {
  return copyMinJs({
    from: ['static/frontend/js/lib2/big-lib/moment.js'],
    to: 'public/js/lib'
  });
});


gulp.task('copy-html', function () {
  return gulp.src('static/frontend/html/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('public/html'));
});

gulp.task('remote-css', function() {
  return remoteSrc([
    'cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.12/css/perfect-scrollbar.min.css'
  ], {
    base: 'https://'
  })
    .pipe(minifycss())
    .pipe(rename({extname: '.css', suffix: '.min'}))
    .pipe(gulp.dest('public/css/lib'));
});

gulp.task('concat-styles', function () {
  return gulp.src(['public/css/lib/**/*', 'public/css/style.min.css', '!*/text-angular/*.css'])
    .pipe(concat('all-styles.css'))
    .pipe(minifycss({specialComments: 0}))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('public/css/'));
});

gulp.task('pages-build', function () {
  return   gulp.src([
      'public/html/index.html',
      'public/html/help.html',
      'public/html/about-me-edit.html'
    ])
    .pipe(fileinclude({
      prefix: '____',
      basepath: 'public/'
    }))
    .pipe(gulp.dest('public/html/'));
});

gulp.task('copy-robots.txt', function () {
  return copy({
    from: ['static/robots.txt'],
    to: 'public/'
  });
});

gulp.task('copy-css-text-angular', function () {
  return gulp.src('static/frontend/css/lib/text-angular/*.css')
    .pipe(concat('text-angular.css'))
    .pipe(minifycss({specialComments: 0}))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('public/css/lib/text-angular'));
});

gulp.task('copy-js-text-angular', function () {
  return gulp.src('static/frontend/js/lib2/text-angular/*.js')
    .pipe(gulp.dest('public/js/lib/text-angular'));
});

gulp.task('common-build', gulp.parallel(
  'copy-html',
  'img-dir',

  'pingvin-styles',
  'lib-styles',
  'copy-lib-styles',
  'remote-css',

  'pingvin-js',
  'angular-lib',
  'ext-lib',
  'angular',
  'copy-big-to-angular-lib',
  'copy-and-min-big-to-lib',
  'copy-big-to-lib',
  'copy-robots.txt'

));


gulp.task('rebuild', gulp.series('clean', 'common-build', 'concat-styles',
  'copy-css-text-angular', 'copy-js-text-angular', 'pages-build'));

gulp.task('watch', function () {
  gulp.watch('static/frontend', gulp.series('common-build'));
});

gulp.task('release',gulp.series(function (callback) { IS_DEBUG = false; callback(); }, 'rebuild'));
gulp.task('debug', gulp.series(function (callback) { IS_DEBUG = true; callback(); }, 'common-build'));