/**
 * Created by maks on 09.07.17.
 */

(function () {
  'use strict';

  angular.module('pingvinApp').component('searchMenuPost', {
    templateUrl: '/public/html/search-menu-post.html',
    controller: ['$scope', '$rootScope', '$log', '$timeout', '$location',
      '$mdMenu', 'categoryService', 'utilService', 'dataService', SearchMenuPostController]
  });

  function SearchMenuPostController($scope, $rootScope, $log, $timeout, $location,
                                    $mdMenu, categoryService, utilService, dataService) {

    var self                = this;

    self.selectedCategory          = undefined;
    self.searchTextCategory        = '';
    self.selectedSubcategory       = undefined;
    self.searchTextSubcategory     = '';
    self.categories                = loadCategories();
    self.subcategories             = [];
    self.motivation                = undefined;

    self.querySearchSubcategory    = querySearchSubcategory;
    self.querySearchCategory       = querySearchCategory;
    self.selectedCategoryChange    = selectedCategoryChange;
    self.searchCategoryChange      = searchCategoryChange;
    self.searchSubcategoryChange   = searchSubcategoryChange;
    self.selectedSubcategoryChange = selectedSubcategoryChange;
    self.sabcategoryDisable        = true;

    self.changeMotivation          = changeMotivation;

    $log                           = $log.getInstance('SearchMenuPostController');


    init();

    function init() {
      initFromUrl();
      $scope.$on('$routeUpdate', function () {
        initFromUrl();
      });
    }

    function initFromUrl() {
      var params = $location.search(),
        search,
        title;
      $log.log('initFromUrl', params);
      if (angular.isDefined(params.subcategory) &&
        angular.isUndefined(params.category)) {
        $location.search('subcategory', null);
      } else if (angular.isDefined(params.category)) {
        title = categoryService.getTitleFromType(self.categories, params.category);
        search = querySearchCategory(title);
        if (search.length === 1 && search[0].display === title) {
          self.selectedCategory = search[0];
          self.searchTextCategory = search[0].display;
          self.subcategories = loadSubcategories(search[0].subcategories);
          self.sabcategoryDisable = false;
          if (angular.isDefined(params.subcategory)) {
            title = categoryService.getTitleFromType(self.subcategories, params.subcategory);
            search = querySearchSubcategory(title);
            if (search.length === 1 && search[0].display === title) {
              self.selectedSubcategory = search[0];
              self.searchTextSubcategory = search[0].display;
            } else {
              $location.search('subcategory', null);
            }
          }
        } else {
          $location.search('category', null);
        }
      }

      if (self.selectedCategory && angular.isUndefined(params.category)) {
        self.subcategories = [];
        self.searchTextSubcategory = '';
        self.searchTextCategory = '';
        self.sabcategoryDisable = true;
        self.selectedSubcategory = undefined;
        self.selectedCategory = undefined;
      }

      if (self.selectedSubcategory && angular.isUndefined(params.subcategory)) {
        self.selectedSubcategory = undefined;
        self.searchTextSubcategory = '';
      }

      self.motivation = params.motivation ? params.motivation : 0;
      $log.log('self.motivation', self.motivation);
    }

    function changeMotivation() {
      $log.info('changeMotivation', self.motivation);
      var motivation = parseInt(self.motivation,  10);
      if (motivation !== 0)
        $location.search('motivation', motivation);
      else
        $location.search('motivation', null);
    }

    function querySearch(list, query) {
      return angular.isUndefined(list) ? [] : list.filter(createFilterFor(query));
    }

    function querySearchSubcategory(query) {
      var res = querySearch(self.subcategories, query);
      if (!res.length)
        self.searchTextSubcategory = '';
      return res;
    }

    function querySearchCategory(query) {
      var res = querySearch(self.categories, query);
      if (!res.length)
        self.searchTextCategory = '';
      return res;
    }

    function searchCategoryChange(text) {
      querySearchCategory(text);
    }

    function selectedCategoryChange(item) {
      $log.log('Category changed to ' + JSON.stringify(item));
      if (angular.isUndefined(item)) {
        self.subcategories = [];
        self.searchTextSubcategory = '';
        self.searchTextCategory = '';
        self.sabcategoryDisable = true;
        $location.search('category', null);
        return;
      }
      self.subcategories = loadSubcategories(item.subcategories);
      self.sabcategoryDisable = false;
      $location.search('category', item.type);
    }

    function searchSubcategoryChange(text) {
      querySearchSubcategory(text);
    }

    function selectedSubcategoryChange(item) {
      $log.log('Subcategory changed to ' + JSON.stringify(item));
      if (angular.isUndefined(item)) {
        self.searchTextSubcategory = '';
        $location.search('subcategory', null);
        return;
      }
      $location.search('subcategory', item.type);
    }


    function loadCategories() {
      return categoryService.categories.map(function (state) {
        state.value = state.title.toLowerCase();
        state.display = state.title;
        return state;
      });
    }

    function loadSubcategories(arr) {
      return arr.map(function (state) {
        state.value = state.title.toLowerCase();
        state.display = state.title;
        return state;
      });
    }

    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(state) {
        return (state.value.indexOf(lowercaseQuery) === 0);
      };
    }

  }
}());