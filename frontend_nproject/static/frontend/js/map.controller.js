/**
 * Created by maks on 17.09.16.
 */


(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('NewPostGoogleMapCtrl', ['$scope', '$log', '$timeout', '$mdDialog',
      'uiGmapGoogleMapApi', 'utilService', 'uiGmapIsReady',
      NewPostGoogleMapCtrl]);

  function NewPostGoogleMapCtrl($scope,  $log, $timeout, $mdDialog, uiGmapGoogleMapApi,
                                utilService, uiGmapIsReady) {

    var self = this;

    // russia, spb
    self.lat       = 59.9386300;
    self.lng       = 30.3141300;
    self.geocoder  = undefined;
    self.toast     = utilService.initToast();
    self.save      = save;
    self.center    = center;
    self.coordinates = coordinates;
    self.isUse       = false;
    self.marker = {
      id: 0,
      coords: {
        latitude: self.lat,
        longitude: self.lng
      },
      options: { draggable: true },
      control: {}
    };
    self.map = {
      center: {
        latitude: self.lat,
        longitude: self.lng
      },
      zoom: 12,
      options: {},
      control: {}
    };

    self.oldCoords =  {
      latitude: undefined,
      longitude: undefined
    };

    $scope.cancel = function () {
      $mdDialog.cancel();
    };




    init();

    function init() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
          self.map.center.latitude =  position.coords.latitude;
          self.map.center.longitude = position.coords.longitude;

          self.marker.coords.latitude =  position.coords.latitude;
          self.marker.coords.longitude = position.coords.longitude;
          self.map.control.refresh(self.map.center);
          $log.log('navigator.geolocation');
        });
      }
      uiGmapGoogleMapApi.then(function (maps) {
        $log.log('uiGmapGoogleMapApi');
        self.geocoder = new google.maps.Geocoder();
      });
      repaint();
    }


    function coordinates(lat, lng) {
      if (lat === '0.0' && lng === '0.0') {
        lat = self.lat;
        lng = self.lng;
        self.isUse = false;
      }
      self.marker.coords.latitude = lat;
      self.marker.coords.longitude = lng;
      self.map.center.latitude = lat;
      self.map.center.longitude = lng;
      self.isUse = true;

      self.oldCoords.latitude = lat;
      self.oldCoords.longitude = lng;
    }

    function repaint() {
      uiGmapIsReady.promise(1).then(function (instances) {
        $log.log('uiGmapIsReady');
        // reload
        $timeout(function () {
          google.maps.event.trigger(self.map.control.getGMap(), 'resize');
          self.map.control.refresh(self.map.center);
          google.maps.event.trigger(self.map.control.getGMap(), 'click');
        }, 100);
      });
    }

    function getGoogleGeoData(arrAddress) {
      var result = {
        itemRoute: '',
        itemLocality: '',
        itemCountry: '',
        itemPc: '',
        itemSnumber: ''
      };
      arrAddress.forEach(function (address_component) {
        if (address_component.types[0] === 'route') {
          $log.info('route:' + address_component.long_name);
          result.itemRoute = address_component.long_name;
        } else if (address_component.types[0] === 'locality') {
          $log.info('town:' + address_component.long_name);
          result.itemLocality = address_component.long_name;
        } else if (address_component.types[0] === 'country') {
          $log.info('country:' + address_component.long_name);
          result.itemCountry = address_component.long_name;
        } else if (address_component.types[0] === 'postal_code_prefix') {
          $log.info('pc:' + address_component.long_name);
          result.itemPc = address_component.long_name;
        } else if (address_component.types[0] === 'street_number') {
          $log.info('street_number:' + address_component.long_name);
          result.itemSnumber = address_component.long_name;
        }
      });

      return result;
    }

    function save(callback) {
      var data = {
          'country': '',
          'address': '',
          'city': '',
          'point': {
            'latitude': '0.0',
            'longitude': '0.0'
          }
        },
        latlng  = {
          'lat': parseFloat(self.marker.coords.latitude),
          'lng': parseFloat(self.marker.coords.longitude)
        };

      if (!self.isUse) {
        if (callback && typeof callback === 'function') callback(data);
        return;
      }

      if (!self.geocoder) {
        $log.error('geocoder not init');
        self.toast('Ошибка геокодирования');
      }

      if (angular.isDefined(self.oldCoords.latitude) &&
        parseFloat(self.oldCoords.latitude).toFixed(4) === latlng.lat.toFixed(4) &&
        parseFloat(self.oldCoords.longitude).toFixed(4) === latlng.lng.toFixed(4)) {
        if (angular.isFunction(callback)) callback();
        $scope.$emit('locationIsReady');
        return;
      }

      self.geocoder.geocode({'location': latlng}, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {

          var gdata = getGoogleGeoData(results[0].address_components);

          data.country = gdata.itemCountry;
          data.city = gdata.itemLocality;
          data.address = results[0].formatted_address;
          data.point.latitude = latlng.lat;
          data.point.longitude = latlng.lng;
          if (angular.isFunction(callback)) callback(data);
          $scope.$emit('locationIsReady', data);

          self.oldCoords.latitude = latlng.lat + '';
          self.oldCoords.longitude = latlng.lng + '';
        } else {
          self.toast('Ошибка геокодирования:' + status);
        }
      });
    }


    function center(address) {
      if (!self.geocoder) {
        $log.error('geocoder not init');
        return;
      }
      if (!address) {
        $log.warn('empty address');
        return;
      }
      self.geocoder.geocode({'address': address}, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          var location = results[0].geometry.location;
          self.map.center.latitude = location.lat();
          self.map.center.longitude = location.lng();
          self.marker.coords.latitude = location.lat();
          self.marker.coords.longitude = location.lng();
          self.map.control.refresh(self.map.center);
        } else {
          self.toast('Ошибка геокодирования:' + status);
        }
      });
    }
  }

}());

