/**
 * Created by maks on 01.09.16.
 */

(function () {
  'use strict';

  angular.module('pingvinApp').controller('SignupController', [
    '$scope', '$rootScope', '$timeout', '$log', '$mdDialog', '$location',
    '$mdMenu', 'authService', 'DEBUG',
    SignupController
  ]);

  function SignupController($scope, $rootScope, $timeout, $log,
                            $mdDialog, $location, $mdMenu, authService, DEBUG) {
    var self = this;

    self.signup        = signup;
    self.login         = login;
    self.forgot        = forgot;
    self.loginUrl      = undefined;
    self.signupUrl     = undefined;
    self.forgotUrl     = undefined;
    self.clear         = clear;
    self.merrorMessage = '';
    self.init          = initPath;
    self.social        = authService.social;
    $scope.$mdMenu     = $mdMenu;
    $scope.cancel      = function () { $mdDialog.cancel(); };

    $log               = $log.getInstance('SignupController');

    $log.log('SignupController');


    init();

    function init() {
      initPath();
      $rootScope.$on('$routeChangeSuccess', function () {
          initPath();
      });
    }

    function initPath() {
      var path = $location.path();
      self.loginUrl      = path + '?login';
      self.signupUrl     = path + '?signup';
      self.forgotUrl     = path + '?forgot';
    }


    function clear() {
      self.merrorMessage = '';
    }

    function alertSignup(name) {
      $mdDialog.show(
        $mdDialog.alert()
          .clickOutsideToClose(true)
          .title('Уведомление о регистрации')
          .textContent(name + ', вам отправлено письмо с ссылкой для активации аккаунта на почту')
          .ok('Закрыть'));
    }

    function alertForgot() {
      $mdDialog.show(
        $mdDialog.alert()
          .clickOutsideToClose(true)
          .title('Уведомление')
          .textContent('Вам отправлено письмо с ссылкой для сброса пароля на почту')
          .ok('Закрыть'));
    }

    function signup() {
      authService.signup($scope.user)
        .then(function (data) {
          //$scope.$parent.cancel();
          $mdDialog.cancel();
          $timeout(function () {
            if (DEBUG)
              login();
            else
              alertSignup(data.first_name);
          });
          $log.log('signup success');
        }, function (response) {
          $log.log('signup fail');
        });
    }

    function login() {
      authService.login($scope.user, function () {
        $mdDialog.cancel();
        $log.log('login success');
      }).then(function() {},
        function () {
          self.merrorMessage = 'Пароль или электронная почта введены не верно';
          $log.log('login fail');
        });
    }

    function forgot() {
      authService.forgot($scope.user)
        .then(function () {
            $mdDialog.cancel();
            $log.log('forgot success');
            $timeout(function () {
              alertForgot();
            });
          },
          function () {
            $log.log('forgot fail');
          });
    }

  }
}());