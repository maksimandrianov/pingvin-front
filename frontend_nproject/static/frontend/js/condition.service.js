/**
 * Created by maks on 09.09.16.
 */
(function () {

  'use strict';

  angular.module('pingvinApp')
    .service('conditionService', ['$rootScope', '$http', '$log', 'conditionType', ConditionService])
    .constant('conditionType', {
      thanks: 1,
      money: 2,
      other: 3
    });

  function ConditionService($rootScope, $http, $log, conditionType) {
    var self = this;

    self.prepareCondition = prepareCondition;
    self.mtoString        = mtoString;
    self.typeCondToStrint = typeCondToStrint;
    self.icon             = icon;

    function prepareCondition(condition) {
      switch (parseInt(condition.type, 10)) {
        case conditionType.thanks:
          if (condition.hasOwnProperty('money')) {
            delete condition.money;
          }
          if (condition.hasOwnProperty('other')) {
            delete condition.other;
          }
          break;
        case conditionType.money:
          if (condition.hasOwnProperty('other')) {
            delete condition.other;
          }
          break;
        case conditionType.other:
          if (condition.hasOwnProperty('money')) {
            delete condition.money;
          }
          break;
        default:
          $log.log('_prepareCondition default');
          break;
      }

      if (condition.hasOwnProperty('id')) delete condition.id;
    }

    function icon(condition) {
      if (angular.isUndefined(condition))
        return '';
      switch (parseInt(condition.type, 10)) {
        case conditionType.thanks:
          return 'wb_sunny';
        case conditionType.money:
          return 'attach_money';
        case conditionType.other:
          return 'swap_vert';
        default:
          return 'default';
      }
    }

    function typeCondToStrint(type) {
      var t = parseInt(type, 10);
      switch (t) {
        case conditionType.thanks:
          return 'благодарность';
        case conditionType.money:
          return 'денежная';
        case conditionType.other:
          return 'другая';
        default:
          return 'default';
      }
    }

    function mtoString(condition) {
      if (!condition) return 'благодарность';
      var t = parseInt(condition.type, 10);
      switch (t) {
        case conditionType.thanks:
          return 'благодарность';
        case conditionType.money:
          return 'деньги: ' + condition.money + ' руб.';
        case conditionType.other:
          return condition.other;
        default:
          return 'default';
      }
    }



  }

}());