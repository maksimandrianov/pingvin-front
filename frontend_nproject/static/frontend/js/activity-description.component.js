/**
 * Created by maks on 19.02.17.
 */

(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .component('activityDescription', {
      templateUrl: '/public/html/activity-description.html',
      controller: ['$scope', '$compile', '$log', '$rootScope',
        'utilService', 'activityType', 'conditionService', 'contactsService',
        ActivityDescriptionController],

      bindings: {
        activity: '<'
      }
    })
    .directive('dynamic', ['$compile', '$log', function ($compile, $log) {
      return {
        restrict: 'A',
        replace: true,
        link: function (scope, ele, attrs) {
          scope.$watch(attrs.dynamic, function(html) {
            ele.html(html);
            $compile(ele.contents())(scope);
          });
        }
      };
    }]);

  function ActivityDescriptionController($scope, $compile, $log, $rootScope, utilService,
                                         activityType, conditionService, contactsService) {
    var self = this;

    self.title          = undefined;
    self.subhead        = undefined;
    self.isHide         = true;
    self.getDescription = contactsService.getDescription;

    self.$onInit        = init;

    function init() {
      self.title = title();
      self.subhead = subhead();
    }


    function fullName(user) {
      return utilService.fullName(user);
    }

    function mtoString(condition) {
      return conditionService.mtoString(condition);
    }

    function icon(condition) {
      return conditionService.icon(condition);
    }

    function userLink(user) {
      return '<a class="link link_blue" href="/user/'+user.id +'">'+fullName(user)+'</a>';
    }

    function subheadText(text) {
      var iconHtml = !!self.activity.condition ?
      '<i class="material-icons icon-wrap__icon activity-descr-sub-head__icon">' +
      icon(self.activity.condition) + '</i>' : '';
      return '<span class="activity-descr-sub-head">' + iconHtml +
        text + mtoString(self.activity.condition) + '</span>';
    }

    function title() {
      var result;
      var helper = self.activity.user;
      var createdUser = self.activity.post.created_user;
      switch (self.activity.status) {
        case activityType.newHelper:
          result = 'Вам хочет помочь ' + userLink(helper) + '.';
          break;
        case activityType.updateCondition:
          result = userLink(helper) + ' обновил мотивацию.';
          break;
        case activityType.selectHelper:
          result = userLink(createdUser) + ' выбрал вас помощником.';
          break;

        case activityType.cancelHelper:
          result = userLink(createdUser) + ' отклонил вашу помощь.';
          break;
        case activityType.finishHelp:
          result = 'Поздравляем! Вас отметили как успешно оказавшего помощь.';
          break;
        default:
          result = '';
      }
      return '<h5 class="md-subhead">' + result + '</h5>';
    }

    function subhead() {
      var result;
      switch (self.activity.status) {
        case activityType.newHelper:
          result = subheadText('С мотивацией - ');
          break;
        case activityType.updateCondition:
          result = subheadText('Новая мотивация - ');
          break;
        case activityType.selectHelper:
          result = subheadText('Вы предложили мотивацию - ');
          break;
        default:
          result = '';
      }
      return '<div class="md-body-1">' + result + '</div>';
    }
  }

}());
