/**
 * Created by maks on 03.04.17.
 */


(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('PostNewCategoryController', ['$scope', 'categoryService', PostNewCategoryController]);


  function PostNewCategoryController($scope, categoryService) {
    var self = this;
    self.categories = categoryService.categories;
    self.getTitle = categoryService.getTitle;
    self.category = {
      'category_type': -1,
      'subcategory_type': -1
    };
    self.save = function () {
      $scope.$emit('categoryIsReady', self.category);
    };
  }

}());