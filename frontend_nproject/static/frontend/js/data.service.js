/**
 * Created by maks on 10.01.17.
 */
(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .service('dataService', ['$rootScope', '$q', '$http', '$log', 'authService', DataService]);

  function DataService($rootScope, $q, $http, $log, authService) {
    var self = this;

    self.user = {};
    $log      = $log.getInstance('DataService');


    self.users = function () {
      return {
        list: chain('/api/users-get/', 'users')(),
        update: function (id, data) { return setData('/api/user-update/' + id + '/', 'user update').post(data) },
        get: function (id) { return detail('/api/user-get/', 'user').get(id) }
      };
    };

    self.posts = function () {
      $log.log(chain('/api/posts-get/', 'posts get')());
      return {
        list: chain('/api/posts-get/', 'posts get')(),
        create: setData('/api/post-new/', 'post new'),
        get: function (id) { return detail('/api/post-get/', 'post').get(id) }
      };
    };

    self.blogPosts = function () {
      $log.log(chain('/api/blog-posts-get/', 'blog-posts get')());
      return {
        list: chain('/api/blog-posts-get/', 'blog-posts get')(),
        create: setData('/api/blog-posts-new/', 'blog-posts new'),
        get: function (id) { return detail('/api/blog-post-get/', 'blog post get').get(id) },
        del: function (id) { return detail('/api/blog-post-del/', 'blog posts del').del(id) }
      };
    };

    self.events = function () {
      return {
        list: chain('/api/events-get/', 'events get')(),
        create: setData('/api/event-new/', 'event new'),
        get: function (id) { return detail('/api/event-get/', 'event').get(id) }
      };
    };

    self.usersSearch = function (params) {
      return {
        list: chain('/api/users-search/?' + params, 'users search')()
      };
    };

    self.postsSearch = function (params) {
      return {
        list: chain('/api/posts-search/?' + params, 'posts search')(),
        get: function (id) { return detail('/api/post-get/', 'post').get(id) }
      }
    };

    self.helpers = function (id) {
      return {
        list: chain('/api/helpers-get/' +id + '/', 'helpers get')()
      };
    };

    self.pcomments = function (id) {
      return {
        list: chain('/api/pcomments-get/' +id + '/', 'list comment')(),
        create: setData('/api/pcomment-new/' + id + '/', 'create comment')
      };
    };

    self.ucomments = function (id) {
      return {
        list: chain('/api/ucomments-get/' +id + '/', 'list comment')()
      };
    };

    self.settings = function () {
      return {
        get: function (id) { return detail('/api/settings-get/', 'settings get').get(id) },
        update: function (id, data) { return setData('/api/settings-update/' + id + '/', 'settings update').post(data) }
      };
    };

    self.activity = function () {
      return {
        list: chain('/api/replies-get/', 'activity')(),
        count :  function () { return $http.get('/api/replies/count/').then(function (response) { return response.data }) }
      };
    };

    self.myPosts = function () {
      return {
        list: chain('/api/my-posts-get/', 'my posts')()
      };
    };

    self.myHelpPosts = function () {
      return {
        list: chain('/api/my-help-posts-get/', 'my help posts')()
      };
    };

    self.feedback = function () {
      return {
        list: chain('/api/feedback-get/', 'list feedback')(),
        create: setData('/api/feedback-new/', 'create feedback')

      };
    };

    self.adminPostsModeration = function () {
      return {
        list: chain('/api/admin-posts-moderation-get/', 'admin posts moderation')()
      };
    };

    self.adminPostsClosed = function () {
      return {
        list: chain('/api/admin-posts-closed-get/', 'admin posts closed')()
      };
    };

    init();

    function init() {
      $rootScope.$on('auth', function (ev, data) {
        if (!data && !data.hasOwnProperty('state')) return true;
        if (data.state) {
          $log.log('user methods init');
          userMethodsInit();
        } else {
          $log.log('user methods destruct');
          userMethodsDestruct();
        }

      });
    }

    function userMethodsDestruct() {
      self.user = {};
    }

    function userMethodsInit() {
      var user = authService.user,
        u = function(url) { return '/api/user-method/' + user.id + url };

      self.user.likePost = function (id) {
        return setData(u('/like-post/'), 'post like').post({'post':id});
      };

      self.user.dislikePost = function (id) {
        return setData(u('/dislike-post/'), 'post dislike').post({'post':id});
      };

      self.user.likeBlogPost = function (id) {
        return setData(u('/like-blog-post/'), 'blog-post like').post({'blog_post':id});
      };

      self.user.dislikeBlogPost = function (id) {
        return setData(u('/dislike-blog-post/'), 'post dislike').post({'blog_post':id});
      };

      self.user.help = function (id, condition) {
        return setData(u('/help/'), 'help').post({'post':id, 'condition': condition});
      };

      self.user.updateCondition = function (id, condition) {
        return setData(u('/update-condition-help/'), 'update condition').post({'post':id, 'condition': condition});
      };

      self.user.close = function (id) {
        return setData(u('/close/'), 'close post').post({'post':id});
      };

      self.user.unblock = function (id) {
        return setData(u('/unblock/'), 'unblock post').post({'post':id});
      };

      self.user.selectHelper = function (id, userId) {
        return setData(u('/select-helper/'), 'select helper').post({'post':id, 'user': userId});
      };

      self.user.cancelHelper = function (id, userId) {
        return setData(u('/cancel-helper/'), 'cancel helper').post({'post':id, 'user': userId});
      };

      self.user.done = function (id, userId, cmnt) {
        return setData(u('/cselect-successfully/'), 'cancel helper').post({'post':id, 'user': userId, 'comment': cmnt});
      };

      self.user.moderationOver = function (id) {
        return setData(u('/moderation-over/'), 'moderation over post').post({'post':id});
      };

      self.user.promptFeedback = function () {
        return setData(u('/prompt-feedback/'), 'prompt-feedback').post();
      };

      self.user.promptPublications = function () {
        return setData(u('/prompt-publications/'), 'prompt-publications').post();
      };

      self.user.promptHelps= function () {
        return setData(u('/prompt-helps/'), 'prompt-helps').post();
      };

      self.user.closePromptFeedback= function () {
        return setData(u('/close-prompt-feedback/'), 'close-prompt-feedback').post();
      };

      self.user.closePromptPublications= function () {
        return setData(u('/close-prompt-publications/'), 'close-prompt-publications').post();
      };

      self.user.closePromptHelps= function () {
        return setData(u('/close-prompt-helps/'), 'close-prompt-helps').post();
      };

      self.user.skipPromptFeedback= function () {
        return setData(u('/skip-prompt-feedback/'), 'skip-prompt-feedback').post();
      };

      self.user.skipPromptPublications= function () {
        return setData(u('/skip-prompt-publications/'), 'skip-prompt-publications').post();
      };

      self.user.skipPromptHelps= function () {
        return setData(u('/skip-prompt-helps/'), 'skip-prompt-helps').post();
      };

    }

    function  setData(url, name) {
      function post(data) {

        return $http.post(url, data)
          .then(complete, failed);
      }

      function get() {
        return $http.get(url)
          .then(complete, failed);
      }

      function complete(response) {
        var data = response.data;
        if (data && data.hasOwnProperty('error')) {
          failed(response);
          return $q.reject(data);
        }
        return data;
      }

      function failed(error) {
        $log.error('m: post - ' + url +' failed with data', error.data , ' code ' + error.status);
      }

      return {
        post: post,
        get: get
      };
    }



    function detail(url, name) {

      function get(id) {
        return $http.get(url + id + '/')
          .then(complete, failed)
      }

      function del(id) {
        return $http.post(url + id + '/')
          .then(complete, failed)
      }

      function complete(response) {
        var data = response.data;
        if (data && data.hasOwnProperty('error')) {
          failed(response);
          return $q.reject(data);
        }
        return data;
      }

      function failed(error) {
        $log.error('m: get - '+ url +' failed with data', error.data , ' code ' + error.status);
      }

      return {
        get: get,
        del: del
      };
    }


    function chain(baseUrl, name) {

      return function () {
        var url = baseUrl,
          nextUrl = url;

        function init() {
          nextUrl = url;
        }

        function hasNext() {
          return nextUrl !== null;
        }

        function next() {
          return $http.get(nextUrl)
            .then(complete, failed);
        }

        function complete(response) {
          var data = response.data;
          if (data && data.hasOwnProperty('error')) {
            failed(response);
            return $q.reject(data);
          }
          nextUrl = data.next;
          return data.results;
        }

        function failed(error) {
          $log.error('m: get - '+ url +' failed with data', error.data , ' code ' + error.status);
        }

        return {
          next: next,
          init: init,
          hasNext: hasNext
        };

      };
    }
  }
}());