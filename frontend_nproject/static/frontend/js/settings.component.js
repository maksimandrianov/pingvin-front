/**
 * Created by maks on 09.09.16.
 */

(function () {
  'use strict';

  angular.module('pingvinApp').component('settings', {
    templateUrl: '/public/html/settings.html',
    controller: ['$scope', '$rootScope', '$log', 'utilService',
      '$location', '$routeParams', '$q', 'authService', 'categoryService',
      'contactsService', 'dataService', SettingsController]
  });

  function SettingsController($scope, $rootScope, $log, utilService,
                              $location, $routeParams, $q,
                              authService, categoryService, contactsService, dataService) {

    var self            = this;

    self.isAllCategories = undefined;
    self.availCategories = [];
    self.categOnlyFlags  = [];

    self.oisAllCategories = undefined;
    self.ocategOnlyFlags  = [];

    self.isAllContacts   = undefined;
    self.availContacts   = [];
    self.contOnlyFlags   = [];

    self.oisAllContacts   = undefined;
    self.ocontOnlyFlags   = [];

    self.hasLocation    = undefined;
    self.ohasLocation   = undefined;
    self.lat            = undefined;
    self.lng            = undefined;

    self.isOnlyCustomHeader = undefined;
    self.oisOnlyCustomHeader = undefined;

    self.checkContFlags = checkContFlags;
    self.checkCategFlags= checkCategFlags;
    self.save           = save;

    self.getTitle       = categoryService.getTitle;
    self.getDescription = contactsService.getDescription;

    self.toast          = utilService.initToast();
    self.dataSettings   = dataService.settings();
    $scope.$rootScope   = $rootScope;

    init();

    function init() {
      $rootScope.showProgress = true;

      authService.ifAuth(function () {
        var id = authService.user.id;
        $q.all([
          dataService.users().get(id),
          self.dataSettings.get(id)
        ]).then(function(datas) {

          var user = datas[0],
            settings = datas[1];

          self.availCategories = user.categories;
          self.availContacts = user.contacts;

          initFromSettings(settings);
          initOvalues();

          $scope.$watch(function () {
              return self.hasLocation
            }.bind(this),
            function (value) {
              if (!value) {
                self.lat = '0.0';
                self.lng = '0.0';
              }
            });
          $rootScope.$broadcast('reloadCtrl', {state: 'url-settings'});
          $rootScope.showProgress = false;
        });
      });

      $scope.$on('$locationChangeStart', function (event, newUrl) {
        if (isChange()) {
          utilService.confirmNotSaved()
            .then(function () { initOvalues() ;$location.$$parse(newUrl); });
          event.preventDefault();
        }
      });
    }

    function initFromSettings(settings) {
      self.categOnlyFlags.resize(settings.categories.length, false);
      self.contOnlyFlags.resize(settings.contacts.length, false);

      self.isAllCategories = !settings.categories.length;
      self.isAllContacts = !settings.contacts.length;

      self.availCategories.forEach(function (obj1, index) {
        self.categOnlyFlags[index] = settings.categories.some(function (obj2) {
          return angular.equals(obj1, obj2);
        });
      });

      self.availContacts.forEach(function (obj1, index) {
        self.contOnlyFlags[index] = settings.contacts.some(function (obj2) {
          return angular.equals(obj1, obj2);
        });
      });

      self.isOnlyCustomHeader = settings.only_custom_header;

      self.lat = settings.location.point.latitude;
      self.lng = settings.location.point.longitude;
      self.hasLocation = !(self.lat === '0.0' && self.lng === '0.0');
    }

    function initOvalues() {
      self.oisAllCategories = self.isAllCategories;
      self.oisAllContacts = self.isAllContacts;
      self.ohasLocation = self.hasLocation;
      self.oisOnlyCustomHeader = self.isOnlyCustomHeader;
      angular.copy(self.categOnlyFlags, self.ocategOnlyFlags);
      angular.copy(self.contOnlyFlags, self.ocontOnlyFlags);
    }

    function isChange() {
      return !(
        self.oisOnlyCustomHeader === self.isOnlyCustomHeader &&
        self.isAllCategories === self.oisAllCategories &&
        self.isAllContacts === self.oisAllContacts &&
        self.hasLocation === self.ohasLocation  &&
        angular.equals(self.categOnlyFlags, self.ocategOnlyFlags) &&
        angular.equals(self.contOnlyFlags, self.ocontOnlyFlags)
      );
    }

    function checkContFlags(val) {
      if (val) return;
      for (var i = 0; i < self.contOnlyFlags.length; ++i) {
        if (self.contOnlyFlags[i]) return;
      }
      self.isAllContacts = true;
    }

    function checkCategFlags(val) {
      if (val) return;
      for (var i = 0; i < self.categOnlyFlags.length; ++i) {
        if (self.categOnlyFlags[i]) return;
      }
      self.isAllCategories = true;
    }

    function prepareContacts() {
      var resultCantacts = [];
      if (!self.isAllContacts) {
        self.contOnlyFlags.forEach(function (obj, index) {
          if (obj) resultCantacts.push(self.availContacts[index].id);
        });
      }
      return resultCantacts;
    }

    function prepareCategories() {
      var resultCategories = [];
      if (!self.isAllCategories) {
        self.categOnlyFlags.forEach(function (obj, index) {
          if (obj) resultCategories.push(self.availCategories[index].id);
        });
      }
      return resultCategories;
    }

    function save(location) {

      var data = {
        categories_ids: prepareCategories(),
        contacts_ids: prepareContacts(),
        only_custom_header: self.isOnlyCustomHeader
      };

      if (angular.isDefined(location)) data.location = location;

      checkContFlags(false);
      checkCategFlags(false);

      self.dataSettings.update(authService.user.id, data)
        .then(function (data) {
          initFromSettings(data);
          initOvalues();
          self.toast('Данные успешно сохранены.');
        }, utilService.dHandleError(self.toast));
    }
  }
}());