/**
 * Created by maks on 04.08.17.
 */



(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .component('resetPassword', {
      templateUrl: '/public/html/reset-password.html',
      controller: ['$scope', '$log', '$rootScope', '$timeout', '$location', 'authService',
        'utilService', ResetPasswordController]

    });


  function ResetPasswordController($scope, $log, $rootScope, $timeout, $location, authService,
                                   utilService) {
    var self = this;

    self.merrorMessage  = '';
    self.clear          = clear;
    self.changePassword = changePassword;
    self.password       = '';
    self.rePassword     = '';
    self.toast          = utilService.initToast();

    var uid, token;

    init();

    function init() {
      $rootScope.$broadcast('reloadCtrl', {state: 'url-reset-password'});
      $rootScope.showProgress = false;
      var params = $location.search();
      uid = params.uid;
      token = params.token;
      $location.search('uid', null);
      $location.search('token', null);
    }

    function clear() {
      self.merrorMessage = '';
    }

    function changePassword() {
      if (angular.isDefined(self.password) &&  (self.password !== self.rePassword)) {
        self.merrorMessage = 'Указанные пароли не совпадают!';
        return;
      }
      var data = {
        uid: uid,
        token: token,
        new_password: self.password.trim(),
        re_new_password: self.rePassword.trim()
      };
      authService.confirmPassword(data)
        .then(function () {
          self.toast('Пароль успешно изменен');
          $location.path('/posts/');
          $timeout(function () {
            $location.search('login', '');
          }, 50);
        }, function () {
          self.toast('Что-то пошло не так...');
        })
    }
  }
}());
