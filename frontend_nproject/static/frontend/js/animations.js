/**
 * Created by maks on 17.09.16.
 */
(function () {
  'use strict';



  angular.module('pingvinApp')
    .animation('.slide-toggle', ['$animateCss', function ($animateCss) {
      var lastId = 0;
      var _cache = {};

      function getId(el) {
        var id = el[0].getAttribute("data-slide-toggle");
        if (!id) {
          id = ++lastId;
          el[0].setAttribute("data-slide-toggle", id);
        }
        return id;
      }
      function getState(id) {
        var state = _cache[id];
        if (!state) {
          state = {};
          _cache[id] = state;
        }
        return state;
      }

      function generateRunner(closing, state, animator, element, doneFn) {
        return function () {
          state.animating = true;
          state.animator = animator;
          state.doneFn = doneFn;
          animator.start().finally(function() {
            if (closing && state.doneFn === doneFn) {
              element[0].style.height = '';
            }
            state.animating = false;
            state.animator = undefined;
            state.doneFn();
          });
        }
      }

      return {
        addClass: function (element, className, doneFn) {
          if (className === 'ng-hide') {
            var state = getState(getId(element));
            var height = (state.animating && state.height) ?
              state.height : element[0].offsetHeight;

            var animator = $animateCss(element, {
              from: {height: height + 'px', opacity: 1},
              to: {height: '0px', opacity: 0}
            });
            if (animator) {
              if (state.animating) {
                state.doneFn =
                  generateRunner(true,
                    state,
                    animator,
                    element,
                    doneFn);
                return state.animator.end();
              }
              else {
                state.height = height;
                return generateRunner(true,
                  state,
                  animator,
                  element,
                  doneFn)();
              }
            }
          }
          doneFn();
        },
        removeClass: function(element, className, doneFn) {
          if (className === 'ng-hide') {
            var state = getState(getId(element));
            var height = (state.animating && state.height) ?
              state.height : element[0].offsetHeight;

            var animator = $animateCss(element, {
              from: {height: '0px', opacity: 0},
              to: {height: height + 'px', opacity: 1}
            });

            if (animator) {
              if (state.animating) {
                state.doneFn = generateRunner(false,
                  state,
                  animator,
                  element,
                  doneFn);
                return state.animator.end();
              }
              else {
                state.height = height;
                return generateRunner(false,
                  state,
                  animator,
                  element,
                  doneFn)();
              }
            }
          }
          doneFn();
        }
      };
    }])
    .directive('toggleParent', function() {
      return {
        restrict: 'C',
        compile: function(element, attr) {
          return function(scope, element) {
            element.on('click', function(event) {
              $(this).closest('.slide-toggle').css('height', 'auto');
            });
          }
        }
      }
    });
}());



//<md-toolbar>
//<div class="md-toolbar-tools">
//  <span>Добавить описание</span>
//<span flex></span>
//<md-button ng-init="expand=true;" ng-click="expand = !expand">
//  <span ng-if="!expand">Открыть</span>
//  <span ng-if="expand">Свернуть</span>
//  </md-button>
//  </div>
//  </md-toolbar>
//
//  <md-content layout-padding class="slide-toggle" ng-show="expand"
//style="background-color: #E8EAF6; border-radius: 0 0 4px 4px">
//
//
//  <div layout="row" layout-align="start center">
//  <h3 layout="column" class="au-title-f">Заголовок:</h3>
//<div layout="row" flex>
//<md-input-container flex>
//<label>Заголовок</label>
//<input ng-model="ctrl.user.first_name">
//  </md-input-container>
//  </div>
//  </div>
//
//  <div layout="row" layout-align="start start">
//  <h3 layout="column" class="au-title-f">Описание:</h3>
//<div layout="row" flex>
//<md-input-container flex>
//<label>Описание</label>
//<textarea ng-model="ctrl.user.my_info"
//md-maxlength="2048" rows="1" cols="45"
//style="box-sizing: border-box; width:100%;">
//
//  </textarea>
//  </md-input-container>
//  </div>
//  </div>
//
//  </md-content>