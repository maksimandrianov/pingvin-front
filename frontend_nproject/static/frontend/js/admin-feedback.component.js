/**
 * Created by maks on 26.07.17.
 */

(function () {
  'use strict';

  angular.module('pingvinApp')
    .component('adminFeedback', {
      templateUrl: '/public/html/admin-feedback.html',
      controller: ['$scope', '$rootScope', '$log', '$timeout',
        '$controller', '$mdDialog', '$location', 'utilService', 'dataService',
        'feedbackConst', AdminFeedbackController
      ]});

  function AdminFeedbackController($scope, $rootScope, $log, $timeout, $controller,
                                   $mdDialog, $location, utilService, dataService, feedbackConst) {
    var self = this;

    angular.extend(self, $controller('BaseListController', {
      $scope: $scope,
      $rootScope: $rootScope,
      $log: $log,
      $timeout: $timeout
    }));

    self.msgNoInfo              = 'Пока никто не оставил обратной связи';
    self.img                    = utilService.originalImg;
    self.header                 = header;
    self.fdate                  = utilService.fdate;
    $log                        = $log.getInstance('AdminFeedbackController');


    init();

    function init() {
      self.BaseListController({
        data: dataService.feedback()
      });
    }


    function header(item) {
      switch (parseInt(item.type, 10)) {
        case feedbackConst.error: return 'Ошибка';
        case feedbackConst.advice: return 'Предложение по улучшению';
        case feedbackConst.question: return 'Вопрос';
        case feedbackConst.simple: return 'Отзыв';
        default: return '';
      }
    }

  }
}());