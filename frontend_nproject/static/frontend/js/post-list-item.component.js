/**
 * Created by maks on 30.08.16.
 */

// todo: not use filter https://codepen.io/martinwolf/pen/qlFdp
(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .component('postListItem', {
      templateUrl: '/public/html/post-list-item.html',
      controller: ['$scope', '$log', '$rootScope', '$timeout', 'utilService',
        PostListItemController],

      bindings: {
        post: '<',
        header: '<',
        detail: '<',
        activity: '<'
      }
    }).directive('imageonload', ['$rootScope', '$log', function ($rootScope, $log) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.bind('load', function() {
          $rootScope.$broadcast('masonry.reload');
          $log.log('image be loaded');
        });
        element.bind('error', function(){
          $log.log('image could not be loaded');
        });
      }
    };
  }]);

  function PostListItemController($scope, $log, $rootScope, $timeout, utilService) {
    var self = this;

    self.creator     = undefined;
    self.openUser    = utilService.openUser;
    self.style       = undefined;
    self.img         = undefined;
    self.nameCreator = undefined;
    self.userAva     = undefined;
    self.fromatDist  = utilService.distanceLabel;
    self.fdate       = utilService.fdate;


    self.$onInit = init;

    function init() {

      self.creator     = self.post.created_user;
      self.nameCreator = utilService.fullName(self.creator);
      self.userAva     = utilService.getUMin(self.creator);

      initHeader();

      $scope.$on('likeChange' + self.post.id, function (ev, data) {
        if (data && data.hasOwnProperty('like') && data.hasOwnProperty('dislike') &&
          data.hasOwnProperty('is_like')) {
          self.post.like = data.like;
          self.post.dislike = data.dislike;
          self.post.is_like = data.is_like;
        } else {
          $log.error('likeChange' + self.post.id + ' with data ', data);
        }
      });

      $scope.$on('helpersChange' + self.post.id, function (ev, data) {
        if (data && data.hasOwnProperty('is_help') && data.hasOwnProperty('helpers')) {
          self.post.is_help = data.is_help;
          self.post.helpers = data.helpers;
        } else {
          $log.error('helpersChange' + self.post.id + ' with data ', data);
        }
      });
    }

    function initHeader() {
      var post = self.post;
      if (post.hasOwnProperty('h_style') && post.h_style) {
        var hStyle = post.h_style;
        if (hStyle.hasOwnProperty('b_color') && hStyle.b_color) {
          self.style = 'style' + hStyle.b_color;
        } else if (hStyle.hasOwnProperty('img') && hStyle.img && self.header) {
          self.style = '';
          self.img = post.type === 0 ? utilService.getPostPreviewLg(hStyle.img) :
            utilService.getEventPreviewLg(hStyle.img);
        } else {
          self.style = utilService.getStyle(self.post.header);
        }
      } else {
        self.style = utilService.getStyle(self.post.header);
      }
    }
  }

}());
