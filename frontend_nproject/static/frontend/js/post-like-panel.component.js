/**
 * Created by maks on 09.09.16.
 */
(function () {
  'use strict';

  angular.module('pingvinApp')
    .component('postLikePanel', {
      templateUrl: '/public/html/like-panel.html',
      controller: ['$scope', '$rootScope', '$log', 'dataService', 'utilService',
        'authService', PostLikePanel],
      bindings: {
        post: '<',
        showHelpersCastom: '<'
      }
    });

  function PostLikePanel($scope, $rootScope, $log, dataService, utilService, authService) {
    var self = this;

    self.postLike    = undefined;
    self.postDislike = undefined;
    self.showHelpers = undefined;
    self.showShare   = showShare;
    self.getUMinUrl  = utilService.getUMinUrl;
    self.fullName    = utilService.fullName;
    self.openUser    = utilService.openUser;

    self.$onInit = function () { authService.ifAuth(init, init); };

    function init() {
      self.showHelpers = self.showHelpersCastom || showHelpers;
      self.showHelpers = authService.dOnlyAuth(self.showHelpers);
      self.postLike    = authService.dOnlyAuth(postLike);
      self.postDislike = authService.dOnlyAuth(postDislike);
    }

    function postLike() {
      dataService.user.likePost(self.post.id).then(change);
    }

    function  postDislike() {
      dataService.user.dislikePost(self.post.id).then(change);
    }

    function change(data) {
      analize(self.post.is_like, data.like);
      self.post.is_like = data.like;
      $rootScope.$broadcast('likeChange' + self.post.id, {
        is_like: self.post.is_like,
        like: self.post.like,
        dislike: self.post.dislike
      });
    }

    function showHelpers(ev) {
      utilService.dialog({
        controller: 'PostDetailHelpersController',
        templateUrl: '/public/html/post-detail-helpers.html',
        scope: $scope.$new(),
        locals: { post: self.post }
      });
    }

    function showShare(ev) {
      utilService.shareDlg(ev, self.post);
    }

    function analize(old, fresh) {
      if (old === null && fresh === true)
        self.post.like += 1;
      else if (old === null && fresh === false)
        self.post.dislike += 1;
      else if (old === true && fresh === null)
        self.post.like -= 1;
      else if (old === true && fresh === false) {
        self.post.dislike += 1;
        self.post.like -= 1;
      } else if (old === false && fresh === null) {
        self.post.dislike -= 1;
      } else if (old === false && fresh === true) {
        self.post.dislike -= 1;
        self.post.like += 1;
      }
    }
  }

}());