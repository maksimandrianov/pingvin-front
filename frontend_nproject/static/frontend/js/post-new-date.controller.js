/**
 * Created by maks on 03.04.17.
 */


(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('PostNewDateController', ['$scope', PostNewDateController]);


  function PostNewDateController($scope) {
    var self = this;

    self.date_end = new Date();
    self.minDate = new Date(
      self.date_end.getFullYear(),
      self.date_end.getMonth(),
      self.date_end.getDate() + 1
    );
    self.maxDate = new Date(
      self.date_end.getFullYear(),
      self.date_end.getMonth() + 1,
      self.date_end.getDate()
    );

    init();

    function init () {
      self.date_end = self.minDate;
    }

    self.save = function () {
      $scope.$emit('dateIsReady', self.date_end);
    };
  }

}());