/**
 * Created by maks on 09.09.16.
 */
(function () {

  'use strict';

  angular.module('pingvinApp')
    .service('categoryService', ['$rootScope', '$http', '$log', CategoryService]);



  function CategoryService($rootScope, $http, $log) {
    var self = this;

    self.categories          = initCategory();
    self.getTitle            = getTitle;
    self.getTitleFromType    = getTitleFromType;
    self.getSubcategories    = getSubcategories;
    self.getTitleCatFromType = getTitleCatFromType;



    function getTitle(c) {
      var i, j;
      for (i = 0; i < self.categories.length; ++i) {
        if (self.categories[i].type === c.category_type) {
          for (j = 0; j < self.categories[i].subcategories.length; ++j) {
            if (self.categories[i].subcategories[j].type === c.subcategory_type) {
              return self.categories[i].subcategories[j].title;
            }
          }
        }
      }
    }

    function getTitleFromType(coll, type) {
      var tt = parseInt(type, 10);
      for (var i = 0; i < coll.length; ++i) {
        if (coll[i].type === tt)
          return coll[i].title;
      }
      return '';
    }

    function getTitleCatFromType(type) {
      return getTitleFromType(self.categories, type);
    }

    function getSubcategories(type) {
      var tt = parseInt(type, 10);
      for (var i = 0; i < self.categories.length; ++i) {
        if (self.categories[i].type === tt)
          return self.categories[i].subcategories;
      }
      return [];
    }

    function initCategory() {
      return [
        {
          "title": "Компьютерная помощь",
          "type": 0,
          "subcategories": [
            {"id": 0, "title": "Ремонт ноутбуков", "type": 0},
            {"id": 1, "title": "Установка Windows", "type": 1},
            {"id": 2, "title": "Настройка Wi-Fi", "type": 2},
            {"id": 3, "title": "Настройка Интернета", "type": 3},
            {"id": 4, "title": "Удаление вирусов", "type": 4},
            {"id": 5, "title": "Установка программ", "type": 5},
            {"id": 6, "title": "Другая помощь с ПК", "type": 6}
          ]
        },
        {
          "title": "Образование",
          "type": 1,
          "subcategories": [
            {"id": 7, "title": "Рефераты", "type": 0},
            {"id": 8, "title": "Курсовые", "type": 1},
            {"id": 9, "title": "Консультация", "type": 2},
            {"id": 10, "title": "Другое в образовании", "type": 3}
          ]
        },
        {
          "title": "Курьерская помощь",
          "type": 2,
          "subcategories": [
            {"id": 11, "title": "Курьерская доставка", "type": 0},
            {"id": 12, "title": "Грузовые перевозки", "type": 1},
            {"id": 13, "title": "Доставка продуктов", "type": 2},
            {"id": 14, "title": "Другая курьерская помощь", "type": 3}
          ]
        },
        {
          "title": "Помощь по дому",
          "type": 3,
          "subcategories": [
            {"id": 15, "title": "Сантехника", "type": 0},
            {"id": 16, "title": "Электрика", "type": 1},
            {"id": 17, "title": "Ремонт квартир", "type": 2},
            {"id": 18, "title": "Ремонт мебели", "type": 3},
            {"id": 19, "title": "Сборка мебели", "type": 4},
            {"id": 20, "title": "Другая помощь по дому", "type": 5}
          ]
        },
        {
          "title": "Ремонт техники",
          "type": 4,
          "subcategories": [
            {"id": 21, "title": "Ремонт бытовой техники", "type": 0},
            {"id": 22, "title": "Ремонт цифровой техники", "type": 1},
            {"id": 23, "title": "Ремонт электроинструментов", "type": 2},
            {"id": 24, "title": "Заправка картриджей", "type": 3},
            {"id": 25, "title": "Другой ремонт техники", "type": 4}
          ]
        },
        {
          "title": "Развлечения",
          "type": 5,
          "subcategories": [
            {"id": 26, "title": "Тамада", "type": 0},
            {"id": 27, "title": "Фотограф", "type": 1},
            {"id": 28, "title": "Экскурсия", "type": 2},
            {"id": 29, "title": "Видеооператор", "type": 3}
          ]
        },
        {
          "title": "Уборка",
          "type": 6,
          "subcategories": [
            {"id": 30, "title": "Генеральная уборка", "type": 0},
            {"id": 31, "title": "Уборка квартир", "type": 1},
            {"id": 32, "title": "Уборка офисов", "type": 2},
            {"id": 33, "title": "Другая уборка", "type": 3}
          ]
        }
      ];
    }

  }

}());