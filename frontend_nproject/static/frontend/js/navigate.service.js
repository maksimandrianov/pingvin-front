/**
 * Created by maks on 20.08.16.
 */
(function () {
  'use strict';

  angular.module('pingvinApp').service('navigateService', ['$q', '$log', NavigateService]);

  function NavigateService($q, $log) {
    var items = [
      {
        name:  'Объявления',
        icon:  'apps',
        first: true,
        url:   '/posts/',
        color: 'teal',
        reg:   false,
        descr: 'Объявления Pingvin.online. ' +
        'Создавайте Ваши объявления о помощи или откликайтесь на объявления других пользователей.'
      },
      {
        name:  'Люди',
        icon:  'people',
        url:   '/users/',
        color: 'blue',
        reg:    false,
        descr: 'Люди Pingvin.online.'
      },
      {
        name:    'События',
        icon:    'event_note',
        divider: true,
        url:     '/events/',
        color:   'deep-orange',
        reg:   false,
        descr: 'События Pingvin.online. ' +
        'Создавайте Ваши события о помощи или откликайтесь на события других пользователей. ' +
        'С помощью событий Вы сможете привлечь больше помощников.'
      },
      {
        name: 'Обо мне',
        icon: 'person',
        url: '/about-me/',
        color: 'green-700',
        reg:   true
      },
      {
        name: 'Моя активность',
        icon: 'view_agenda',
        url:  '/activity/',
        color: 'green-700',
        reg:   true,
        tabs: [
          {
            label:'Обновления',
            value: 'news'
          },
          {
            label: 'Опубликованное мной',
            value: 'my-posts'
          },
          {
            label: 'Хочу помочь',
            value: 'my-helps'
          }
        ],
        param: 'act'
      },
      {
        name: 'Мои настройки',
        icon: 'settings',
        url: '/settings/',
        color: 'green-700',
        reg:   true
      },
      {
        name: 'Обратная связь',
        icon: 'feedback',
        url: '/feedback/',
        color: 'green-700',
        reg:   true
      },
      {
        name:  'Администратор',
        icon:  'star',
        url: '/admin/',
        reg:   true,
        isa:   true,
        param: 'act',
        tabs: [
          {
            label: 'Обратная свзяь',
            value: 'feedback'
          },
          {
            label: 'Модерация',
            value: 'moderation'
          },
          {
            label: 'Закрытые публикации',
            value: 'closed'
          }
        ]
      }
    ];

    return {
      loadNavigateItems : function () {
        return $q.when(items);
      }
    };
  }

}());