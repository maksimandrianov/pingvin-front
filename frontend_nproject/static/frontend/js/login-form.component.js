/**
 * Created by maks on 24.03.17.
 */

(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .component('loginForm', {
      templateUrl: '/public/html/login-form.html',
      controller: 'SignupController'
    });
}());
