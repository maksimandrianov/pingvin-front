/**
 * Created by maks on 18.03.17.
 */

(function () {
  'use strict';

  angular.module('pingvinApp').component('feedback', {
    templateUrl: '/public/html/feedback.html',
    controller: ['$scope', '$log', '$rootScope', '$mdDialog',
      '$location', '$controller', '$timeout', '$routeParams', 'conditionService',
      'activityType', 'dataService', 'utilService', 'Upload', 'feedbackConst',
      FeedbackController]
  }).constant('feedbackConst', {
    error: 0,
    advice: 1,
    question: 2,
    simple: 3
  });

  function FeedbackController($scope, $log, $rootScope, $mdDialog, $location,
                              $controller, $timeout, $routeParams, conditionService,
                              activityType, dataService, utilService, Upload, feedbackConst) {

    var self = this;

    self.toast         = utilService.initToast();
    self.feedbackConst = feedbackConst;
    self.feedbackTypes = [
      {v: self.feedbackConst.error, m: 'Я нашел ошибку в работе сервиса'},
      {v: self.feedbackConst.advice, m: 'У меня есть предложение по улучшению сервиса'},
      {v: self.feedbackConst.question, m: 'Мне кое-что не понятно в работе сервиса, я хочу задать вопрос'}
    ];

    self.feedbackType = self.feedbackConst.question;
    self.message      = '';
    self.browserrInfo = true;
    self.picFile      = undefined;
    self.email        = '';

    self.getText      = getText;
    self.isError      = isError;
    self.isQuestion   = isQuestion;
    self.cancel       = cancel;

    self.sendFeedback = sendFeedback;

    self.progress     = 0;
    self.loading      = false;

    $log              = $log.getInstance('FeedbackController');


    init();

    function init() {
      $rootScope.$broadcast('reloadCtrl', {state: 'url-feedback'});
      $rootScope.showProgress = false;

      var lastFeedbackType;
      $scope.$watch(function() {
        return self.feedbackType;
      }.bind(self), function(newName) {
        if (lastFeedbackType == self.feedbackConst.error) {
          self.browserrInfo = true;
          self.picFile      = undefined;
        } else if (lastFeedbackType == self.feedbackConst.question) {
          self.email = '';
        }
        lastFeedbackType = newName;
      }.bind(self));
    }

    function upload() {
      return Upload.upload({
        url: '/upload/',
        data: {
          file: self.picFile
        }
      }).then(function (response) {
        $timeout(function () {self.loading = false;});
        return response.data;
      }, function (response) {
        if (response.status > 0) $log.error('upload error')
      }, function (evt) {
        self.progress = parseInt(100.0 * evt.loaded / evt.total);
      });
    }

    function clear() {
      self.feedbackType = undefined;
      self.message      = '';
      self.browserrInfo = true;
      self.picFile      = undefined;
      self.email        = '';
    }

    function cancel() {
      self.picFile = undefined;
    }

    function isError() {
      return self.feedbackType === self.feedbackConst.error;
    }

    function isQuestion() {
      return self.feedbackType === self.feedbackConst.question;
    }

    function isSimple() {
      return self.feedbackType === self.feedbackConst.simple;
    }

    function sendFeedback() {
      if (isError() && angular.isDefined(self.picFile)) {
        upload().then(function (data) {
          var urlData = data || {};
          feedback(urlData.original);
          self.progress = 0;
          self.loading = false;
        }, function () {
          self.toast('Ошибка при загрузке файла');
        });
      } else {
        feedback();
      }
    }

    function feedback(url) {
      var data = {},
        msgData = {},
        error = false,
        trimStr = '';
      if (angular.isUndefined(self.feedbackType)) {
        $log.error('Type feedback not selected');
        error = true;
      }

      trimStr = self.message.trim();
      msgData.message = trimStr;

      if (isQuestion()) {
        var email = self.email.trim();
        if (email)
          msgData.email =  email;
      }

      if (isError() && self.browserrInfo && platform && platform.description) {
        msgData.platform  =  platform.description;
      }


      if (!isSimple() && !trimStr.length) {
        $log.error('Message is empty');
        error = true;
      }

      if (error) return;
      msgData.type = self.feedbackType;
      if (isError() && angular.isDefined(url)) {
        msgData.url  =  url;
      }


      dataService.feedback().create
        .post(msgData).then(function success(data) {
        $log.log('returned', data);
        clear();
        self.toast('Отзыв отправлен');
      }, function fail(response) {
        self.toast(response.code);
      });
    }

    function getText() {
      switch (self.feedbackType) {
        case self.feedbackConst.error:
          return 'Вы нашли ошибку? Опишите как можно более подробно что пошло не так. ' +
            'И мы начнем работать над испралением ошибки. ' +
            'Укажите последовательность действий, которая приведет к этой ошибке.';
        case self.feedbackConst.advice:
          return 'Вы знаете как улучшить сервис? Здорово! Мы рассмотрим Ваше предложение и возможно, ' +
            'что оно вступит в силу в ближайшее время. Укажите детали Вашего предложения.';
        case self.feedbackConst.question:
          return 'Что-то осталось не понятным? Мы рады ответить на любой Ваш вопрос по работе сервиса!' +
            ' Укажите почту на которую мы вышлем Вам ответ.';
        default:
          return '';
      }
    }

  }

}());
