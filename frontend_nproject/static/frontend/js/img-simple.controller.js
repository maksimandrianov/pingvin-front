/**
 * Created by maks on 25.09.17.
 */


(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('ImageSimpleController',
      ['$scope', '$timeout', '$log', 'Upload', 'utilService', 'type',
        ImageSimpleController]);

  function ImageSimpleController($scope, $timeout, $log, Upload, utilService, type) {
    var self = this;

    self.current     = 'color';
    self.currCol     = undefined;
    self.color       = true;
    self.image       = false;
    self.isLoad      = false;
    self.colors      = 1;
    self.error       = {};
    self.picFile     = undefined;
    self.cropCoords  = undefined;
    self.progress    = 0;
    self.loadimg     = false;
    self.save        = save;
    self.type        = type;
    self.toast       = utilService.initToast();

    init();

    function init() {
      $scope.$on('ImgCropRegionError', function (ev, data) {
        data = data || {};
        self.error.show = true;
        self.error.msg = data.message || '';
      });
      $timeout(function () {
        self.isLoad = true;
      }, 50);

      $scope.$watch(function () {
        return self.picFile
      }.bind(self), function (newVal) {
        if (newVal)  {
          self.error.show = false;
        }
      });
    }

    function save() {
      if (check()) {
        uploadFiles(self.picFile, self.cropCoords)
          .then(function(data) {
            self.progress = 100;
            self.toast('Фотография загружена');
            $timeout(function () {
              self.loadimg = false;
            });
            data = data || {};
            var img = data.cropped || data.original || '';
            $scope.$emit('imgUploaded', img);
          });
      }
    }

    function uploadFiles(file, coords) {
      self.loadimg = true;
      var data = {file: file};
      var size = utilService.realSize($('.crop-area-post'));
      var isCrop = file.type !== 'image/gif';

      if (isCrop) {
        data.size = JSON.stringify(size);
        data.crop = JSON.stringify(coords);
      }

      return Upload.upload({
        url: '/upload/',
        data: data

      }).then(function (response) {
        return response.data;
      }, function (response) {
        if (response.status > 0) self.errorMsg = response.status
          + ': ' + response.data;
      }, function (evt) {
        self.progress = parseInt(90.0 * evt.loaded / evt.total);
      });
    }


    function check() {
      if (!self.picFile || !self.cropCoords) {
        self.error.show = true;
        self.error.msg = 'Файл не выбран!';
        return false;
      }
      return true;
    }
  }

}());