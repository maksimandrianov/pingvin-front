/**
 * Created by maks on 13.07.17.
 */


(function () {
  'use strict';

  angular.module('pingvinApp').component('help', {
    templateUrl: '/public/html/help.html',
    controller: ['$scope', '$rootScope', '$log', '$timeout', '$location',
      '$mdMenu', '$mdDialog', '$anchorScroll', 'categoryService', HelpController]
  });


  function HelpController($scope, $rootScope, $log, $timeout, $location,
                          $mdMenu, $mdDialog, $anchorScroll, categoryService) {

    var self = this;

    var pageName = 'Помощь - ';

    self.helpList   = [
      {
        name: 'Введение в Pingvin.online',
        value: 1
      },
      {
        name: 'Регистрация, вход и выход',
        value: 2
      },
      {
        name: 'Объявления и события',
        value: 3
      },
      {
        name: 'Отклик на объявления и события',
        value: 4
      },
      {
        name: 'Выбор помощника',
        value: 5
      },
      {
        name: 'Ваш профиль',
        value: 6
      },
      {
        name: 'Настройки',
        value: 7
      },
      {
        name: 'Популярные вопросы',
        value: 8
      }
    ];
    self.categories  = categoryService.categories;
    self.selected    = self.helpList[0];
    self.current     = 0;
    self.selectValue = selectValue;
    self.isSelected  = isSelected;
    self.gotoAnchor  = gotoAnchor;

    $scope.error       = 404;
    $scope.description = 'Страница не найдена';

    $log                     = $log.getInstance('HelpController');

    init();

    function init() {

      $rootScope.showProgress = false;

      initFromUrl();

      $scope.$on('$routeUpdate', function () {
        initFromUrl();
      });
    }

    function initFromUrl() {
      var q, params = $location.search(),
        descr = 'Помощь в использовании сайта Pingvin.online. Найдите ответ или задайте вопрос.';

      if (angular.isUndefined(params.q)) {
        $rootScope.$broadcast('reloadCtrl', {
          state: 'url-help',
          name: pageName + self.helpList[0].name,
          descr: descr
        });
        self.selected    = self.helpList[0];
        self.current     = 0;
        return;
      }
      q = parseInt(params.q);
      var min_ = self.helpList[0].value;
      var max_ = self.helpList[self.helpList.length-1].value;
      if (!(q >= min_ && q <= max_)) {
        $log.warn('pad params q', q);
        reset();
        return;
      }
      self.selected = self.helpList[q-1];
      self.current = q-1;

      $rootScope.$broadcast('reloadCtrl', {
        state: 'url-help',
        name: pageName + self.selected.name,
        descr: descr
      });
    }

    function reset() {
      self.selected    = undefined;
      self.current     = undefined;
    }

    function selectValue(v) {
      self.selected = v;
    }

    function isSelected(v) {
      return self.selected === v;
    }

    function gotoAnchor(x) {
      var newHash = 'anchor' + x;
      if ($location.hash() !== newHash) {
        $location.hash('anchor' + x);
      } else {
        $anchorScroll();
      }
    }

  }
}());