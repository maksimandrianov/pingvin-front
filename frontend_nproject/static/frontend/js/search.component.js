/**
 * Created by maks on 07.07.17.
 */

(function () {
  'use strict';

  angular.module('pingvinApp').component('search', {
    templateUrl: '/public/html/search.html',
    controller: ['$scope', '$rootScope', '$log', '$timeout', '$location',
      '$mdMenu', '$mdDialog', 'debounce', 'categoryService', 'dataService',
      'conditionService', SearchController]
  });


  function SearchController($scope, $rootScope, $log, $timeout, $location,
                            $mdMenu, $mdDialog, debounce, categoryService, dataService,
                            conditionService) {

    var self                = this;

    var searchParams = {},
      oldSearchParams = {};

    self.searchString        = '';
    self.searchw             = debounce(search, 500, false);
    self.searchInputChange   = searchInputChange;
    self.searchContextChange = searchContextChange;
    self.searchOptHtml       = '';
    self.contentHtml         = '';
    self.options             = {};
    self.removeChip          = removeChip;

    self.searchOptions       = [];

    self.context             = 1;
    self.contexts            = [
      {val: 1, name: 'Объявления'},
      {val: 2, name: 'События'},
      {val: 3, name: 'Люди'}
    ];


    $scope.$mdMenu           = $mdMenu;

    $log                     = $log.getInstance('SearchController');


    init();

    function init() {
      $rootScope.$broadcast('reloadCtrl', {
        state: 'url-search',
        name: 'Поиск'
      });

      $rootScope.showProgress = false;

      initFromUrl();
      search();
      angular.copy(searchParams, oldSearchParams);

      $timeout(function () {
        angular.element('.page-search-input').focus();
      }, 50);

      $scope.$on('$routeUpdate', function () {
        initFromUrl();
        if (!angular.equals(searchParams, oldSearchParams)) {
          self.searchw();
          $log.info(searchParams, oldSearchParams);
          angular.copy(searchParams, oldSearchParams);
        }
        $mdDialog.cancel();
      });
    }

    function removeChip(chip) {
      $location.search(chip.param, null);
    }

    function saveParams() {
      var params = $location.search();
      searchParams = {};
      searchParams.search = params.search;
      searchParams.context = params.context;
      searchParams.category = params.category;
      searchParams.subcategory = params.subcategory;
      searchParams.motivation = params.motivation;
      searchParams.country = params.country;
      searchParams.city = params.city;
      return searchParams;
    }

    function initFromUrl() {
      saveParams();
      var params = $location.search();
      self.searchString = params.search || '';
      self.context = params.context || 1;
      initFromUrlUser(params);
      initFromPosts(params);
    }

    function initFromPosts(params) {
      if (self.context != 1 && self.context != 2) {
        $location.search('category', null);
        $location.search('subcategory', null);
        $location.search('motivation', null);
      }
      testParam(params, 'motivation',  conditionService.typeCondToStrint);
      testParam(params, 'category', categoryService.getTitleCatFromType);
      testParam(params, 'subcategory', function (type) {
        var subcat = categoryService.getSubcategories(params.category);
        return categoryService.getTitleFromType(subcat, type);
      });
    }

    function initFromUrlUser(params) {
      if (self.context != 3) {
        $location.search('country', null);
        $location.search('city', null);
      }
      testParam(params, 'country');
      testParam(params, 'city');
    }

    function testParam(params, name, func) {
      var element = self.searchOptions.find(getWithParam(name));
      if (angular.isUndefined(params[name]) && element) {
        var index = self.searchOptions.indexOf(element);
        if (index > -1)
          self.searchOptions.splice(index, 1);
      }
      if (angular.isDefined(params[name])) {
        var text = angular.isFunction(func) ? func(params[name]) : params[name];
        if (!element) {
          self.searchOptions.push({
            label: params[name],
            param: name,
            text: text
          })
        } else {
          element.label = params[name];
          element.text = text;
        }
      }
    }

    function getWithParam(param) {
      return function (element, index, array) {
        return element.param === param;
      };
    }

    function searchInputChange() {
      $timeout (function () {
        $location.search('search', self.searchString);
      }, 1000);
    }

    function searchContextChange() {
      $location.search('context', self.context);
      buildSearchOptHtml();
    }

    function buildSearchOptHtml() {
      //self.searchOptHtml = '';
    }

    function buildContentHtml() {
      var contentHtml = self.contentHtml,
        params = '',
        urlParams = $location.search();
      self.contentHtml = '';

      switch (parseInt(self.context, 10)) {
        case 1:
          buildContentHtmlPostsOrEvents(urlParams, params, true);
          break;
        case 2:
          buildContentHtmlPostsOrEvents(urlParams, params);
          break;
        case 3:
          buildContentHtmlUsers(urlParams, params);
          break;
        default:
          $log.log('default, value is', self.context);
      }

      $log.log(self.options);

      if (contentHtml === self.contentHtml)
        self.contentHtml += ' ';  // fake change
    }

    function buildContentHtmlPostsOrEvents(urlParams, params, isPost) {
      if (self.searchString)
        params += '&search=' + self.searchString;
      if (angular.isDefined(urlParams.category))
        params += '&category=' + urlParams.category;
      if (urlParams.subcategory)
        params += '&subcategory=' + urlParams.subcategory;
      if (urlParams.motivation)
        params += '&motivation=' + urlParams.motivation;

      if (isPost) {
        params += '&type=0';
        self.options.msg = 'По вашему запросу не найдено ни одного объявления';
      } else {
        params += '&type=1';
        self.options.msg = 'По вашему запросу не найдено ни одного события';
      }

      if (params.charAt(0) === '&')
        params.substr(1);
      self.options.src = dataService.postsSearch(params);
      self.options.isView = true;
      self.searchOptHtml = '<search-menu-post></search-menu-post>';
      self.contentHtml = '<post-list options="$ctrl.options"></post-list>';
    }

    function buildContentHtmlUsers(urlParams, params) {
      if (self.searchString)
        params += '&name=' + self.searchString;
      if (urlParams.city)
        params += '&city=' + urlParams.city;
      if (urlParams.country)
        params += '&country=' + urlParams.country;
      if (params.charAt(0) === '&')
        params.substr(1);
      self.options.src = dataService.usersSearch(params);
      self.options.msg = 'По вашему запросу не найдено ни одного пользователя';
      self.options.isView = true;
      self.searchOptHtml = '<search-menu-user></search-menu-user>';
      self.contentHtml = '<user-list options="$ctrl.options"></user-list>';
    }

    function search() {
      $log.log('search');
      buildContentHtml();
    }
  }



  angular.module('pingvinApp').factory('debounce', ['$timeout','$q', function($timeout, $q) {
    return function debounce(func, wait, immediate) {
      var timeout;
      var deferred = $q.defer();
      return function() {
        var context = this, args = arguments;
        var later = function() {
          timeout = null;
          if(!immediate) {
            deferred.resolve(func.apply(context, args));
            deferred = $q.defer();
          }
        };
        var callNow = immediate && !timeout;
        if ( timeout ) {
          $timeout.cancel(timeout);
        }
        timeout = $timeout(later, wait);
        if (callNow) {
          deferred.resolve(func.apply(context,args));
          deferred = $q.defer();
        }
        return deferred.promise;
      };
    };
  }]);
}());