/**
 * Created by maks on 28.07.17.
 */

(function () {
  'use strict';

  angular.module('pingvinApp')
    .component('adminClosed', {
      templateUrl: '/public/html/post-list-one-col.html',
      controller: ['$scope', '$rootScope', '$log', '$timeout',
        '$controller', '$mdDialog', '$location', 'utilService', 'dataService',
        AdminPostsClosedController
      ]});

  function AdminPostsClosedController($scope, $rootScope, $log, $timeout, $controller,
                                      $mdDialog, $location, utilService, dataService) {
    var self = this;

    angular.extend(self, $controller('BaseListController', {
      $scope: $scope,
      $rootScope: $rootScope,
      $log: $log,
      $timeout: $timeout
    }));

    self.hasHeader              = false;
    self.currDescription        = false;
    self.isActivity             = false;
    self.msgNoInfo              = 'Пока нет закрытых публикаций';
    self.showDetail             = showDetail;
    self.isOpenDialog           = false;
    $log                        = $log.getInstance('AdminPostsClosedController');


    init();

    function init() {
      self.BaseListController({
        data: dataService.adminPostsClosed()
      });
      initPostFromUrl();
      var urlParams = $location.search();
      $scope.$on('$routeUpdate', function () {
        if (!self.isOpenDialog) {
          initPostFromUrl();
        } else if (!urlParams.hasOwnProperty('post') &&
          !urlParams.hasOwnProperty('event')) {
          $mdDialog.cancel();
        }
      });
      $scope.$on('postClose', function (e, el) {
        utilService.removeUnit(el.id, self.collection);
      });
    }

    function initPostFromUrl() {
      var urlParams = $location.search();
      if (urlParams.post) {
        showDetail(null, urlParams.post, 0);
      } else if (urlParams.event) {
        showDetail(null, urlParams.event, 1);
      }
    }

    function showDetail(ev, id, t) {
      var type, data;
      if (t == 0) {
        type = 'post';
        data = dataService.posts();
      } else if (t == 1) {
        type = 'event';
        data = dataService.events();
      }

      utilService.dialog({
        controller: 'PostDetailController',
        templateUrl: '/public/html/post-detail.html',
        scope: $scope.$new(),
        locals: {
          option: {
            postId: id,
            type: type,
            pData: data,
            isClosed: true
          }
        },
        onShowing: function () {
          self.isOpenDialog = true;
          $location.search(type, id);
        },
        onRemoving: function () {
          self.isOpenDialog = false;
          $location.search(type, null);
        }
      });
    }

  }
}());