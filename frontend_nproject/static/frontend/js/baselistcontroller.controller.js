/**
 * Created by maks on 24.02.17.
 */

(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('BaseListController', ['$scope', '$rootScope', '$log', '$timeout',
      BaseListController
    ]);

  function BaseListController($scope, $rootScope, $log, $timeout) {
    var self = this;

    var showMoreInternal, isShowButtonInternal, isView;

    self.loading            = false;
    self.isWasFisrtQuery    = false;
    self.isEmpty            = isEmpty;
    self.collection         = [];
    self.BaseListController = BaseListController;
    self.isShowButtonInit   = isShowButtonInit;
    self.showMoreInit       = showMoreInit;
    self.showMore           = function (first) { showMoreInternal(first); };
    self.isShowButton       = function () { return isShowButtonInternal(); };
    self.isLoading          = function () {return self.loading; };

    $log                    = $log.getInstance('BaseListController');

    function BaseListController(params) {
      $log.log('init BaseListController');
      isView = params.isView || false;
      var data = params.data,
        preShowMore = params.preShowMore,
        preShowMoreAsync = params.preShowMoreAsync,
        postIsShowBtn = params.postIsShowBtn;
      showMoreInternal = showMoreInit(data, preShowMore,preShowMoreAsync);
      isShowButtonInternal = isShowButtonInit(data, postIsShowBtn);

      data.list.init();
      self.showMore(true);
    }


    function showMoreInternal() {

    }

    function isShowButtonInternal() {
      return false;
    }

    function isEmpty() {
      if (!self.isWasFisrtQuery) return false;
      return !self.collection.length;
    }




    function isShowButtonInit(data, func) {
      var postProcessing = !!func && typeof func == 'function' ?
        func : function () { return true},
        flag = false;
      $timeout(function () {flag=true;}, 500);
      return function() {
        return data.list.hasNext() && self.collection.length !== 0 && flag;
      };
    }

    function showMoreInit(data, func1, func2) {
      var postProcessing = angular.isFunction(func1) ?
        func1 : function (data) { return data };
      return function (first) {
        first = !!first;
        if (self.loading) return;
        if (first && !isView)
          $rootScope.showProgress = true;
        self.loading = true;
        data.list.next()
          .then(function (data) {
            $log.log('showMore call elements:', data);
            Array.prototype.push.apply(self.collection, postProcessing(data));
            $timeout(function () {
              if (first) {
                if (!isView)
                  $rootScope.showProgress = false;
                self.isWasFisrtQuery = true;
              }
              if (angular.isFunction(func2)) func2();
              self.loading = false;
            }, 10);
          }, function () {
            if (first && !isView) $rootScope.showProgress = false;
            self.loading = false;
          });
      };
    }
  }
}());