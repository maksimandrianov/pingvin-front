/**
 * Created by maks on 09.09.16.
 */
(function () {

  'use strict';

  angular
    .module('pingvinApp')
    .constant('contactsType', {
      EMAIL: 1,
      VK: 2,
      FB: 3,
      PH_MOBILE: 4,
      PH_HOME: 5
    })
    .service('contactsService', ['$rootScope', '$http', '$log', 'contactsType',
      ContactsService]);



  function ContactsService($rootScope, $http, $log, contactsType) {
    var self = this;

    self.contacts         = initContacts();
    self.getDescription   = getDescription;



    function getDescription(type) {
      var i;
      for (i = 0; i < self.contacts.length; ++i) {
        if (self.contacts[i].type === type) {
          return self.contacts[i].descr;
        }
      }
    }

    function initContacts() {
      return [
        {
          type: contactsType.EMAIL,
          descr: 'Эл. почта'
        },
        {
          type: contactsType.VK,
          descr: 'Вконтакте'
        },
        {
          type: contactsType.FB,
          descr: 'Facebook'
        },
        {
          type: contactsType.PH_MOBILE,
          descr: 'Мобильный телефон'
        },
        {
          type: contactsType.PH_HOME,
          descr: 'Домашний телефон'
        }
      ];
    }

  }

}());