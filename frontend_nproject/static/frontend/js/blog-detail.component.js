/**
 * Created by maks on 27.09.17.
 */


(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .component('blogDetail', {
      templateUrl: '/public/html/blog-detail.html',
      controller: ['$rootScope', '$scope', '$mdMedia', '$log',
        '$mdDialog', '$routeParams', '$location', '$timeout',
        '$window', '$controller', '$anchorScroll', 'authService', 'dataService',
        'utilService', '$filter', BlogDetailController]
    });

  function BlogDetailController($rootScope, $scope, $mdMedia, $log,
                                $mdDialog, $routeParams, $location, $timeout,
                                $window, $controller, $anchorScroll, authService,
                                dataService, utilService, $filter) {

    var self = this;

    self.post           = undefined;
    self.style          = undefined;
    self.img            = undefined;
    self.avatar         = undefined;
    self.userId         = undefined;
    self.userName       = undefined;
    self.created        = undefined;
    self.header         = undefined;
    self.text           = undefined;
    self.hasError       = undefined;

    self.isa            = undefined;

    self.toast          = utilService.initToast();
    self.removeBlogPost = removeBlogPost;

    init();

    function init() {
      $rootScope.showProgress = true;
      var link = $routeParams.blogId || '?';
      dataService.blogPosts().get(link)
        .then(function (data) {
          $rootScope.showProgress = false;
          self.hasError = false;
          self.post = data;
          initHeader(self.post);
          initPost(self.post);
          $rootScope.$broadcast('reloadCtrl', {
            state: 'url-blog',
            name: self.header,
            descr: ''
          });
        }, function (response) {
          $rootScope.showProgress = false;
          self.hasError = true;
          $scope.error = utilService.getErrorCode(response);
          switch (parseInt($scope.error, 10)) {
            case 404:
              $scope.description = 'Пост блога не найден';
              break;
          }
        });

      authService.ifAuthOnce.then(function () {
        self.isa = authService.isa();
      }, function () {
        self.isa = authService.isa();
      });


      $scope.$on('removePostBlog', function (e, data) {
        removeBlogPost(data);
      });
    }

    function initPost(post) {
      self.avatar = utilService.getUAva(post.created_user);
      self.userId = post.created_user.id;
      self.userName = utilService.fullName(post.created_user);
      self.created = utilService.fdate(post.created);
      self.header = post.header;
      self.text = post.text;
    }

    function initHeader(p) {
      var post = p || self.post || {};
      if (post.h_style) {
        var hStyle = post.h_style || {};
        if (hStyle.b_color) {
          self.style = 'style' + hStyle.b_color;
        } else if (hStyle.img) {
          self.style = '';
          $log.log(utilService.getBlogPreviewLg(hStyle.img), hStyle.img);
          self.img = utilService.getBlogPreviewLg(hStyle.img);
        } else {
          self.style = utilService.getStyle(self.post.header);
        }
      } else {
        self.style = utilService.getStyle(self.post.header);
      }
    }

    function removeBlogPost(post) {
      var confirm = $mdDialog.confirm({
        multiple: true,
        title: 'Удалить пост блога',
        textContent: 'Вы хотите удалить пост ' + post.header + '?',
        ok: 'Удалить',
        cancel: 'Отмена',
        clickOutsideToClose: true,
        fullscreen:          true,
        escapeToClose:       true,
        disableParentScroll: true,
        skipHide:            true
      });

      $mdDialog.show(confirm).then(function () {
        dataService.blogPosts().del(post.link)
          .then(function (data) {
            $mdDialog.cancel();
            self.toast('Пост блога успешно удален');
            $timeout(function () {
              $location.path('/blog/');
            }, 50);
          }, function (data) {
            self.toast('Ошибка удаления');
          });
      });
    }
  }
}());
