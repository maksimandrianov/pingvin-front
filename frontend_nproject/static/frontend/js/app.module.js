/**
 * Created by maks on 20.08.16.
 */

// todo: refactor my settings, about me, users

angular.module('pingvinApp', [
  'ngMaterial',
  'ngRoute',
  'ngMessages',
  'uiGmapgoogle-maps',
  'wu.masonry',
  'ngRateIt',
  'ngFileUpload',
  'uiCropper',
  'ngSanitize',
  'log.ex.uo',
  'slickCarousel',
  'oc.lazyLoad'
]);
