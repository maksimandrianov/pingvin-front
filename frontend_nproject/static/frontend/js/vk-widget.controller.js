/**
 * Created by maks on 20.09.17.
 */

(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('VkWidgetController', ['$scope', '$log', '$mdMedia', '$timeout',
      VkWidgetController
    ]);

  function VkWidgetController($scope, $log, $mdMedia, $timeout) {

    var self = this;

    self.init       = init;

    function init() {
      if ($mdMedia('gt-xs')) {
        $timeout(function () {
          VK.Widgets.CommunityMessages("vk_community_messages", 149600462, {
            widgetPosition: "right",
            disableExpandChatSound: "1",
            disableNewMessagesSound: "1",
            tooltipButtonText: "У Вас есть вопрос или пожелание?"
          });
        });
      }
    }

  }

}());
