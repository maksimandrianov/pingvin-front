/**
 * Created by maks on 11.03.17.
 */



(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .controller('postDetailComments', ['$scope', '$rootScope', '$log', '$timeout',
      '$location', '$routeParams', '$anchorScroll', '$mdMedia', 'utilService',
      'dataService', 'authService',
      PostDetailCommentsController
    ]);

  function PostDetailCommentsController($scope, $rootScope, $log, $timeout, $location,
                                        $routeParams, $anchorScroll, $mdMedia, utilService,
                                        dataService, authService) {
    var self = this;


    self.getName          = utilService.fullName;
    self.avatar           = utilService.getUMin;
    self.toast            = utilService.initToast();
    self.addComment       = addComment;
    self.cancelComment    = cancelComment;
    self.clickToFakeInput = undefined;
    self.isVisible        = false;
    self.isCmntLoad       = false;
    self.comments         = [];
    self.showMorePress    = false;
    self.moreComments     = undefined;
    self.fdate            = utilService.fdate;

    self.dataPcomments    = undefined;
    self.isShowBtn        = false;
    self.init             = init;

    $log = $log.getInstance('PostDetailCommentsController');


    var isSmall,
      emojiBtn,
      emojiPicker,
      emojiWrapper,
      emojioneArea;



    function init(postId) {
      $timeout(function () {

        self.dataPcomments = dataService.pcomments(postId);
        self.dataPcomments.list.init();
        moreComments(true);

        $timeout(function () {
          var checkDoubleEnter = 1;
          emojioneArea = angular.element('#post-comment-textarea')
            .emojioneArea({
              events: {
                keypress: function (editor, event) {
                  var code = event.keyCode || event.which;
                  if (code == 13) {
                    ++checkDoubleEnter;
                    emojioneArea[0].emojioneArea.hidePicker();
                  }
                  else checkDoubleEnter = 0;
                  if (checkDoubleEnter === 2) {
                    checkDoubleEnter = 1;
                    event.preventDefault();
                  }
                },
                keydown: function (editor, event) {
                  var code = event.keyCode || event.which;
                  if (code === 8) {
                    var hasLen = emojioneArea[0].emojioneArea.getText().length;
                    if (hasLen) checkDoubleEnter = 0;
                    else checkDoubleEnter = 1;
                    emojioneArea[0].emojioneArea.hidePicker();
                  }
                },
                paste: function (editor, event) {
                  var text = emojioneArea[0].emojioneArea.getText();
                  text = text.replace(/\n\s*\n/g, '\n');
                  text = text.replace(/\t\s*\t/g, ' ');
                  emojioneArea[0].emojioneArea.setText(text);
                  emojioneArea[0].emojioneArea.hidePicker();
                },
                click: function (editor, event) {
                  emojioneArea[0].emojioneArea.hidePicker();
                }
              }
            });

          $timeout(function () {
            initEmojiPicker();
            $(window).resize(function () {
              emojioneArea[0].emojioneArea.hidePicker();
            });
            emojiBtn.click(initEmojiPicker);
          }, 50);

        });

        $scope.$watch(function () {
          return $mdMedia('gt-sm');
        }, function (isGtSm) {
          $timeout(function () {
            var scrollBarSelector = $('.post-detail-comment-bl__list');
            isGtSm ? scrollBarSelector.perfectScrollbar() : scrollBarSelector.perfectScrollbar('destroy');
            isSmall = !isGtSm;
          }, 50);
        });
      });
      authService.ifAuth(afterAuthInit, afterAuthInit);
    }


    function initEmojiPicker() {
      emojiWrapper = emojiWrapper || $('.post-emoji-textarea-wrapper');
      if (!emojiWrapper) return;
      emojiBtn = emojiWrapper.find('.emojionearea-button');
      var offset = emojiBtn.offset();
      if (!offset) return;
      var top = offset.top - 15;
      var left = offset.left - 275.5;
      emojiPicker = emojiPicker || emojiWrapper.find('.emojionearea-picker');
      emojiPicker.css({'position': 'fixed', 'top': top + 'px', 'left': left + 'px'});
    }

    function afterAuthInit() {
      self.moreComments = authService.dOnlyAuth(moreComments);
      self.clickToFakeInput = authService.dOnlyAuth(clickToFakeInput);
    }

    function moreComments(first) {
      first ? self.isCmntLoad = false : self.showMorePress = true;
      self.dataPcomments.list
        .next()
        .then(function success(data) {
          $log.log(data);
          var tmp = data.reverse(),
            lastId = !self.comments.length || tmp[tmp.length - 1].id;
          self.comments = tmp.concat(self.comments);

          processComment(first, true, lastId);

          self.isCmntLoad = true;
          self.showMorePress = false;
        }, function fail(response) {
          self.isCmntLoad = true;
          self.showMorePress = false;
        });
    }

    function updateScrollBar() {
      if (!isSmall) {
        $('.post-detail-comment-bl__list').perfectScrollbar('update');
      }
    }


    function processComment(first, more, lastId) {
      $timeout(function () {
        angular.element('.unicode-emojis').each(function () {
          var elem = angular.element(this),
            htmlEncoded = emojione.unicodeToImage(elem.html());
          elem.html(htmlEncoded);
          elem.removeClass('unicode-emojis');
        });

        updateScrollBar();

        if (!more) return;
        if (first) {
          self.isVisible = true;
          setButtomScrollComment('anchor-send-comment');
        } else {
          setButtomScrollComment('pcomment-' + lastId);
        }
      }, 100);
    }


    function setButtomScrollComment(ancor) {
      $timeout(function () {
        var oldHash = $location.hash();
        $location.hash(ancor);
        $anchorScroll();
        $location.hash(oldHash);
      });
    }

    function focusedInput() {
      $timeout(function () {
        angular.element('.emojionearea-editor').focus();
        setButtomScrollComment('anchor-send-comment');
      });
    }

    function clickToFakeInput() {
      self.isShowBtn = true;
      focusedInput();
      $timeout(function () {
        initEmojiPicker();
      })
    }

    function cancelComment() {
      clearComment();
      self.isShowBtn = false;
    }

    function clearComment() {
      updateScrollBar();
      emojioneArea[0].emojioneArea.setText('');
    }

    function addComment() {
      var text = emojioneArea[0].emojioneArea.getText().trim();
      text = text.replace(/\n\s*\n/g, '\n');
      text = text.replace(/\t\s*\t/g, ' ');
      if (!text.length) {
        clearComment();
        return;
      }
      var data = {
        text: text
      };
      self.dataPcomments.create
        .post(data)
        .then(function success(data) {
          self.comments.push(data);
          processComment(false);
          setButtomScrollComment('anchor-send-comment');
          clearComment();
        }, function fail(response) {
          self.toast(response.code);
        });
    }
  }

}());
