/**
 * Created by maks on 03.11.16.
 */

(function () {
  'use strict';

  angular.module('pingvinApp').component('activity', {
    templateUrl: '/public/html/activity.html',
    controller: ['$scope', '$log', '$rootScope',
      '$location',  ActivityController]

  }).constant('activityType', {
    newHelper: 0,
    updateCondition: 1,
    selectHelper: 2,
    cancelHelper: 3,
    finishHelp: 4
  });

  function ActivityController($scope, $log, $rootScope, $location) {

    var self = this;
    self.current = undefined;
    self.$onInit = init;

    $log = $log.getInstance('ActivityController');

    function init() {
      $rootScope.$broadcast('reloadCtrl', {state: 'url-activity'});
      $scope.$on('changeTab', function(ev, data) {
        if (self.current == data) return;
        to(data);
      });
    }

    function to(tab) {
      if (tab == 0) $location.search('act', 'news');
      else if (tab == 1) $location.search('act', 'my-posts');
      else if (tab == 2) $location.search('act', 'my-helps');
      self.current = tab;
    }

  }
}());
