/**
 * Created by maks on 28.07.17.
 */

(function () {
  'use strict';

  angular.module('pingvinApp')
    .component('adminModeration', {
      templateUrl: '/public/html/post-list-one-col.html',
      controller: ['$scope', '$rootScope', '$log', '$timeout',
        '$controller', '$mdDialog', '$location', 'utilService', 'dataService',
        AdminPostsModerationController
      ]});

  function AdminPostsModerationController($scope, $rootScope, $log, $timeout, $controller,
                                          $mdDialog, $location, utilService, dataService) {
    var self = this;

    angular.extend(self, $controller('BaseListController', {
      $scope: $scope,
      $rootScope: $rootScope,
      $log: $log,
      $timeout: $timeout
    }));

    self.hasHeader              = false;
    self.currDescription        = false;
    self.isActivity             = false;
    self.msgNoInfo              = 'Пока нет немодерированных публикаций';
    self.showDetail             = showDetail;
    self.isOpenDialog           = false;
    $log                        = $log.getInstance('AdminPostsModerationController');


    init();

    function init() {
      self.BaseListController({
        data: dataService.adminPostsModeration()
      });
      initPostFromUrl();
      $scope.$on('$routeUpdate', function () {
        var urlParams = $location.search();
        if (!self.isOpenDialog) {
          initPostFromUrl();
        } else if (!urlParams.hasOwnProperty('post') && !urlParams.hasOwnProperty('event')) {
          $mdDialog.cancel();
        }
      });
      $scope.$on('postClose', function (e, el) {
        utilService.removeUnit(el.id, self.collection);
      });
    }

    function initPostFromUrl() {
      var urlParams = $location.search();
      if (urlParams.event) {
        showDetail(null, urlParams.event, 1);
      }
    }

    function showDetail(ev, id, t) {
        var type = t === 1 ? 'event' : 'post',
          data = t === 1 ? dataService.events() : dataService.posts();

      utilService.dialog({
        controller: 'PostDetailController',
        templateUrl: '/public/html/post-detail.html',
        scope: $scope.$new(),
        locals: {
          option: {
            postId: id,
            type: type,
            pData: data,
            isModer: true
          }
        },
        onShowing: function () {
          self.isOpenDialog = true;
          $location.search(type, id);
        },
        onRemoving: function () {
          self.isOpenDialog = false;
          $location.search(type, null);
        }
      });
    }

  }
}());