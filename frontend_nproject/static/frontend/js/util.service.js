/**
 * Created by maks on 09.09.16.
 */


(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .service('utilService', ['$mdToast', '$log', '$location', '$mdDialog', '$window',
      'GOOGLE_API_KEY', UtilService]);

  function UtilService($mdToast, $log, $location, $mdDialog, $window, GOOGLE_API_KEY) {
    var self = this;

    var DEFAULT_AVA = '/public/img/anonim.png';
    var SOCIAL_AVA = 5;

    //  common
    self.getStyle          = getStyle;
    self.initToast         = initToast;
    self.dialog            = dialog;
    self.mapUrl            = mapUrl;
    self.confirmNotSaved   = confirmNotSaved;
    self.parseURL          = parseUri;
    self.realSize          = realSize;
    self.confirmNotLogin   = confirmNotLogin;
    self.printStack        = printStack;
    self.distanceLabel     = distanceLabel;
    self.fdate             = fdate;
    self.removeUnit        = removeUnit;
    self.isDefaultAvatar   = isDefaultAvatar;
    self.getErrorCode      = getErrorCode;
    self.dHandleError      = dHandleError;
    self.getMetaImg        = getMetaImg;
    self.getPostPreviewLg  = getPostPreviewLg;
    self.getEventPreviewLg = getEventPreviewLg;
    self.getBlogPreviewLg  = getBlogPreviewLg;
    self.originalImg       = originalImg;
    self.getRandomInt      = getRandomInt;
    //  user
    self.openUser          = openUser;
    self.fullName          = fullName;
    self.getUAva           = getUAva;
    self.getUMed           = getUMed;
    self.getUMin           = getUMin;
    self.getUMinUrl        = getUMinUrl;
    self.popupCenter       = popupCenter;
    self.textFromHtml      = textFromHtml;
    self.shareDlg          = shareDlg;
    self.share             = {
      vk: shareVk,
      fb: shareFacebook,
      gp: shareGooglePlus,
      tw: shareTwitter
    };

    init();

    function init() {
      String.prototype.capitalize = function () {
        return this.charAt(0).toUpperCase() + this.slice(1);
      };

      Array.prototype.resize = function(newSize, defaultValue) {
        while(newSize > this.length)
          this.push(defaultValue);
        this.length = newSize;
      };
    }

    function realSize(obj){
      var clone = obj.clone();
      clone.css('visibility', 'hidden');
      $('body').append(clone);
      var result = {
        width: clone.outerWidth(),
        height: clone.outerHeight()
      };
      clone.remove();
      return result;
    }

    function textFromHtml(html) {
      var tmp = document.createElement('span');
      tmp.innerHTML = html;
      return tmp.textContent || tmp.innerText || '';
    }

    function fixUrl(context, detail, size, url) {
      return '/img/' + context + detail  + size + url;
    }

    function getUserPhoto(detail, size, url) {
      return fixUrl('user/', detail, size, url);
    }

    function getPostPhoto(detail, size, url) {
      return fixUrl('post/', detail, size, url);
    }

    function getBlogPhoto(detail, size, url) {
      return fixUrl('blog/', detail, size, url);
    }

    function getEventPhoto(detail, size, url) {
      return fixUrl('event/', detail, size, url);
    }

    function getUserAvatarPhoto(size, url) {
      return  getUserPhoto('avatar/', size, url);
    }

    function getPostPreviewPhoto(size, url) {
      return  getPostPhoto('preview/', size, url);
    }

    function getEventPreviewPhoto(size, url) {
      return  getEventPhoto('preview/', size, url);
    }

    function getBlogPreviewPhoto(size, url) {
      return getBlogPhoto('preview/', size, url);
    }

    function getUserAvatarSm(url) {
      return  getUserAvatarPhoto('sm', url);
    }

    function getUserAvatarMd(url) {
      return  getUserAvatarPhoto('md', url);
    }

    function getUserAvatarLg(url) {
      return  getUserAvatarPhoto('lg', url);
    }

    function getPostPreviewLg(url) {
      return getPostPreviewPhoto('lg', url);
    }

    function getEventPreviewLg(url) {
      return getEventPreviewPhoto('lg', url);
    }

    function getBlogPreviewLg(url) {
      return getBlogPreviewPhoto('lg', url);
    }

    function getUAva(user) {
      if (user.avatar &&
        user.avatar.hasOwnProperty('url') &&
        user.avatar.url) {
        if (user.avatar.type === SOCIAL_AVA)
          return user.avatar.url;
        return getUserAvatarLg(user.avatar.url);
      }
      return DEFAULT_AVA;
    }

    function getUMinUrl(url, type) {
      if (url) {
        if (type === SOCIAL_AVA)
          return url;
        return getUserAvatarSm(url);
      }
      return DEFAULT_AVA;
    }

    function getUMin(user) {
      if (user.avatar &&
        user.avatar.hasOwnProperty('url') &&
        user.avatar.url) {
        if (user.avatar.type === SOCIAL_AVA)
          return user.avatar.url;
        return getUserAvatarSm(user.avatar.url);
      }
      return DEFAULT_AVA;
    }

    function getUMed(user) {
      if (user.avatar &&
        user.avatar.hasOwnProperty('url') &&
        user.avatar.url) {
        if (user.avatar.type === SOCIAL_AVA)
          return user.avatar.url;
        return getUserAvatarMd(user.avatar.url);
      }
      return DEFAULT_AVA;
    }

    function originalImg(img) {
      return '/public' + img;
    }

    function isDefaultAvatar(url) {
      return url === DEFAULT_AVA;
    }

    function isNumeric(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function getErrorCode(response) {
      return angular.isObject(response) ?
        (response.error || response.status) : '';
    }

    function dHandleError(toast) {
      return function (responce) {
        toast('Ошибка ' + getErrorCode(responce));
      };
    }

    function confirmNotSaved() {
      var confirm = $mdDialog.confirm({
        title: 'Измененные данные не были сохранены',
        textContent: 'Вы хотите продожить без сохранения ?',
        ok: 'Продолжить',
        cancel: 'Отмена',
        clickOutsideToClose: false,
        fullscreen:          true,
        escapeToClose:       false,
        disableParentScroll: true,
        skipHide:            true
      });

      return $mdDialog.show(confirm);
    }

    function mapUrl(point) {
      return 'https://maps.googleapis.com/maps/api/staticmap?center=' +
        point.latitude + ',' + point.longitude +
        '&zoom=15&size=800x300&maptype=roadmap&markers=color:blue%7Clabel:%7C' +
        point.latitude + ',' + point.longitude + '&key=' + GOOGLE_API_KEY;
    }

    function dialog(option) {
      var base = {
        controllerAs: '$ctrl',
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        fullscreen: true,
        escapeToClose: true,
        disableParentScroll: true
      };
      for(var k in option) base[k]=option[k];
      $mdDialog.show(base);
    }

    function openUser(userObj) {
      $log.log(openUser, userObj);
      var id =  userObj.id || userObj.user_id;
      var url = '/user/' + id  + '/';
      $location.url(url);
    }

    function fullName(userObj) {
      return userObj.first_name + ' ' + userObj.second_name;
    }

    function initToast() {
      return function (message) {

        var options = {
          template: '<md-toast><span class="md-toast-content" flex>' + message + '</span></md-toast>',
          position: 'bottom left',
          hideDelay: 2000
        };

        $mdToast.show(options);
      };
    }

    function popupCenter(url, title, w, h) {
      // Fixes dual-screen position                         Most browsers      Firefox
      var dualScreenLeft = $window.screenLeft != undefined ? $window.screenLeft : screen.left;
      var dualScreenTop = $window.screenTop != undefined ? $window.screenTop : screen.top;

      var width = $window.innerWidth ? $window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
      var height = $window.innerHeight ? $window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

      var left = ((width / 2) - (w / 2)) + dualScreenLeft;
      var top = ((height / 2) - (h / 2)) + dualScreenTop;
      var newWindow = $window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

      // Puts focus on the newWindow
      if ($window.focus) {
        newWindow.focus();
      }
      return newWindow
    }

    function getHash(str) {
      var char, i, hash = 0;
      if (str.length === 0) {
        return hash;
      }
      for (i = 0; i < str.length; i++) {
        char = str.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash;
      }
      return hash;
    }

    function getMetaImg(url, func){
      $('<img/>',{
        load : function() {
          func(this);
        },
        src: url
      });
    }


    function getStyle(header) {
      var kStylesCount = 6,
        numStyle = getHash(header);
      if (numStyle < 0) {
        numStyle *= -1;
      }
      return 'style' + ((numStyle % (kStylesCount)) + 1);
    }


    function parseUri (str) {
      var	o   = parseUri.options,
        m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i   = 14;
      while (i--) uri[o.key[i]] = m[i] || "";
      uri[o.q.name] = {};
      uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
        if ($1) uri[o.q.name][$1] = $2;
      });

      return uri;
    }

    parseUri.options = {
      strictMode: false,
      key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
      q:   {
        name:   "queryKey",
        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
      },
      parser: {
        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
      }
    };

    function fdate(date) {
      if (!date) {
        return date;
      }
      var d = date;
      var currYear = new Date().getFullYear() + ' ';
      d = d.replace(currYear, '');
      return d.replace(/(\d+):(\d+):(\d+)/g, function (str, p1, p2, p3, offset, s) {
          return p1 + ':' + p2;
        }
      );
    }


    function confirmNotLogin(ev) {
      $log.log($location.path() + '?login');

      var confirm = $mdDialog.confirm()
        .title('Может быть вы еще не зарегистрированы ?')
        .htmlContent('<div class="md-body-1">Данное действие доступно только для пользователей Pingvin</div>' +
          '<div class="md-body-1">Уже зарегистрированны ? <a class="link link_blue link_b" ' +
          'href="' + $location.path() + '?login' + '">Войти.</a></div>')
        .targetEvent(ev)
        .ok('Зарегистрироваться')
        .cancel('Отмена')
        .multiple(true);
      $mdDialog.show(confirm).then(function() {
        //$location($location.path());
        //$location.search('signup', true);

        $location.url($location.path() + '?signup')
      }, function() {
        $log.log('cancel', $location.path());
      });
    }


    function printStack() {
      var stack = new Error().stack;
      console.log('PRINTING CALL STACK');
      console.log(stack);

    }


    function removeUnit(id, from, key) {
      for (var i = 0; i < from.length; ++i) {
        var k = angular.isUndefined(key) ? 'id' : key;
        if (from[i][k]=== id) {
          from.splice(i, 1);
          return true;
        }
      }
      $log.warn('not found unit for remove with id', id);
    }



    function distanceLabel(distanceMeters) {
      if (angular.isUndefined(distanceMeters) || distanceMeters == null) {
        return '';
      }

      function analizeRemainder(stringFloatNumber) {
        if (!angular.isString(stringFloatNumber)) return '';
        var stringFloatNumberArr = stringFloatNumber.split('.');
        if (stringFloatNumberArr.length != 2) return stringFloatNumber;
        var reminder = stringFloatNumberArr[1];
        for (var i = reminder.length-1; i >= 0; i--) {
          if (reminder[i] === '0') reminder = reminder.slice(0, -1);
          else break;
        }
        if (reminder.length) {
          stringFloatNumberArr[1] = reminder;
          return stringFloatNumberArr.join('.');
        } else {
          return stringFloatNumberArr[0];
        }
      }

      var tmp = distanceMeters/1000;
      var tmp1 = parseInt(tmp);
      if (tmp1 > 0) {
        tmp = tmp.toFixed(2);
        return analizeRemainder(tmp) + ' км';
      } else {
        tmp = distanceMeters.toFixed(2);
        return analizeRemainder(tmp) + ' м';
      }

    }


    function shareVk(url, header, img) {
      var shareUrl = "";
      shareUrl = 'https://vk.com/share.php?';
      shareUrl += 'url='     + encodeURIComponent(url);
      shareUrl += '&title='  + encodeURIComponent(header);
      shareUrl += '&image='  + encodeURIComponent(img);
      shareUrl += '&noparse=true';
      return shareUrl;
    }

    function shareFacebook(url, header, text, img) {
      var shareUrl = "";
      shareUrl = 'http://www.facebook.com/sharer.php?s=100';
      shareUrl += '&p[title]='     + encodeURIComponent(header);
      shareUrl += '&p[summary]='   + encodeURIComponent(text);
      shareUrl += '&p[url]='       + encodeURIComponent(url);
      shareUrl += '&p[images][0]=' + encodeURIComponent(img);
      return shareUrl;
    }

    function shareGooglePlus(url) {
      var shareUrl = "";
      shareUrl  = 'https://plus.google.com/share?';
      shareUrl += 'url='      + encodeURIComponent(url);
      return shareUrl;
    }

    function shareTwitter(url, header) {
      var shareUrl = "";
      shareUrl  = 'https://twitter.com/share?';
      shareUrl += 'text='      + encodeURIComponent(header);
      shareUrl += '&url='      + encodeURIComponent(url);
      shareUrl += '&counturl=' + encodeURIComponent(url);
      return shareUrl;
    }


    function getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function shareDlg(event, post) {
        var elem = angular.element('.post-detail-dialog');
        var parentElem = elem ? elem.parent() : undefined;
        dialog({
          multiple: true,
          controller: 'ShareController',
          templateUrl: '/public/html/share.html',
          locals: { post: post },
          onShowing: function () { if (parentElem) parentElem.addClass('second_plan_dialog'); },
          onRemoving: function () { if (parentElem) parentElem.removeClass('second_plan_dialog'); }
        });
    }

  }  // UtilService

}());
