/**
 * Created by maks on 06.03.17.
 */


(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .component('loadMoreButton', {
      template: '<div ng-cloak="" layout="row" class="load-bnt" layout-align="center center" ng-if="$ctrl.isShow">' +
      '<md-button ng-show="!$ctrl.isLoading" ng-click="$ctrl.click()">Показать еще</md-button>' +
      '<md-progress-circular ng-show="$ctrl.isLoading" md-mode="indeterminate" md-diameter="48"></md-progress-circular>' +
      '</div>',
      controller: ['authService', function (authService) {
        var self = this;

        self.click = undefined;

        self.$onInit = init;


        function init() {
          authService.ifAuth(afterAuthInit, afterAuthInit);
        }

        function afterAuthInit() {
          self.click = authService.dOnlyAuth(self.onClick);
        }

        function internalClick() {

        }
      }],
      bindings: {
        isShow: '=?',
        isLoading: '=?',
        onClick: '&'
      }
    });
}());