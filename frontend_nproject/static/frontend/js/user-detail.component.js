/**
 * Created by maks on 05.09.16.
 */

/*jslint browser: true,
 nomen: true,
 white: true */
/*global moment, angular*/


(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .component('userDetail', {
      templateUrl: '/public/html/user-detail.html',
      controller: ['$scope', '$window', '$log', '$rootScope', '$mdMedia', '$routeParams',
        '$timeout', '$location', '$mdDialog', 'authService', 'categoryService',
        'utilService', 'dataService', UserDetailController]

    });


  function UserDetailController($scope, $window, $log, $rootScope, $mdMedia, $routeParams,
                                $timeout, $location, $mdDialog, authService, categoryService,
                                utilService, dataService) {
    var self = this;

    self.user          = undefined;
    self.age           = undefined;
    self.gender        = undefined;
    self.info          = undefined;
    self.name          = undefined;
    self.avatar        = undefined;
    self.isOwner       = undefined;
    self.isa           = undefined;
    self.buttonsCat = {
      show: false,
      disablePrev: false,
      disableNext: false,
      mwindowWidth: undefined,
      melementWidth: undefined,
      mcountBlock: undefined
    };
    self.buttonsHon    = angular.copy(self.buttonsCat);
    self.slickConfCat  = initConfigSlick(self.buttonsCat);
    self.slickConfHon  = initConfigSlick(self.buttonsHon);
    self.showBntHon    = false;
    self.title         = categoryService.getTitle;
    self.getName       = utilService.fullName;
    self.toast         = utilService.initToast();
    self.isDefaultAva  = utilService.isDefaultAvatar;
    self.addPhoto      = addPhoto;
    self.removePhoto   = removePhoto;
    self.getStyle      = utilService.getStyle;
    $scope.$mdMedia    = $mdMedia;
    $scope.$rootScope  = $rootScope;


    var typeSlider = {
      CATEGORIES: 1,
      HONORS: 2
    };

    init();

    function init() {
      $rootScope.showProgress = true;
      authService.ifAuth(function () {
        if (authService.isLogin && !$routeParams.hasOwnProperty('userId')) {
          initUser(authService.user.id, function() {
            $rootScope.$broadcast('reloadCtrl', {state: 'url-about-me'});
          });
        } else {
          initUserWithId();
        }
      }, function () {
        initUserWithId();
      });

      $scope.$on('newAvatar', function(ev, data) {
        self.avatar = utilService.getUAva(data);
      });

      $scope.$on('render-categories', function () {
        initButtons(typeSlider.CATEGORIES, self.user.categories.length);
        angular.element($window).on('resize', function () {
          initButtons(typeSlider.CATEGORIES, self.user.categories.length);
        });
      });

      $scope.$on('render-honors', function () {
        initButtons(typeSlider.HONORS, self.user.mhonors.length);
        angular.element($window).on('resize', function () {
          initButtons(typeSlider.CATEGORIES, self.user.mhonors.length);
        });
      });
    }

    function initConfigSlick(buttonsObj) {
      return {
        method: {},
        event: {
          beforeChange: function (event, slick, currentSlide, nextSlide) {
            nextSlide === 0 ?
              buttonsObj.disablePrev = true :
              buttonsObj.disablePrev = false;

            if (angular.isUndefined(buttonsObj.mwindowWidth) ||
              angular.isUndefined(buttonsObj.melementWidth))
              return;

            var factElInWindow = buttonsObj.mwindowWidth / buttonsObj.melementWidth;
            var remLastEl = 1 - (factElInWindow - Math.floor(factElInWindow));
            var additions = remLastEl < 0.08 ? 1 : 0;

            nextSlide + additions + factElInWindow + remLastEl > buttonsObj.mcountBlock ?
              buttonsObj.disableNext = true :
              buttonsObj.disableNext = false;
          }
        }
      };
    }

    function initButtons(type, countBlock) {
      $timeout(function () {
        var elementBox, config, buttons;
        if (type === typeSlider.CATEGORIES) {
          elementBox = $('.slider-block-categories');
          buttons = self.buttonsCat;
          config = self.slickConfCat;
        } else if (type === typeSlider.HONORS) {
          elementBox = $('.slider-block-honors');
          buttons = self.buttonsHon;
          config = self.slickConfHon;
        }
        if (elementBox) {
          buttons.melementWidth = $('.user-detail-element-box-type', elementBox).outerWidth(true);
          buttons.mwindowWidth = $('slick', elementBox).outerWidth(true);
          buttons.mcountBlock = countBlock;
          if (countBlock * buttons.melementWidth > buttons.mwindowWidth ) {
            buttons.show = true;
            $timeout(function() {
              $('.slick-track', elementBox).removeClass('slider-list-remove-transform');
              buttons.mwindowWidth = $('.slick-list', elementBox)
                .addClass('slider-list-with-buttons')
                .outerWidth();
              $timeout(function () {
                config.method.slickGoTo(0);
              });
            }, 50);
          } else {
            buttons.show = false;
            $timeout(function() {
              $('.slick-list', elementBox).removeClass('slider-list-with-buttons');
              $('.slick-track', elementBox).addClass('slider-list-remove-transform');
            }, 50);

          }
        }
      });
    }

    function initUserWithId() {
      if ($routeParams.hasOwnProperty('userId')) {
        initUser($routeParams.userId, function () {
          $rootScope.$broadcast('reloadCtrl', {
            state: 'url-user',
            name: self.name
          });
        });
      }
    }

    function initUser(id, callbackReload) {
      dataService.users().get(id)
        .then(function (data) {
          self.user = data;
          self.isOwner = authService.isOwn(self.user);
          self.isa = authService.isa();
          self.name = self.getName(self.user);
          self.age = getAge();
          self.gender = getGender();
          self.info = getInfo();
          self.avatar = utilService.getUAva(self.user);
          $timeout(function () {
            $rootScope.showProgress = false;
          }, 10);
          if (angular.isFunction(callbackReload)) callbackReload();
        }, function (response) {
          var code = utilService.getErrorCode(response);
          var message = {
            error: code,
            description: 'Страница пользователя не найдена'
          };
          $scope.$emit('error', message);
        });
    }

    function addPhoto() {
      utilService.dialog({
        controller: 'UserDetailPhotoLoadController',
        skipHide: true,
        scope: $scope.$new(),
        templateUrl: '/public/html/user-avatar.html'
      });
    }

    function removePhoto() {
      var confirm = $mdDialog.confirm()
        .title('Предупреждение')
        .textContent('Вы уверены, что хотите удалить фотографию ?')
        .ok('Удалить')
        .cancel('Отмена');

      $mdDialog.show(confirm)
        .then(function () {
          dataService.users()
            .update(authService.user.id, {avatar: {url: ''}})
            .then(function (data) {
              $scope.$emit('newAvatar', utilService.getUAva(data));
              $timeout(function() {
                self.toast('Фотография удалена');
                $mdDialog.cancel();
              }, utilService.dHandleError(self.toast));
              $log.log('avatar updated with answer', data);
            });
        });
    }

    function textAge(age) {
      var txt,
        count = age % 100;
      if (count >= 5 && count <= 20) {
        txt = 'лет';
      } else {
        count = count % 10;
        if (count === 1) txt = 'год';
        else if (count >= 2 && count <= 4) txt = 'года';
        else txt = 'лет';
      }
      return txt;
    }

    function getAge() {
      if (!self.user || !self.user.bday) return null;
      var age = moment().diff(self.user.bday, 'years');
      return age + ' ' + textAge(age);
    }

    function getGender() {
      if (!self.user) return null;
      var gender;
      switch (self.user.gender) {
        case 'm':
          gender = 'Мужской';
          break;
        case 'f':
          gender = 'Женский';
          break;
        default:
          gender = null;
          break;
      }
      return gender;
    }

    function getInfo() {
      if (!self.user || !self.user.info.length) return null;
      return self.user.info;
    }
  }
}());
