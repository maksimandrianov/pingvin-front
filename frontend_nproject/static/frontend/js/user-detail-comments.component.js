/**
 * Created by maks on 09.03.17.
 */


(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .component('userDetailComments', {
      templateUrl: '/public/html/user-detail-comments.html',
      controller: ['$scope', '$rootScope', '$log', '$timeout',
        '$controller', '$routeParams', 'utilService', 'dataService', 'authService',
        UserDetailCommentsController],

      bindings: {
        userName: '=?'
      }
    });

  function UserDetailCommentsController($scope, $rootScope, $log, $timeout,
                                       $controller, $routeParams, utilService,
                                       dataService, authService) {
    var self = this;

    angular.extend(self, $controller('BaseListController', {
      $scope: $scope,
      $rootScope: $rootScope,
      $log: $log,
      $timeout: $timeout
    }));

    self.getName  = utilService.fullName;
    self.avatar   = utilService.getUMin;
    self.openUser = utilService.openUser;
    self.toast    = utilService.initToast();
    self.fdate    = utilService.fdate;

    init();

    function init() {
      authService.ifAuth(function () {
        var userId = $routeParams.hasOwnProperty('userId') ? $routeParams.userId : authService.user.id;
        initComments(userId);
      }, function() {
        if ($routeParams.hasOwnProperty('userId')) {
          initComments($routeParams.userId);
        } else {
          $log.log('User comments load error');
        }
      });
    }

    function initComments(userId) {
      self.BaseListController({
        data: dataService.ucomments(userId),
        preShowMoreAsync: processComment
      });
    }

    function processComment() {
      angular.element('.unicode-emojis').each(function () {
        var elem =  angular.element(this),
          htmlEncoded = emojione.unicodeToImage(elem.html());
        elem.html(htmlEncoded);
        elem.removeClass('unicode-emojis');
      });
    }

  }

}());
