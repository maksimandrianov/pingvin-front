/**
 * Created by maks on 20.08.16.
 */

(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .component('postList', {
      templateUrl: '/public/html/post-list.html',
      controller: ['$rootScope', '$scope', '$mdMedia', '$log',
        '$mdDialog', '$routeParams', '$location', '$timeout',
        '$window', '$controller', '$anchorScroll', 'authService', 'dataService',
        'utilService', PostListController],
      bindings: {
        options: '<'
      }
    })
    .directive('onFinishRender', ['$timeout', '$log', function ($timeout, $log) {
      return {
        restrict: 'A',
        link: function (scope, element, attr) {
          if (scope.$last === true) {
            $timeout(function () {
              scope.$emit(attr.onFinishRender);
            });
          }
        }
      };
    }]);

  function PostListController($rootScope, $scope, $mdMedia, $log,
                              $mdDialog, $routeParams, $location, $timeout,
                              $window, $controller, $anchorScroll, authService,
                              dataService, utilService) {

    var self = this;

    angular.extend(self, $controller('BaseAddableController', {
      $scope: $scope,
      $rootScope: $rootScope,
      $log: $log,
      $timeout: $timeout,
      $controller: $controller,
      $location: $location,
      $routeParams: $routeParams,
      $mdDialog: $mdDialog,
      utilService: utilService,
      authService: authService
    }));

    self.showToast    = utilService.initToast();
    self.postDetail   = self.showDetail;
    self.isShowPbtn   = undefined;
    self.msgNoInfo    = '';

    $scope.$mdMedia   = $mdMedia;

    $log              = $log.getInstance('PostListController');

    self.$onInit = init;

    function init() {
      $log.log('init PostListController');

      var options    = self.options || {};
      var dataPosts  = options.src || dataService.posts();
      var isView     = options.isView || false;
      self.msgNoInfo = options.msg || 'Пока нет никаких объявлений';

      self.BaseAddableController({
        type: 'post',
        state: 'url-posts',
        data: dataPosts,
        isView: isView,
        preShowMore: preShowMore,
        postIsShowBtn: function () {
          return _.first(self.collection).isVisible === true;
        }
      });

      $scope.$on('ngRepeatFinished', function () {
        $log.log('ngRepeatFinished');
        $timeout(function () {
          $rootScope.$broadcast('masonry.reload');
          $log.log('masonry.reload');
          $timeout(function () {
            preShowMore(self.collection, true);
            $timeout(function () {
              self.isShowPbtn = true;
            }, 100)
          }, 50);
        });
      }, 50);
    }

    function preShowMore(posts, value) {
      for (var i = 0; i < posts.length; ++i) {
        posts[i].isVisible = !!value;
      }
      return posts;
    }
  }

}());
