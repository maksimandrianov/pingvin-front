/**
 * Created by maks on 03.04.17.
 */


(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('PostDetailHelpController', ['$scope', '$rootScope', '$mdDialog',
      'conditionService', 'utilService', 'dataService', 'post',
      PostDetailHelpController]);


  function PostDetailHelpController($scope, $rootScope, $mdDialog, conditionService,
                                    utilService, dataService, post) {
    var self = this;

    self.okBtnName = 'Откликнуться';
    self.header    = 'Указать мотивацию';
    self.text      = 'Вы можете предложить что-то другое. ' +
      'Потом пользователь, опубликовавший объявление выберет самое интересное предложение. ' +
      'Например, если мотивация публикации это деньги, то скорее всего будет выбран помощник, ' +
      'который согласен сделать работу за более низкую цену.';
    self.condition = {};
    self.toast     = utilService.initToast();
    self.save      = save;

    init();

    function init() {
      if (post.condition) {
        angular.copy(post.condition, self.condition);
      } else {
        self.condition = { 'type': 1 };
      }
    }

    function save() {
      var id = post.id;
      conditionService.prepareCondition(self.condition);
      dataService.user.help(id, self.condition)
        .then(function(data) {
          $scope.$emit('helpUser');
          post.is_help = true;
          post.helpers += 1;

          $rootScope.$broadcast('helpersChange' + id, {
            is_help: post.is_help,
            helpers: post.helpers
          });
          self.toast('Спасибо за отклик');
          $mdDialog.cancel();
        });
    }

  }

}());