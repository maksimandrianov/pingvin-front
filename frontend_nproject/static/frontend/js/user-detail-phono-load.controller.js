/**
 * Created by maks on 03.04.17.
 */


(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('UserDetailPhotoLoadController', ['$scope', '$log', '$timeout',
      '$mdDialog', 'Upload', 'dataService', 'authService', 'utilService',
      UserDetailPhotoLoadController]);


  function UserDetailPhotoLoadController($scope, $log, $timeout, $mdDialog, Upload, dataService,
                                         authService, utilService) {
    var self = this;

    self.error      = {};
    self.picFile    = undefined;
    self.cropCoords = undefined;
    self.progress   = 0;
    self.loadimg    = false;
    self.save       = save;
    self.toast      = utilService.initToast();
    $scope.cancel   = function () {$mdDialog.cancel();};

    $log            = $log.getInstance('UserDetailPhotoLoadController');

    init();

    function init() {
      $scope.$on('ImgCropRegionError', function (ev, data) {
        data = data || {};
        self.error.show = true;
        self.error.msg = data.message || '';
      });
      $scope.$watch(function () {
        return self.picFile
      }.bind(self), function (newVal) {
        if (newVal) self.error.show = false;
      });
    }

    function save() {
      if (!check()) return;
      uploadFiles(self.picFile, self.cropCoords)
        .then(function (data) {
          var avaLinks = {
            avatar: { url: data.cropped },
            avatar_orig: { url: data.original }
          };
          dataService.users()
            .update(authService.user.id, avaLinks)
            .then(function (data) {
              self.progress = 100;
              $scope.$emit('newAvatar', data);
              $timeout(function() {
                self.toast('Фотография загружена');
                self.loadimg = false;
                $mdDialog.cancel();
              });
              $log.log('avatar updated with answer', data);
            });
        });
    }

    function uploadFiles(file, coords) {
      self.loadimg = true;
      var size = utilService.realSize($('.crop-area-avatar'));
      return Upload.upload({
        url: '/upload/',
        data: {
          file: file,
          size: JSON.stringify(size),
          crop: JSON.stringify(coords)
        }
      }).then(function (response) {
        return response.data;
      }, function (response) {
        if (response.status > 0) self.errorMsg = response.status
          + ': ' + response.data;
      }, function (evt) {
        self.progress = parseInt(90.0 * evt.loaded / evt.total);
      });
    }

    function check() {
      if (!self.picFile || !self.cropCoords) {
        self.error.show = true;
        self.error.msg = 'Файл не выбран!';
        return false;
      }
      return true;
    }

  }

}());