/**
 * Created by maks on 06.08.17.
 */

(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .component('blogItem', {
      templateUrl: '/public/html/blog-item.html',
      controller: ['$scope', '$log', '$rootScope', '$timeout', 'utilService', 'authService',
        PostBlogItemController],

      bindings: {
        post: '<'
      }
    });

  function PostBlogItemController($scope, $log, $rootScope, $timeout, utilService, authService) {
    var self = this;

    self.style          = undefined;
    self.img            = undefined;
    self.getName        = utilService.fullName;
    self.avatar         = utilService.getUAva;
    self.openUser       = utilService.openUser;
    self.toast          = utilService.initToast();
    self.fdate          = utilService.fdate;
    self.getImg         = utilService.getBlogPreviewLg;
    self.removeBlogPost = removeBlogPost;
    self.isa            = undefined;

    $log                = $log.getInstance('PostBlogItemController');

    self.$onInit = init;

    function init() {
      authService.ifAuthOnce.then(function () {
        self.isa = authService.isa();
      }, function () {
        self.isa = authService.isa();
      });

      initHeader();
    }

    function removeBlogPost(post) {
      $scope.$emit('removePostBlog', post);
    }

    function initHeader() {
      var post = self.post || {};
      if (post.h_style) {
        var hStyle = post.h_style || {};
        if (hStyle.b_color) {
          self.style = 'style' + hStyle.b_color;
        } else if (hStyle.img) {
          self.style = '';
          $log.log(utilService.getBlogPreviewLg(hStyle.img), hStyle.img);
          self.img = utilService.getBlogPreviewLg(hStyle.img);
        } else {
          self.style = utilService.getStyle(self.post.header);
        }
      } else {
        self.style = utilService.getStyle(self.post.header);
      }
    }
  }

}());