/**
 * Created by maks on 20.09.17.
 */

(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('ShareDlgController', ['utilService',
      ShareDlgController
    ]);

  function ShareDlgController(utilService) {

    var self = this;

    self.share = share;

    function share(ev, post) {
      utilService.shareDlg(ev, post);
    }
  }
}());