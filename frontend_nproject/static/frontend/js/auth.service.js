/**
 * Created by maks on 01.09.16.
 */

(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .service('authService', ['$rootScope', '$http', '$q', '$log', '$route',
      '$timeout','$location', 'DEBUG', 'utilService', AuthService]);

  function AuthService($rootScope, $http, $q, $log, $route, $timeout, $location, DEBUG, utilService) {

    var BASE_URL = DEBUG ? 'http://localhost:8888' : 'https://pingvin.online';
    var self = this;

    self.user            = undefined;
    self.isLogin         = undefined;
    self.login           = login;
    self.logout          = logout;
    self.signup          = signup;
    self.forgot          = forgot;
    self.confirmPassword = confirmPassword;
    self.isLog           = isLog;
    self.ifAuth          = ifAuth;
    self.ifAuthOnce      = init();
    self.isOwn           = isOwn;
    self.redirect        = redirect;
    self.dOnlyAuth       = dOnlyAuth;
    self.isa             = isa;
    self.reloadRout      = reload;
    self.social          = {
      fb: loginFacebook,
      vk: loginVk,
      tw: loginTwitter,
      gp: loginGooglePlus
    };


    $log            = $log.getInstance('AuthService');



    function init(calback) {
      return $http.get('/api/me/').then(function (response) {
        var data = response.data;
        if (!data.hasOwnProperty('error')) {
          self.user = data;
          emitLogin();
          if (angular.isFunction(calback)) calback();
          return data;
        } else {
          self.user = null;
          emitNotLogin();
          redirect();
          return $q.reject(data);
        }
      }, function (response) {
        $log.log('me fail', response);
        emitNotLogin();
        redirect();
      });
    }

    function ifAuth(calbackAuth, calbackNotAuth) {
      $log.log(self.isLog());
      if (hasAnswer()) {
        if (self.isLog() && angular.isFunction(calbackAuth)) calbackAuth();
        else if (!self.isLog() && angular.isFunction(calbackNotAuth)) calbackNotAuth();
      }
      $rootScope.$on('auth', function (event, data) {
        if (data && data.hasOwnProperty('state')) {
          if (data.state && angular.isFunction(calbackAuth)) calbackAuth();
          else if (!data.state && angular.isFunction(calbackNotAuth)) calbackNotAuth();
        }
      });
    }

    function isa() {
      return !!self.user && self.user.is_admin;
    }

    function redirect(url) {
      var u = url || '/posts/';
      switch ($location.path()) {
        case '/settings/':
        case '/about-me/':
        case '/about-me-edit/':
        case '/activity/':
        case '/feedback/':
        case '/search/':
          $log.log('logout or not auth - redirect to /posts/');
          //$location.url(u).replace();
          var message = {
            error: 401,
            description: 'Эта страница доступна только для авторизованных пользователей'
          };
          $rootScope.$broadcast('error', message);
          break;
      }
    }

    function isOwn(user) {
      return !angular.isUndefined(self.user)
        && self.user !== null
        && self.user.id === user.id;
    }

    function emitLogin() {
      $log.log('emit login');
      self.isLogin = true;
      $rootScope.$broadcast('auth', {state: true});
      $timeout(function() { localStorage.setItem('pingvin-login', 1); }, 500);

    }

    function emitNotLogin() {
      $log.log('emit not login');
      self.isLogin = false;
      $rootScope.$broadcast('auth', {state: false});
      $timeout(function() { localStorage.setItem('pingvin-login', 0); }, 500);
    }

    function isLog() {
      return !!self.user;
    }

    function hasAnswer() {
      return angular.isDefined(self.user);
    }

    function reload() {
      $log.log('+++route reload+++');
      $timeout(function () {
        $route.reload();
      });
    }

    function clearError() {
      $rootScope.$broadcast('noError');
    }

    function logout(calback) {
      return $http.post('/api/logout/').then(function success(response) {
        var data = response.data;
        if (data.hasOwnProperty('error')) return $q.reject(data);
        self.user = null;
        emitNotLogin();
        if (angular.isFunction(calback)) calback();
        redirect();
        reload();
        return data;
      }, function fail(response) {
        $log.log('logout fail', response);
      });
    }

    function signup(data, calback) {
      return $http.post('/api/signup/', data).then(function (response) {
        var data = response.data;
        $log.log('signup', data);
        if (data.hasOwnProperty('error')) {
          return $q.reject(data);
        }
        if (angular.isFunction(calback)) calback();
        return data;
      }, function (response) {
        $log.log('signup fail', response);
      });
    }

    function login(data, calback) {
      return $http.post('/api/login/', data).then(function (response) {
        var data = response.data;
        $log.log('login', data);
        if (data.hasOwnProperty('error')) {
          emitNotLogin();
          return $q.reject(data);
        }
        self.user = data;
        emitLogin();
        if (angular.isFunction(calback)) calback();
        reload();
        clearError();
        return data;
      }, function (response) {
        $log.log('login fail', response);
        emitNotLogin();
      });
    }

    function forgot(data) {
      return $http.post('/api/password/reset/', data).then(function (response) {
        var data = response.data;
        $log.log('forgot', data);
        if (data.hasOwnProperty('error')) {
          return $q.reject(data);
        }
        return data;
      }, function (response) {
        $log.log('signup fail', response);
        return response;
      });
    }

    function confirmPassword(data) {
      return $http.post('/api/password/reset/confirm/', data).then(function (response) {
        var data = response.data;
        $log.log('confirm password', data);
        if (data.hasOwnProperty('error')) {
          return $q.reject(data);
        }
        return data;
      }, function (response) {
        $log.log('confirm password fail', response);
        return response;
      });
    }

    function dOnlyAuth(f) {
      if (isLog()) {
        return function () {
          return f.apply(this, arguments);
        }
      } else {
        return utilService.confirmNotLogin;
      }
    }

    function loginFacebook() {
      var ID_APP = '1878413785741841';
      var API_VER = '2.9';
      var REDIRECT_URL = BASE_URL + '/social-login/?sn=fb';
      var url = 'https://www.facebook.com/v2.9/dialog/oauth?client_id=' + ID_APP +
        '&redirect_uri=' + REDIRECT_URL + '&scope=email&response_type=code';
      utilService.popupCenter(url, 'Login fb.com', 626, 436);
    }

    function loginVk() {
      var ID_APP = '6097367';
      var API_VER = '5.65';
      var REDIRECT_URL = BASE_URL + '/social-login/?sn=vk';
      var url = 'https://oauth.vk.com/authorize?client_id=' + ID_APP +
        '&display=popup&redirect_uri=' + REDIRECT_URL + '&response_type=code&v=' + API_VER;
      utilService.popupCenter(url, 'Login vk.com', 626, 436);
    }

    function loginGooglePlus() {
      var ID_APP = '668548278228-htf1num1hdo8kmtimhrno819ub0rl0qo.apps.googleusercontent.com';
      var API_VER = '2';
      var REDIRECT_URL = BASE_URL + '/social-login/?sn=gp';
      var url = 'https://accounts.google.com/o/oauth2/v' + API_VER + '/auth?client_id='+ ID_APP +
        '&access_type=offline&redirect_uri=' + REDIRECT_URL + '&response_type=code' +
        '&scope=https://www.googleapis.com/auth/userinfo.email+' +
        'https://www.googleapis.com/auth/plus.login+' +
        'https://www.googleapis.com/auth/userinfo.profile';
      utilService.popupCenter(url, 'Login google.com', 626, 436);
    }

    function loginTwitter() {

    }



  }

}());