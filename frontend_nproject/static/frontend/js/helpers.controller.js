/**
 * Created by maks on 19.10.16.
 */

(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .controller('PostDetailHelpersController', ['$scope', '$log',
      '$mdDialog', '$timeout', 'authService', 'conditionService', 'utilService',
      'dataService', 'activityType', 'post',
      PostDetailHelpersController]);

  function PostDetailHelpersController($scope, $log, $mdDialog, $timeout,
                                       authService, conditionService, utilService,
                                       dataService, activityType, post) {

    var self = this;

    self.toast           = utilService.initToast();
    self.dataHelpers     = dataService.helpers(post.id);
    self.fullName        = utilService.fullName;
    self.condition       = function (condition) { return conditionService.mtoString(condition); };
    self.isOwn           = authService.isOwn;
    self.isOwner         = self.isOwn(post.created_user);
    self.helpers         = [];
    self.updateCondition = updateCondition;
    self.selectHelper    = selectHelper;
    self.cancelHelper    = cancelHelper;
    self.doneHelp        = doneHelp;
    self.isShowGraphic   = false;
    self.loading         = false;
    self.floading        = false;
    self.isShowButton    = isShowButton;
    self.moreHelpers     = moreHelpers;
    self.avatar          = utilService.getUMin;
    self.fdate           = utilService.fdate;
    self.hStatus         = activityType;

    $scope.cancel        = function () { $mdDialog.cancel(); };

    $log                 = $log.getInstance('PostDetailHelpersController');

    init();

    function init() {
      self.dataHelpers.list.init();
      moreHelpers(true);
      google.charts.load('current', {
        packages:['corechart'],
        'language': 'ru'
      });
      $scope.$on('updateCondition', function () {
        $log.log('$on updateCondition');
        drawStuff();
      });
    }

    function updateCondition(helper, ev) {
      var parentElem = angular.element('#post-detail-helpers').parent();
      utilService.dialog({
        multiple: true,
        controller: 'HelpersUpdateConditionController',
        locals: { idPost: post.id, helper: helper },
        templateUrl: '/public/html/post-new-motivation.html',
        skipHide: true,
        scope: $scope.$new(),
        onShowing: function () { parentElem.addClass('second_plan_dialog'); },
        onRemoving: function () { parentElem.removeClass('second_plan_dialog'); }
      });
    }

    function moreHelpers(first) {
      if (self.loading) return;
      self.loading = true;
      self.floading = true;
      self.dataHelpers.list.next()
        .then(function success(data) {
          self.helpers = self.helpers.concat(data);
          $log.log(self.helpers);
          if (first)
            firstShowGraphic();
          else
            drawStuff();

          self.floading = false;
          self.loading = false;
        }, function fail() {
        });
    }

    function isShowButton() {
      return self.dataHelpers.list.hasNext() && self.helpers.length !== 0;
    }

    function selectHelper(helper) {
      var confirm = $mdDialog.confirm({
        multiple: true,
        title: 'Вы хотите выбрать помощником ' + self.fullName(helper.user) + '?',
        textContent: 'Помощнику откроются ваши контакты, что бы он с вами связался. ',
        ok: 'Выбрать',
        cancel: 'Отмена',
        clickOutsideToClose: true,
        fullscreen:          true,
        escapeToClose:       true,
        disableParentScroll: true,
        skipHide:            true
      });

      $mdDialog.show(confirm).then(function () {
        dataService.user.selectHelper(post.id, helper.user.id )
          .then(function(data) {
            if (data.hasOwnProperty('status'))
              helper.status = data.status;
            self.toast('Помощник выбран');
          });
      });
    }

    function cancelHelper(helper) {
      dataService.user.cancelHelper(post.id, helper.user.id)
        .then(function(data) {
          if (data.hasOwnProperty('status'))
            helper.status = data.status;
          self.toast('Выбор помощника отменен');
        });
    }

    function doneHelp(helper, ev) {
      var parentElem = angular.element('#post-detail-helpers').parent();

      utilService.dialog({
        multiple: true,
        controller: 'HelpersDoneController',
        locals: { postId: post.id ,helper: helper},
        templateUrl: '/public/html/post-done-help.html',
        skipHide: true,
        scope: $scope.$new(),
        onShowing: function () { parentElem.addClass('second_plan_dialog'); },
        onRemoving: function () { parentElem.removeClass('second_plan_dialog'); }
      });

    }

    function createRows() {
      var result = [];
      self.helpers.forEach(function (helper) {
        if (parseInt(helper.condition.type, 10) === 2) {
          result.push([
            self.fullName(helper.user),
            helper.condition.money
          ]);
        }
      });
      return result;
    }

    function drawStuff() {
      var rows = createRows();
      self.isShowGraphic = rows.length ? true : false;
      if (self.isShowGraphic) {
        $timeout(function () {
          var data = new google.visualization.DataTable(),
            options = {
              title: 'График предложений',
              width: 450,
              height: 400,
              legend: {position: 'none'},
              bar: {groupWidth: '95%'},
              hAxis: {title: 'Пользователи', format: 'decimal', showTextEvery: 1},
              vAxis: {title: 'Рубли'}
            };
          data.addColumn('string', 'Пользователь');
          data.addColumn('number', 'Руб.');
          data.addRows(rows);

          $timeout(function () {
            var chart = new google.visualization.ColumnChart(document.getElementById('chart-user-money'));
            chart.draw(data, options);
          });
        });
      }
    }

    function firstShowGraphic() {
      google.charts.setOnLoadCallback(drawStuff);
    }

  }
}());