/**
 * Created by maks on 21.10.16.
 */

(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .controller('PostDetailController', ['$scope', '$rootScope', '$http', '$log',
      '$mdDialog', '$timeout', '$location', '$mdMedia', '$mdMenu', 'conditionService',
      'categoryService', 'authService', 'utilService', 'dataService', 'option',
      PostDetailController])
    .directive('mapLoad', ['$rootScope', '$log',
      function ($rootScope, $log) {
        return {
          restrict: 'A',
          link: function(scope, element, attrs) {
            element.bind('load', function() {
              $rootScope.$broadcast('mapLoad');
              $log.log('map be loaded');
            });
            element.bind('error', function(){
              $log.log('map could not be loaded');
            });
          }
        }
      }]);


  function PostDetailController($scope, $rootScope, $http, $log, $mdDialog, $timeout,
                                $location, $mdMedia, $mdMenu, conditionService, categoryService,
                                authService, utilService, dataService, option) {

    var self = this;

    var pData = option.pData;

    self.postId        = option.postId;
    self.isModer       = option.isModer || false;
    self.isClosed      = option.isClosed || false;
    self.post          = undefined;
    self.getLabelCond  = function () { return conditionService.mtoString(self.post.condition); };
    self.mapUrl        = utilService.mapUrl;
    self.showHelpers   = showHelpers;
    self.isShowBtn     = false;
    self.isOwner       = undefined;
    self.isa           = undefined;
    self.isEvent       = undefined;
    self.userAva       = undefined;
    self.isLog         = authService.isLog;
    self.button        = button();
    self.name          = utilService.fullName;
    self.toast         = utilService.initToast();
    self.title         = categoryService.getTitle;
    self.fdate         = utilService.fdate;
    self.titlePost     = titlePost;
    self.isLoaded      = undefined;
    self.hasError      = false;
    $scope.error       = '';
    $scope.description = '';
    $scope.$mdMenu     = $mdMenu;

    self.small         = { post: true, comments: false };
    self.smallShowPost = function () { self.small.post=true; self.small.comments=false };
    self.smallShowCmnt = function () { self.small.post=false; self.small.comments=true };

    $scope.$mdMedia    = $mdMedia;

    $log               = $log.getInstance('PostDetailController');

    var scrollBarSelector;

    init();


    function init() {
      pData.get(self.postId)
        .then(function success(data) {
          self.post = data;
          self.isLoaded = true;
          self.isEvent = self.post.type === 1;
          self.userAva = utilService.getUMin(self.post.created_user);
          var initAuth = function () {
            self.isOwner = authService.isOwn(self.post.created_user);
            self.isa = authService.isa();
            helpBtnInit();
          };
          authService.ifAuthOnce.then(initAuth, initAuth);


          $scope.$watch(function() { return $mdMedia('gt-sm'); }, function(isGtSm) {
            $timeout(function () {
              scrollBarSelector = $('.j-post-detail-content-wrap');
              isGtSm ? scrollBarSelector.perfectScrollbar() : scrollBarSelector.perfectScrollbar('destroy');
            }, 50);
          });
          $scope.$on('mapLoad', function () {
            $log.log('$on mapLoad');
            if (angular.isDefined(scrollBarSelector)) {
              scrollBarSelector.perfectScrollbar('update');
              $log.log('scrollbar update');
            }
          });

          $scope.$on('helpUser', function () {
            $log.log('$on helpUser');
            self.isShowBtn = false;
          });
        }, function fail(response) {
          self.hasError = true;
          $scope.error = utilService.getErrorCode(response);
          switch (parseInt($scope.error, 10)) {
            case 404:
              $scope.description = 'Публикация не найдена';
              break;
          }

        });
    }

    function helpBtnInit() {
      if (self.isOwner || self.post.is_help) return;
      self.isShowBtn = true;
    }

    function showHelpers(ev) {
      var parent = angular.element('.post-detail-dialog').parent();

      utilService.dialog({
        multiple: true,
        controller: 'PostDetailHelpersController',
        templateUrl: '/public/html/post-detail-helpers.html',
        skipHide: true,
        scope: $scope.$new(),
        locals: { post: self.post },
        onShowing: function () { parent.addClass('second_plan_dialog'); },
        onRemoving: function () { parent.removeClass('second_plan_dialog'); }
      });
    }

    $scope.cancel = function () {
      $mdDialog.cancel();
    };

    function titlePost() {
      if (angular.isUndefined(self.post))
        return '';
      switch(parseInt(self.post.type)) {
        case 0: return 'Объявление';
        case 1: return 'Событие';
        default: return '';
      }
    }

    function button() {
      return {
        close: close,
        help: help,
        moderated: moderated,
        cancelClose: cancelClose
      };

      function close() {
        var parentElem = angular.element('.post-detail-dialog').parent();
        parentElem.addClass('second_plan_dialog');
        var confirm = $mdDialog.confirm({
          multiple: true,
          title: 'Удалить публикацю',
          textContent: 'Вы хотите удалить публикацю ' + self.post.header + '?',
          ok: 'Удалить',
          cancel: 'Отмена',
          clickOutsideToClose: true,
          fullscreen:          true,
          escapeToClose:       true,
          disableParentScroll: true,
          skipHide:            true,
          onRemoving: function () { parentElem.removeClass('second_plan_dialog'); }
        });

        $mdDialog.show(confirm).then(function () {
          dataService.user.close(self.post.id, dataService.user.id)
            .then(function(data) {
              if (!data.hasOwnProperty('is_actually') || data.is_actually) return;
              $rootScope.$broadcast('postClose', {id: self.post.id});
              self.toast('Публикация удалена');
              $mdDialog.cancel();
            });
        });
      }

      function help(ev) {
        var parentElem = angular.element('.post-detail-dialog').parent();
        utilService.dialog({
          multiple: true,
          controller: 'PostDetailHelpController',
          locals: { post: self.post },
          templateUrl: '/public/html/post-new-motivation.html',
          skipHide: true,
          scope: $scope.$new(),
          onShowing: function () { parentElem.addClass('second_plan_dialog'); },
          onRemoving: function () { parentElem.removeClass('second_plan_dialog'); }
        });
      }

      function moderated(ev) {
        dataService.user.moderationOver(self.post.id)
          .then(function () {
            $rootScope.$broadcast('postClose', {id: self.post.id});
            self.toast('Публикация прошла модерацию');
            $mdDialog.cancel();
          });
      }

      function cancelClose(ev) {
        dataService.user.unblock(self.post.id)
          .then(function () {
            $rootScope.$broadcast('postClose', {id: self.post.id});
            self.toast('Отмена удаления публикации');
            $mdDialog.cancel();
          });
      }
    }

  }
}());
