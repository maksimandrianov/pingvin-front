/**
 * Created by maks on 03.04.17.
 */


(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('PostNewMotivationController', ['$scope', PostNewMotivationController]);


  function PostNewMotivationController($scope) {
    var self = this;
    self.okBtnName = 'Добавить';
    self.header = 'Добавить мотивацию';
    self.text = 'Вы можете указать дополнительную мотивацию для помощников и увеличить скорость отклика.';
    self.condition = {
      'type': 1,
      'money': 0,
      'other': ''
    };
    self.save = function () {
      $scope.$emit('motivationIsReady', self.condition);
    };
  }

}());