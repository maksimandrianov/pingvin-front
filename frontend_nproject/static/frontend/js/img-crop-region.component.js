/**
 * Created by maks on 04.03.17.
 */

(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .component('imgCropRegion', {
      templateUrl: '/public/html/img-crop-region.html',
      controller: ['$scope', '$log', '$rootScope', '$timeout', 'utilService',
        ImgCropRegionController],

      bindings: {
        picFile: '=?',
        cropCoords: '=?',
        progress: '=?',
        loading: '=?',
        areaMinSize: '=?',
        type: '@'
      }
    });

  function ImgCropRegionController($scope, $log, $rootScope, $timeout, utilService) {
    var self = this;

    self.mode        = 0;
    self.errFile     = undefined;
    self.cropFile    = undefined;
    self.progress    = 0;
    self.hintCrop    = '';
    self.cropImg     = cropImg;
    self.cancel      = cancel;
    self.areaMinSize = undefined;

    self.areaClass   = undefined;
    self.resultSize  = undefined;
    self.atype       = undefined;
    self.ratio       = undefined;
    self.pattern     = '.jpg,.jpe.,jpeg,.bmp,.png';
    self.isCrop      = true;

    self.fload       = fload;

    self.toast       = utilService.initToast();

    var msg1 = 'Кликните, что бы выбрать изображение',
      msg2 = 'Выберите облать фотографии, а после нажимте на кнопку Обрезать, что бы сделать предпросмотр',
      msg3 = 'Нажмите Добавить, что бы загрузить изображение';


    self.$onInit = init;

    function init() {

      switch (self.type) {
        case 'avatar':
          self.areaClass = 'crop-area-avatar';
          self.resultSize = {w: 250, h: 250};
          self.atype = 'square';
          break;
        case 'post':
          self.areaClass = 'crop-area-post';
          self.resultSize = {w: 400, h: 250};
          self.atype = 'rectangle';
          break;
        case 'event':
          self.areaClass = 'crop-area-post';
          self.resultSize = {w: 700, h: 300};
          self.atype = 'rectangle';
          break;
        case 'blog':
          self.areaClass = 'crop-area-post';
          self.resultSize = {w: 700, h: 300};
          self.atype = 'rectangle';
          break;
        default:
          self.areaClass = 'crop-area-post';
          self.atype = 'rectangle';
          self.pattern += ',.gif';
          $log.log('default');
      }
      if (self.resultSize)
        self.ratio = self.resultSize.w / self.resultSize.h;
      self.hintCrop = msg1;

      $scope.$watch(function () {
        return self.progress;
      }.bind(self), function (newVal) {
      }.bind(self));

      $scope.$watch(function () {
        return self.picFile;
      }.bind(self), function (newVal) {
        if (newVal) self.isCrop = newVal.type !== 'image/gif';
      }.bind(self));

      $scope.$on('resetImgCropRegion',  function () {
        resetImg();
      })

    }

    function fload() {
      var mode = self.picFile ? 1 : 0;
      if (mode == 1) {
        var func = function (img) {

          if (self.resultSize &&  self.ratio) {

            var imgHeight = img.height,
              imgWidth = img.width,
              size = self.resultSize,
              errorCoff = 1.05;

            if (imgHeight < size.h || imgWidth < size.w) {
              $log.error('error size');
              resetImg();
              $scope.$emit('ImgCropRegionError', {
                message: 'Минимальная ширина фото - ' + size.w +
                ', а минимальная высота - ' + size.h
              });
              self.toast('Минимальный размер изображения ' + size.w + 'px на ' + size.h + 'px');
              return;
            } else {
              self.mode = mode;
            }

            var areaSize = utilService.realSize($('.' + self.areaClass));
            var coffW = areaSize.width / imgWidth;
            var coffH = areaSize.height / imgHeight;
            var coff = (coffW < 1 && coffH < 1) ?
              (coffW < coffH ? coffW : coffH) : 1;

            $timeout(function () {
              self.areaMinSize = self.resultSize.w === self.resultSize.h ?
              self.resultSize.w * coff : {
                w: Math.ceil(self.resultSize.w * coff * self.ratio),
                h: Math.ceil(self.resultSize.h * coff)
              };
            });
          } else {
            self.mode = mode;
          }
          $timeout(function () {
            self.hintCrop = msg2;
            $('#fix-crop').closest('.slide-toggle').css('height', 'auto');
          });
        };
        $timeout(function () {
          utilService.getMetaImg(self.picFile.$ngfBlobUrl, func);
        });

      }
    }


    function resetImg() {
      self.errFile = undefined;
      self.picFile = undefined;
      self.cropFile = undefined;
      self.v = 0;
      self.mode = 0;
      self.hintCrop = msg1;
    }


    function cancel() {
      if (self.mode === 1) {
        resetImg();
      } else if (self.mode === 2) {
        self.cropFile = undefined;
        self.progress = 0;
        self.mode = 1;
        self.hintCrop = msg2;
      }
    }

    function cropImg() {
      self.mode = 2;
      self.hintCrop = msg3;
    }
  }

}());
