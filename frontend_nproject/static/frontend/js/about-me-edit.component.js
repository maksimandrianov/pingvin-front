/**
 * Created by maks on 29.10.16.
 */

/*jslint browser: true,
 nomen: true,
 white: true */
/*global moment, angular*/


(function () {
  'use strict';

  angular.module('pingvinApp').component('aboutMeEdit', {
    templateUrl: '/public/html/about-me-edit.html',
    controller: ['$scope', '$http', '$log', '$rootScope',
      '$routeParams', '$location', '$mdDialog', '$timeout', 'authService',
      'categoryService', 'contactsService', 'utilService', 'dataService',
      'contactsType', AboutMeEditController]
  });

  function AboutMeEditController($scope, $http, $log,  $rootScope,
                                 $routeParams, $location, $mdDialog, $timeout, authService,
                                 categoryService, contactsService, utilService, dataService, contactsType) {

    var self  = this;

    // common info
    self.common        = undefined;
    self.ocommon       = {};
    self.isCommonHint  = undefined;
    self.commonHints   = undefined;
    self.updateCommon  = updateCommon;
    // contacts
    self.contacts      = [];
    self.ocontacts     = [];
    self.description   = contactsService.getDescription;
    self.contactsTypes = contactsService.contacts;
    self.addContact    = addContact;
    self.rmContact     = rmContact;
    self.updateContacts= updateContacts;
    self.changeContact = changeContact;
    self.contact       = '';
    self.pattern       = '';
    self.patternMsg    = '';
    // categories
    self.allcategories = categoryService.categories;
    self.categories    = [];
    self.ocategories   = [];
    self.ctitle        = categoryService.getTitle;
    self.addCategory   = addCategory;
    self.updateCategories = updateCategories;

    self.to           = to;
    self.current      = undefined;
    self.fromp        = undefined;
    self.toast        = utilService.initToast();
    self.getStyle     = utilService.getStyle;

    // for validator
    var currentDate = new Date();
    self.minDate = new Date(
      currentDate.getFullYear() - 100,
      currentDate.getMonth(),
      currentDate.getDate()
    );
    self.maxDate = new Date(
      currentDate.getFullYear() - 12,
      currentDate.getMonth(),
      currentDate.getDate()
    );

    $log = $log.getInstance('AboutMeEditController');


    init();

    function init() {
      $rootScope.showProgress = true;
      reloadCtrl();
      $scope.$on('$locationChangeStart', function (event, newUrl, oldUrl) {
        if ($location.path() == '/about-me-edit/') return;
        $log.log(newUrl, oldUrl, isChange());
        if (isChange()) {
          utilService.confirmNotSaved()
            .then(function () {
              fixChange();
              $location.$$parse(newUrl);

            }, function () {
              if (newUrl.indexOf('?act=') > 0) self.current = self.fromp;
            });
          event.preventDefault();
          $log.log('$locationChangeStart preventDefault');
        }
      });

      authService.ifAuth(function good () {
        dataService.users()
          .get(authService.user.id)
          .then(function (data) {
            initCommon(data);
            initContacts(data);
            initCategories(data);
            getIsCommonHint();
            getCommonHints();
            $rootScope.showProgress = false;
            $log.log(data);
          }, function () {

          })
      }, function () {
        $rootScope.showProgress = false;
      });


      $scope.$on('changeTab', function(ev, data) {
        if  (self.current == data) return;
        if (angular.isDefined(self.common) && isChange()) {
          utilService.confirmNotSaved()
            .then(function () {
              fixChange();
              to(data);
              initLocation();
            }, function () {
              $scope.$emit('fixCurrentTab');
            });
        } else {
          to(data);
          initLocation();
        }
      });
    }

    function changeContact() {
      self.contactData = '';
      switch (self.contact) {
        case contactsType.EMAIL:
          self.pattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;
          self.patternMsg = 'Почта не валидна';
          break;
        case contactsType.VK:
          self.pattern = /(https{0,1}:\/\/)?(www\.)?(vk.com\/)(id\d|[a-zA-z][a-zA-Z0-9_.]{2,})/;
          self.patternMsg = 'Ссылка на vk не валидна';
          break;
        case contactsType.FB:
          self.pattern = /(?:http:\/\/)?(?:www\.)?facebook\.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-]*)/;
          self.patternMsg = 'Ссылка на fb не валидна';
          break;
        case contactsType.PH_MOBILE:
          self.pattern =  /^\+[1-9]{1}[0-9]{3,14}$/;
          self.patternMsg = 'Номер телефона не валиден. Например +79651000000';
          break;
        case contactsType.PH_HOME:
          self.pattern =  /^\+[1-9]{1}[0-9]{3,14}$/;
          self.patternMsg = 'Номер телефона не валиден. Например +78125768850, ' +
            'где +7 - код страны, 812-код города, далее номер телефона.';
          break;
        default:
          clearContactSelect();

      }
    }


    function reloadCtrl() {
      $rootScope.$broadcast('reloadCtrl', {
        state: 'url-about-me-edit',
        name: 'Редактировать',
        tabs: [
          {
            label: 'Общая информация',
            value: 'common'
          },
          {
            label: 'Контакты',
            value: 'contacts'
          },
          {
            label: 'Категории',
            value: 'categories'
          }
        ],
        param: 'act'
      });
    }

    function newCommon(data) {
      return {
        first_name: data.first_name,
        second_name: data.second_name,
        gender: data.gender,
        bday: data.bday ? new Date(data.bday) : data.bday,
        info: data.info
      };
    }

    function initCommon(data) {
      self.common = newCommon(data);
      angular.copy(self.common, self.ocommon);
    }

    function initContacts(data) {
      angular.copy(data.contacts, self.contacts);
      angular.copy(self.contacts, self.ocontacts);
    }

    function initCategories(data) {
      angular.copy(data.categories, self.categories);
      angular.copy(self.categories, self.ocategories);
    }

    function initLocation() {
      var param = $location.$$search.act;
      if (param === 'common') self.current = 0;
      else if (param === 'contacts') self.current = 1;
      else if (param === 'categories')  self.current = 2;
    }


    function to(tab) {
      if (tab == 0) $location.search('act', 'common');
      else if (tab == 1) $location.search('act', 'contacts');
      else if (tab == 2) $location.search('act', 'categories');
      self.fromp = self.current;
    }

    function fixChange() {
      if (!angular.equals(self.common, self.ocommon)) {
        angular.copy(self.ocommon, self.common);
      }
      if (!angular.equals(self.contacts, self.ocontacts)) {
        angular.copy(self.ocontacts, self.contacts);
      }
      if (!angular.equals(self.categories, self.ocategories)) {
        angular.copy(self.ocategories, self.categories);
      }
    }

    function isChange() {
      return !(!!self.common &&
      angular.equals(self.common, self.ocommon) &&
      angular.equals(self.contacts, self.ocontacts) &&
      angular.equals(self.categories, self.ocategories));
    }

    function getCommonHints() {
      if (!self.ocommon) return undefined;
      var arr = [];
      if (self.ocommon.gender === 'x') arr.push('пол');
      if (!self.ocommon.bday) arr.push('дату рождения');
      if (!self.ocommon.info.length) arr.push('информацию о себе');
      self.commonHints =  arr.join(', ');
    }

    function getIsCommonHint() {
      self.isCommonHint = self.ocommon.gender === 'x' ||
        !self.ocommon.bday ||
        !self.ocommon.info.length;
    }

    function addCategory(category, subcategory) {
      var i,
        obj = {
          category_type: category,
          subcategory_type: subcategory
        };
      for (i = 0; i < self.categories.length; ++i) {
        if (angular.equals(obj, self.categories[i])) return;
      }
      self.categories.push(obj);
    }

    function rmContact(index) {
      self.contacts.splice(index, 1);
    }

    function addContact(type, data) {
      if (!self.contacts || !self.contactData) {
        return;
      }
      var i,
        obj = {
          type: type,
          data: data
        };
      for (i = 0; i < self.contacts.length; ++i) {
        if (angular.equals(obj, self.contacts[i])) return;
      }
      self.contacts.push(obj);
      clearContactSelect();
      self.contactData = '';
    }

    function clearContactSelect() {
      self.contact       = '';
      self.pattern       = '';
      self.patternMsg    = '';
    }

    function update(data, callback) {
      dataService.users()
        .update(authService.user.id, data)
        .then(callback, utilService.dHandleError(self.toast));
    }

    function updateCommon() {
      if (!isChange()) {
        self.toast('Данные успешно сохранены.');
        return;
      }
      var common = newCommon(self.common);
      common.bday = common.bday ?
        moment(common.bday).format('YYYY-MM-DD') : null;
      update(common, function (data) {
        if (data.first_name !== self.ocommon.first_name) {
          $scope.$emit('newName', data.first_name);
          $log.log('$emit newName');
        }
        initCommon(data);
        getIsCommonHint();
        getCommonHints();
        self.toast('Данные успешно сохранены.');
      });
    }

    function updateContacts() {
      if (!isChange()) {
        self.toast('Данные успешно сохранены.');
        return;
      }
      update({contacts: self.contacts}, function (data) {
        $log.log(data);
        initContacts(data);
        self.toast('Данные успешно сохранены.');
      });
    }

    function updateCategories() {
      if (!isChange()) {
        self.toast('Данные успешно сохранены.');
        return;
      }
      update({categories: self.categories}, function (data) {
        initCategories(data);
        self.toast('Данные успешно сохранены.');
      });
    }
  }
}());
