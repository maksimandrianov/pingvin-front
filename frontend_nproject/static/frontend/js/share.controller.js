/**
 * Created by maks on 01.07.17.
 */

(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('ShareController', ['$scope', '$rootScope', '$log', '$mdDialog', 'utilService', 'post',
      ShareController]);

  function ShareController($scope, $rootScope, $log, $mdDialog, utilService, post) {


    var defaultImg      = 'http://pingvin.online/public/img/logo_email.png';

    var self = this;

    self.share           = utilService.share;
    self.shareVk         = shareVk;
    self.shareFacebook   = shareFacebook;
    self.shareTwitter    = shareTwitter;
    self.shareGooglePlus = shareGooglePlus;
    self.url             = '';
    self.header          = '';
    self.text            = '';
    self.img             = '';
    self.isPost          = !!post;

    $scope.cancel        = function () { $mdDialog.cancel();};
    $log                 = $log.getInstance('ShareController');

    init();

    function init() {
      var baseUrl = 'https://pingvin.online/'; // todo fix hardcode
      if (post) {
        self.url = post.type ?  baseUrl + 'events/?event=' + post.id : baseUrl + 'posts/?post=' + post.id;
      } else {
        self.url = baseUrl;
      }
      self.header = post ? post.header :
        'Pingvin.online поможет найти рядом людей, которым нужна помощь, и людей, готовых помочь.';
      self.text = post ? post.text : '';
      self.img = post ? image() : defaultImg;
    }

    function shareVk() {
      var shareUrl = self.share.vk(self.url, self.header, self.img);
      popup(shareUrl);
      $log.log(shareUrl);
    }

    function shareFacebook() {
      var shareUrl = self.share.fb(self.url, self.header, self.text, self.img);
      popup(shareUrl);
      $log.log(shareUrl);
    }

    function shareTwitter() {
      var shareUrl = self.share.tw(self.url, self.header + ' - ');
      popup(shareUrl);
      $log.log(shareUrl);
    }

    function shareGooglePlus() {
      var shareUrl = self.share.gp(self.url, self.header);
      popup(shareUrl);
      $log.log(shareUrl);
    }

    function image() {
      return post.h_style && post.h_style.img ?
        ('https://pingvin.online/public' + post.h_style.img) : defaultImg; // todo fix hardcode
    }

    function popup(url) {
      utilService.popupCenter(url, 'Поделиться', 626, 436);
    }


  }
}());