/**
 * Created by maks on 03.04.17.
 */


(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('HelpersUpdateConditionController', ['$scope', '$timeout', '$log',
      'dataService', '$mdDialog', 'utilService', 'conditionService', 'idPost', 'helper',
      HelpersUpdateConditionController]);


  function HelpersUpdateConditionController($scope, $timeout, $log, dataService, $mdDialog,
                                            utilService, conditionService, idPost, helper) {
    var self = this;

    self.okBtnName = 'Изменить';
    self.header    = 'Вы хотите изменить предложение ?';
    self.text      = 'Укажите более интересное предложение для автора публикации и тогда он выберет именно вас.';
    self.condition = {};
    self.save      = save;
    self.toast     = utilService.initToast();

    init();

    function init() {
      if (helper.condition) {
        angular.copy(helper.condition, self.condition);
      } else {
        self.condition = { 'type': 1 };
      }
    }

    function save() {
      var id = idPost;
      conditionService.prepareCondition(self.condition);
      dataService.user.updateCondition(id, self.condition)
        .then(function(data) {
          helper.condition = data.condition;
          $scope.$emit('updateCondition');
          self.toast('Предложение обновлено');
          $mdDialog.cancel();
        });
    }
  }

}());