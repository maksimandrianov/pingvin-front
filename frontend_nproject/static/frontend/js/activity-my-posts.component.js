/**
 * Created by maks on 26.07.17.
 */


(function () {
  'use strict';

  angular.module('pingvinApp').component('activityMyPosts', {
    templateUrl: '/public/html/post-list-one-col.html',
    controller: ['$scope', '$log', '$rootScope', '$mdDialog',
      '$location', '$controller', '$timeout', 'dataService',
      'utilService',  ActivityMyPostsController]

  });

  function ActivityMyPostsController($scope, $log, $rootScope, $mdDialog, $location, $controller,
                                  $timeout, dataService, utilService) {

    var self = this;

    self.hasHeader              = false;
    self.currDescription        = false;
    self.isActivity             = true;
    self.msgNoInfo              = 'Пока нет ваших актуальных публикаций';

    angular.extend(self, $controller('BaseActivityController', {
      $scope: $scope,
      $rootScope: $rootScope,
      $log: $log,
      $timeout: $timeout,
      $controller: $controller,
      $mdDialog: $mdDialog,
      $location: $location,
      utilService: utilService,
      dataService: dataService
    }));


    init();

    function init() {
      var params = {
        data: dataService.myPosts()
      };
      self.BaseActivityController(params);
    }
  }

}());
