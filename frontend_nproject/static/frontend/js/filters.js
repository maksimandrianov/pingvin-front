/**
 * Created by maks on 31.08.16.
 */

(function () {
  'use strict';


  angular
    .module('pingvinApp')
    .filter('cut', ['utilService', function (utilService) {
      return function (value, wordwise, max, tail) {
        if (!value)
          return '';

        max = parseInt(max, 10);
        if (!max) {
          return value;
        }

        if (value.length <= max) {
          return value;
        }

        if (utilService.textFromHtml(value).length <= max) {
          return value;
        }

        var newMax = max, textLength = 0, htmlLength = 0;
        for (var i = 0; i < value.length; ++i) {
          if (value[i] === '<') {
            while (value[++i] !== '>' || htmlLength >= value.length) {
              ++htmlLength
            }
          } else {
            ++textLength;
            ++htmlLength;
          }
          if (textLength === max) {
            break;
          }
        }
        newMax = htmlLength;

        value = value.substr(0, newMax);
        var left = value.lastIndexOf('<a');
        if (left >= 0) {
          var right = value.indexOf('</a>', left);
          if (right < 0)
            value = value.substr(0, left);
        }


        if (wordwise) {
          var lastspace = value.lastIndexOf(' ');
          if (lastspace !== -1) {

            if (value.charAt(lastspace - 1) === '.' || value.charAt(lastspace - 1) === ',') {
              lastspace = lastspace - 1;
            }
            value = value.substr(0, lastspace);
          }
        }

        return value + (tail || '…');
      };
    }]);

  angular
    .module('pingvinApp')
    .filter('mlinky', function () {
      var urlPattern = /((http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?)( <(.*)?>)?/gi;
      return function (text, target, classes) {
        return text.replace(urlPattern, function (str, p1, p2, p3, p4, p5, p6, offset, s) {
          var url = p1,
            text = p6 || url,
            target = target || '_blank',
            classes = classes || 'link link_b link_blue';
          return '<a target="' + target + '" href="' + url + '" ' + 'class="' + classes +'"' + '>' + text + '</a>';
        });
      };
    });


}());