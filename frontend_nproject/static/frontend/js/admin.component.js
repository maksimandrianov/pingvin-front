/**
 * Created by maks on 26.07.17.
 */

(function () {
  'use strict';

  angular.module('pingvinApp').component('admin', {
    templateUrl: '/public/html/admin.html',
    controller: ['$scope', '$log', '$rootScope',
      '$location',  AdminController]

  });

  function AdminController($scope, $log, $rootScope, $location) {

    var self = this;
    self.current = undefined;
    self.$onInit = init;

    $log = $log.getInstance('AdminController');

    function init() {
      $rootScope.$broadcast('reloadCtrl', {state: 'url-admin'});
      $scope.$on('changeTab', function(ev, data) {
        if (self.current == data) return;
        to(data);
      });
    }

    function to(tab) {
      if (tab == 0) $location.search('act', 'feedback');
      else if (tab == 1) $location.search('act', 'moderation');
      else if (tab == 2) $location.search('act', 'closed');
      self.current = tab;
    }

  }
}());