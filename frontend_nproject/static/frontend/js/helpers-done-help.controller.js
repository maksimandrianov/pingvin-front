/**
 * Created by maks on 03.04.17.
 */


(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('HelpersDoneController', ['$scope', '$mdDialog', '$timeout', '$log',
      'dataService', 'utilService', 'helper', 'postId',
      HelpersDoneController]);


  function HelpersDoneController($scope, $mdDialog, $timeout, $log, dataService,
                                 utilService, helper, postId) {

    var self = this;

    self.helper       = helper;
    self.fullName     = utilService.fullName;
    self.rating       = 5;
    self.addComment   = addComment;
    self.isShowEditor = false;
    self.ratingTitle  = ratingTitle;
    self.toast        = utilService.initToast();

    $log             = $log.getInstance('DoneHelpController');

    var emojiBtn,
      emojiPicker,
      emojiWrapper,
      emojioneArea;


    init();

    function init() {
      $timeout(function () {
        var checkDoubleEnter = 1;
        emojioneArea = angular.element('#post-done-help-textarea').emojioneArea({
          pickerPosition: 'bottom',
          events: {
            keypress: function (editor, event) {
              var code = event.keyCode || event.which;
              if (code == 13) {
                ++checkDoubleEnter;
                emojioneArea[0].emojioneArea.hidePicker();
              }
              else checkDoubleEnter = 0;
              if (checkDoubleEnter === 2) {
                checkDoubleEnter = 1;
                event.preventDefault();
              }
            },
            keydown: function (editor, event) {
              var code = event.keyCode || event.which;
              if (code === 8) {
                var hasLen = emojioneArea[0].emojioneArea.getText().length;
                if (hasLen) checkDoubleEnter = 0;
                else checkDoubleEnter = 1;
                emojioneArea[0].emojioneArea.hidePicker();
              }

            },
            paste: function (editor, event) {
              var text = emojioneArea[0].emojioneArea.getText();
              text = text.replace(/\n\s*\n/g, '\n');
              text = text.replace(/\t\s*\t/g, ' ');
              emojioneArea[0].emojioneArea.setText(text);
              emojioneArea[0].emojioneArea.hidePicker();
            },
            click: function (editor, event) {
              emojioneArea[0].emojioneArea.hidePicker();
            }
          }
        });
        self.isShowEditor = true;
        $timeout(function () {
          initEmojiPicker();
          $(window).resize(function () {
            emojioneArea[0].emojioneArea.hidePicker();
          });
          emojiBtn.click(initEmojiPicker);
        });
      });
    }

    function addComment() {
      var text = emojioneArea[0].emojioneArea.getText().trim();
      text = text.replace(/\n\s*\n/g, '\n');
      text = text.replace(/\t\s*\t/g, ' ');

      var comment = {
        'rating': self.rating,
        'text': text
      };

      dataService.user.done(postId, helper.user.id, comment)
        .then(function(data) {
          $log.log(postId, helper.user.id, comment, data);
          if (data.hasOwnProperty('status'))
            helper.status = data.status;
          self.toast('Отзыв отправлен');
          $mdDialog.cancel();
        });
    }

    function initEmojiPicker() {
      emojiWrapper = emojiWrapper || $('.done-emoji-textarea-wrapper');
      if (!emojiWrapper) return;
      emojiBtn = emojiBtn || emojiWrapper.find('.emojionearea-button');
      var offset = emojiBtn.offset();
      if (!offset) return;
      var top = 35 + offset.top;
      var left = offset.left - 275.5;
      emojiPicker = emojiPicker || emojiWrapper.find('.emojionearea-picker');
      emojiPicker.css({'position': 'fixed', 'top': top + 'px', 'left': left + 'px'});
    }


    function ratingTitle(rating) {
      switch (rating) {
        case 1: return 'Очень плохо';
        case 2: return 'Плохо';
        case 3: return 'Удовлетворительно';
        case 4: return 'Хорошо';
        case 5: return 'Отлично';
        default: return 'default';
      }
    }

  }

}());