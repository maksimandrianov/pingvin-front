/**
 * Created by maks on 03.04.17.
 */

(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('PostNewHeaderStyleController',
      ['$scope', '$timeout', '$log', 'Upload', 'utilService', 'type',
      PostNewHeaderStyleController]);

  function PostNewHeaderStyleController($scope, $timeout, $log, Upload, utilService, type) {
    var self = this;

    self.current     = 'color';
    self.currCol     = undefined;
    self.color       = true;
    self.image       = false;
    self.isLoad      = false;
    self.colors      = 1;
    self.error       = {};
    self.picFile     = undefined;
    self.cropCoords  = undefined;
    self.progress    = 0;
    self.loadimg     = false;
    self.colorSelect = colorSelect;
    self.change      = change;
    self.getNumber   = getNumber;
    self.save        = save;
    self.type        = type;
    self.toast       = utilService.initToast();

    init();

    function init() {
      $scope.$on('ImgCropRegionError', function (ev, data) {
        data = data || {};
        self.error.show = true;
        self.error.msg = data.message || '';
      });
      $timeout(function () {
        self.isLoad = true;
      }, 50);

      $scope.$watch(function () {
        return self.picFile
      }.bind(self), function (newVal) {
        if (newVal)  {
          self.error.show = false;
        }
      });
    }

    function save() {
      if (check()) {
        var style = {};
        if (self.current === 'color') {
          style.b_color = self.currCol;
          $scope.$emit('hStyleIsReady', style);
        }  else if (self.current === 'image') {
          uploadFiles(self.picFile, self.cropCoords)
            .then(function(data) {
              self.progress = 100;
              self.toast('Фотография загружена');
              $timeout(function () {
                self.loadimg = false;
              });
              style.img = data.cropped;
              $scope.$emit('hStyleIsReady', style);
            });
        }
      }
    }

    function uploadFiles(file, coords) {
      self.loadimg = true;
      var size = utilService.realSize($('.crop-area-post'));
      return Upload.upload({
        url: '/upload/',
        data: {
          file: file,
          size: JSON.stringify(size),
          crop: JSON.stringify(coords)
        }
      }).then(function (response) {
        return response.data;
      }, function (response) {
        if (response.status > 0) self.errorMsg = response.status
          + ': ' + response.data;
      }, function (evt) {
        self.progress = parseInt(90.0 * evt.loaded / evt.total);
      });
    }

    function getNumber(num) {
      return new Array(num);
    }

    function change(current) {
      self.color = !self.color;
      self.image = !self.image;
      self.currCol = undefined;
      self.error.show = false;
      $scope.$broadcast('resetImgCropRegion');
    }

    function colorSelect(num) {
      if (angular.isUndefined(self.currCol) && self.error.show) {
        self.error.show = false;
      }
      self.currCol = num;
    }

    function check() {
      if (self.current === 'color') {
        if (!self.currCol) {
          self.error.show = true;
          self.error.msg = 'Вы не выбрали цвет!';
          return false;
        }
      } else if (self.current === 'image') {
        if (!self.picFile || !self.cropCoords) {
          self.error.show = true;
          self.error.msg = 'Файл не выбран!';
          return false;
        }
      } else {
        return false;
      }
      return true;
    }
  }

}());