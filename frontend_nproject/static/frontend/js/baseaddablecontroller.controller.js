/**
 * Created by maks on 24.02.17.
 */


(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('BaseAddableController', ['$scope', '$rootScope', '$log', '$timeout',
      '$controller', '$location', '$routeParams', '$mdDialog', 'utilService', 'authService',
      BaseAddableController
    ]);

  function BaseAddableController($scope, $rootScope, $log, $timeout,
                                 $controller, $location, $routeParams, $mdDialog,
                                 utilService, authService) {
    var self = this;

    var isView;

    angular.extend(self, $controller('BaseListController', {
      $scope: $scope,
      $rootScope: $rootScope,
      $log: $log,
      $timeout: $timeout
    }));

    self.BaseAddableController = BaseAddableController;
    self.params = {};
    self.isOpenDialog = false;
    self.showDetail = showDetail;
    self.showNew = undefined;
    self.removeUnit = utilService.removeUnit;
    $rootScope.addNewButton = false;


    $log = $log.getInstance('BaseAddableController');


    function BaseAddableController(params) {
      $log.log('init BaseAddableController');
      self.BaseListController(params);
      self.params = params;

      isView = params.isView || false;

      if (!isView) {
        $rootScope.$broadcast('reloadCtrl', {state: params.state});
        $rootScope.addNewButton = true;

        $scope.$on('addNewButtonClick', function (e, data) {
          self.showNew();
        });
      }

      $scope.$on('$destroy', function () {
        $rootScope.addNewButton = false;
      });

      $scope.$on('postClose', function (e, el) {
        if (self.removeUnit(el.id, self.collection)) {
          $rootScope.$broadcast('masonry.reload');
        }
      });

      $scope.$on('$routeUpdate', function () {
        if (!self.isOpenDialog) {
          $log.log('$routeUpdate initFromUrl');
          initFromUrl();
        } else if (!$routeParams.hasOwnProperty(params.type) && !$routeParams.hasOwnProperty('new')) {
          $log.log('$routeUpdate dialog cancel');
          $mdDialog.cancel();
        }
      });



      authService.ifAuth(authAfterInit, authAfterInit);

    }

    function authAfterInit() {
      initShowNew();
      initFromUrl();
    }

    function initShowNew() {
      self.showNew = authService.dOnlyAuth(showNew)
    }

    function initFromUrl() {
      if ($routeParams.hasOwnProperty(self.params.type) && $routeParams[self.params.type]) {
        showDetail(null, $routeParams[self.params.type]);
      } else if ($routeParams.hasOwnProperty('new')) {
        self.showNew($routeParams.new);
      }
    }


    function showNew(subDialog) {
      utilService.dialog({
        controller: 'PostNewController',
        templateUrl: '/public/html/post-new.html',
        locals: {
          posts: self.collection,
          subDialog: subDialog,
          data: self.params.data,
          type: self.params.type
        },
        onShowing: function () {
          if (isView)
            return;
          $rootScope.addNewButton = false;
          self.isOpenDialog = true;
          $location.search('new', true);
        },
        onRemoving: function () {
          if (isView)
            return;
          $rootScope.addNewButton = true;
          self.isOpenDialog = false;
          $location.search('new', null);
        }
      });
    }

    function showDetail(ev, id) {
      utilService.dialog({
        controller: 'PostDetailController',
        templateUrl: '/public/html/post-detail.html',
        scope: $scope.$new(),
        locals: {
          option: {
            postId: id,
            type: self.params.type,
            pData: self.params.data
          }
        },
        onShowing: function () {
          if (isView)
            return;
          self.isOpenDialog = true;
          $location.search(self.params.type, id);
        },
        onRemoving: function () {
          if (isView)
            return;
          self.isOpenDialog = false;
          $location.search(self.params.type, null);
        }
      });
    }
  }
}());