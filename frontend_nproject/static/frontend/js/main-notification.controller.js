/**
 * Created by maks on 02.09.17.
 */



(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('MainNotificationController', ['$scope', '$mdDialog', '$timeout',
      '$interval', '$log', '$location', 'dataService', 'utilService', 'authService',
      '$mdMedia', 'DEBUG', MainNotificationController]);


  function MainNotificationController($scope, $mdDialog, $timeout, $interval, $log,
                                      $location, dataService, utilService, authService,
                                      $mdMedia, DEBUG) {

    var self = this;

    var MIN_NOTIFICATION_MS = DEBUG ? 4 * 1000 : 100 * 1000;
    var MAX_NOTIFICATION_MS = DEBUG ? 8 * 1000 : 150 * 1000;
    var NOT_RELEVANT_MS = 1000;


    var statesName =  {
      FEEDBACK: 1,
      HELP: 2,
      PUBLICATIONS: 3
    };

    self.showFeedbackDlg        = showFeedbackDlg;
    self.rating                 = 5;

    self.isShowNotification     = false;
    self.hideNotification       = hideNotification;
    self.content                = undefined;
    self.isShow                 = isShow;
    self.states                 = initStates();
    self.currentState           = 0;

    self.showPromptHelp         = showPromptHelp;
    self.showPromptPublications = showPromptPublications;
    self.skipPromptHelp         = skipPromptHelp;
    self.skipPromptPublications = skipPromptPublications;

    self.name                   = function() { return authService.user ? authService.user.first_name : ''; };

    $log                        = $log.getInstance('MainNotificationController');

    init();

    function init() {
      $scope.$on('simpleFeedbackOk', function () {
        self.isShowNotification = false;
        startChecking();
      });
      if ($mdMedia('gt-xs')) {
        authService.ifAuth(startChecking, reset);
      }
    }

    function isShow() {
      return self.isShowNotification && authService.isLog();
    }

    function getTimeout() {
      return utilService.getRandomInt(MIN_NOTIFICATION_MS, MAX_NOTIFICATION_MS);
    }

    function reset() {
      self.isShowNotification = false;
      self.content            = '';
      self.currentState       = 0;
      self.states             = initStates();
    }

    function callIfAuth(func) {
      if (!authService.isLog()) {
        reset();
        return;
      }
      if (angular.isFunction(func))
        func();
    }

    function startChecking(timeout) {
      var time = angular.isDefined(timeout) ? timeout : getTimeout();
      $log.log('startChecking', time);
      $timeout(checkStates, time);
    }


    function checkFeedback() {
      callIfAuth(function () {
        self.currentState = statesName.FEEDBACK;
        dataService.user.promptFeedback()
          .then(function (data) {
            $log.log('feedback', data);
            if (data && data.hasOwnProperty('has_feedback')) {
              if (!data.has_feedback) {
                self.content = '<div ng-include="\'/public/html/prompt-feedback.html\'"></div>';
                self.isShowNotification = true;
              }
            }
            if (!isShow())
              startChecking(NOT_RELEVANT_MS);
          });
      });
    }

    function checkHelp() {
      callIfAuth(function () {
        self.currentState = statesName.HELP;
        dataService.user.promptHelps()
          .then(function (data) {
            $log.log('help', data);
            if (data && data.hasOwnProperty('has_help')) {
              if (!data.has_help) {
                self.content = '<div ng-include="\'/public/html/prompt-publications.html\'"></div>';
                self.isShowNotification = true;
              }
            }
            if (!isShow())
              startChecking(NOT_RELEVANT_MS);
          });
      });
    }

    function checkPublications() {
      callIfAuth(function () {
        self.currentState = statesName.PUBLICATIONS;
        dataService.user.promptPublications()
          .then(function (data) {
            $log.log('publications', data);
            if (data && data.hasOwnProperty('has_publication')) {
              if (!data.has_publication) {
                self.content = '<div ng-include="\'/public/html/prompt-help.html\'"></div>';
                self.isShowNotification = true;
              }
            }
            if (!isShow())
              startChecking(NOT_RELEVANT_MS);
          });
      });
    }

    function initStates() {
      return _.shuffle([
        checkFeedback,
        checkHelp,
        checkPublications
      ]);
    }

    function checkStates() {
      if (!(authService.isLog() && self.states.length))
        return;
      var func = self.states.pop();
      func();
    }


    function showFeedbackDlg() {
      utilService.dialog({
        controller: 'FeedbackSimpleController',
        templateUrl: '/public/html/feedback-simple-dialog.html',
        locals: {
          rating: self.rating
        }
      });
    }

    function skipPromptHelp() {
      dataService.user.skipPromptHelps()
        .then(function (data) {
          self.isShowNotification = false;
          startChecking();
        });
    }

    function skipPromptPublications() {
      dataService.user.skipPromptPublications()
        .then(function (data) {
          self.isShowNotification = false;
          startChecking();
        });
    }

    function showPromptHelp() {
      $location.path('/blog/kak-otklikatsya-na-obyyavleniya-v-pingvin-29-09-17/');
      dataService.user.closePromptHelps()
        .then(function (data) {
          self.isShowNotification = false;
          startChecking();
        });
    }

    function showPromptPublications() {
      $location.path('/blog/rekomendacii-po-sozdaniyu-obyyavleniy-v-pingvin-29-09-17/');
      dataService.user.closePromptPublications()
        .then(function (data) {
          self.isShowNotification = false;
          startChecking();
        });
    }

    function hideNotification() {
      var func;
      switch (self.currentState) {
        case statesName.FEEDBACK:
          func = dataService.user.closePromptFeedback;
          break;
        case statesName.PUBLICATIONS:
          func = dataService.user.closePromptPublications;
          break;
        case statesName.HELP:
          func = dataService.user.closePromptHelps;
          break;
      }
      if (!angular.isFunction(func))
        return;
      func().then(function (data) {
        self.isShowNotification = false;
        startChecking();
      });
    }
  }

}());