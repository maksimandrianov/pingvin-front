/**
 * Created by maks on 23.02.17.
 */

(function () {
  'use strict';

  angular.module('pingvinApp').component('eventList', {
    templateUrl: '/public/html/post-list-one-col.html',
    controller: ['$scope', '$log', '$rootScope', '$mdDialog',
      '$location', '$controller', '$timeout', '$routeParams', 'conditionService',
      'activityType', 'dataService', 'utilService', 'authService', EventsController]

  });

  function EventsController($scope, $log, $rootScope, $mdDialog, $location,
                            $controller, $timeout, $routeParams, conditionService,
                            activityType, dataService, utilService, authService) {

    var self = this;

    angular.extend(self, $controller('BaseAddableController', {
      $scope: $scope,
      $rootScope: $rootScope,
      $log: $log,
      $timeout: $timeout,
      $controller: $controller,
      $location: $location,
      $routeParams: $routeParams,
      $mdDialog: $mdDialog,
      utilService: utilService,
      authService: authService
    }));

    self.dataEvents      = dataService.events();
    self.hasHeader       = true;
    self.currDescription = false;

    self.msgNoInfo       = 'Пока нет никаких событий';

    $rootScope.addNewButton = true;

    init();

    function init() {
      self.BaseAddableController({
        type: 'event',
        state: 'url-events',
        data: self.dataEvents
      });
    }



  }

}());
