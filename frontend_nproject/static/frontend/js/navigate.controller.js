/**
 * Created by maks on 20.08.16.
 */
(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('NavigateController', ['navigateService', 'authService',
      '$scope', '$location', '$rootScope', '$mdSidenav', '$mdDialog', '$log',
      '$timeout', '$routeParams', '$window', '$mdMedia',
      'dataService', 'utilService',
      NavigateController
    ]);

  function NavigateController(navigateService, authService, $scope, $location,
                              $rootScope, $mdSidenav, $mdDialog, $log, $timeout,
                              $routeParams, $window, $mdMedia,
                              dataService, utilService) {
    var self = this;

    self.answered      = false; // show all content
    self.selected      = undefined;
    self.user          = undefined;
    self.items         = [];
    self.openMenu      = openMenu;
    self.toggleList    = toggleList;
    self.logout        = logout;
    self.activityCount = 0;
    self.currentTab    = undefined;
    self.showTabs      = showTabs;
    self.changeTab     = changeTab;
    self.name          = undefined;
    self.avatar        = undefined;
    self.isActivity    = isActivity;
    self.isSelected    = isSelected;
    self.showSignup    = showSignup;
    self.showForgot    = showForgot;
    self.viewMode      = undefined;
    self.headerName    = headerName;
    self.title         = title;
    self.isShowMItem   = isShowMItem;

    // search
    self.hasSearch    = false;
    self.searchString = "";
    self.openSearch   = openSearch;
    self.closeSearch  = closeSearch;
    self.search       = search;

    // for error handling
    self.hasError           = false;
    $scope.error            = '';
    $scope.description      = '';

    $rootScope.showProgress = true;
    $rootScope.addNewButton = false;
    $scope.$rootScope       = $rootScope;
    $scope.$mdMedia         = $mdMedia;

    self.isOpenDialog       = false;
    self.isOpenLoginDlg     = false;
    self.isOpenSignupDlg    = false;

    self.metas              = $('meta');

    $log = $log.getInstance('NavigateController');


    init();

    function init() {
      authService.ifAuth(function () {
        self.user = authService.user;
        self.name = self.user.first_name;
        self.avatar = utilService.getUMin(self.user);

        var path = $location.path();
        if (path === '/' || path === '') {
          $location.path('/posts/').replace();
          initView();
        }

        dataService.activity()
          .count()
          .then(function (data) {
            self.activityCount = data.count;
          });
        answered();
      }, function () {
        answered();
        initView();
      });

      $scope.$on('$routeUpdate', function () {
        initView();
      });

      authService.ifAuthOnce.then(function () {
      }, function () {
        initFromUrl();
      });

      navigateService
        .loadNavigateItems()
        .then(function (items) {
          var currSelected = getCurrSelected();
          self.items = [].concat(items);
          self.selected = $location.path() === '/' ?
            items[getFirstIndex()] :
            currSelected === -1 ? {name: ''} : items[currSelected];
          $rootScope.$on('reloadCtrl', function (ev, data) {
            $log.log('reload ctrl with', data);
            self.currentTab = 0;
            if (data && (data.state === 'url-about-me-edit' ||
              data.state === 'url-user') ||
              data.state === 'url-login' ||
              data.state === 'url-search' ||
              data.state === 'url-reset-password' ||
              data.state === 'url-blog' ||
              data.state === 'url-info' ||
              data.state === 'url-help' ||
              data.state === 'url-privacy') {
              selectItem(data);
            } else {
              currSelected = getCurrSelected();
              self.selected = $location.path() === '/' ?
                items[getFirstIndex()] :
                currSelected === -1 ? {name: ''} : items[currSelected];
            }
            initTabs();
            setMetaTags(self.selected);
          });
        });

      $scope.$on('$locationChangeStart', function (event, newUrl, oldUrl) {
        self.hasError = false;
        if ($location.path() != '/search/')
          self.hasSearch = false;
        $log.log('$locationChangeStart initTabs');
        initTabs();
        var p = utilService.parseURL(newUrl).path;
        if (authService.isLog()) {
          if (p === '/' || p === '') {
            $location.path('/posts/').replace();
          }
        }
        initView(p);
        initDialogs();
      });

      $scope.$on('error', function (ev, data) {
        $log.log('error handler');
        self.hasError = true;
        $scope.error = data.error || '';
        if (data.hasOwnProperty('description'))
          $scope.description = data.description;
        $rootScope.showProgress = false;
        self.selected = {};
      });

      $scope.$on('noError', function (ev) {
        self.hasError = false;
      });


      $scope.$on('fixCurrentTab', function () {
        $log.log('fixCurrentTab fixCurrentTab');
        fixCurrentTab();
      });

      $scope.$on('newAvatar', function (ev, data) {
        self.avatar = utilService.getUMin(data);
      });

      $scope.$on('newName', function (ev, data) {
        $log.log('$on newName');
        self.name = data;
      });

      $scope.$on('viewModeEnable', function (ev, data) {
        $log.log('$on viewModeEnable');
        self.viewMode = true;
      });

      $scope.$on('viewModeDisable', function (ev, data) {
        $log.log('$on viewModeDisable');
        self.viewMode = false;
      });

      $scope.$on('changeActivityCount', function (ev, data) {
        $log.log('$on changeActivityCount');
        self.activityCount = 0;
      });


      $window.addEventListener('storage',
        function (event) {
          if(event.key === 'pingvin-login') {
            if (parseInt(event.newValue) ^ (+authService.isLog())) {
              $window.location.reload();
            }
          }
        }, false);

    }

    function initDialogs() {
      $timeout(function () {
        if (authService.isLog()) return;
        if (!self.isOpenDialog) {
          $log.log('$routeUpdate initFromUrl');
          initFromUrl();
        } else if (!$routeParams.hasOwnProperty('signup') &&
          !$routeParams.hasOwnProperty('login')) {
          $log.log('$routeUpdate dialog cancel');
          $mdDialog.cancel();
        } else {
          if ((self.isOpenLoginDlg && $routeParams.hasOwnProperty('signup')) ||
            (self.isOpenSignupDlg && $routeParams.hasOwnProperty('login'))) {
            $log.log('$routeUpdate cancel dialog and initFromUrl');
            $mdDialog.cancel();
            initFromUrl();
          }
        }
      }, 50);
    }

    function initView(p) {
      var path = p || $location.path();
      var tmp = path.split('/');
      var path1 = tmp.length > 1 ? tmp[1] : '';
      if (path1 === '') {
        authService.ifAuth(function() {},
          function() {
            self.viewMode = false;
            $location.path('/').replace();
          });
      } else if (path1 === '' ||
        path1 === 'help' ||
        path1 === 'blog' ||
        path1 === 'password' ||
        path1 === 'privacy') {
        self.viewMode = false;
        $timeout(function() {
          closeSidenav();
        }, 50);
      } else {
        self.viewMode = true;
      }
    }

    function headerName() {
      return self.viewMode ? self.selected.name :
        '<a href="/" class="header__logo logo"><h5 class="logo__text-min">Pingvin</h5></a>';
    }

    function title() {
      var defaultTitle = 'Pingvin.online';
      if (self.hasError)
        return defaultTitle + ' — ошибка';
      if (self.selected && self.selected.name)
        return self.selected.name + ' — ' + defaultTitle;

      return defaultTitle + ' — поиск помощника';
    }

    function initFromUrl() {
      $log.log('initFromUrl', $routeParams.hasOwnProperty('login'));
      if ($routeParams.hasOwnProperty('signup')) {
        showSignup();
      } else if ($routeParams.hasOwnProperty('login')) {
        showLogin();
      } else if ($routeParams.hasOwnProperty('forgot')) {
        showForgot();
      }
    }

    function isActivity(item) {
      return self.activityCount && item.name == 'Моя активность';
    }

    function isShowMItem(item) {
      if (item.isa) return authService.isa();
      if (item.reg) return authService.isLog();
      return true;
    }

    function fixCurrentTab() {
      if (!(self.selected &&
        self.selected.hasOwnProperty('param') &&
        self.selected.hasOwnProperty('tabs') &&
        self.selected.tabs.length))
        return false;

      var val = $location.$$search[self.selected.param];
      for (var i = 0; i < self.selected.tabs.length; ++i) {
        if (self.selected.tabs[i].value === val) {
          self.currentTab = i;
          return true;
        }
      }
      return true;
    }

    function initTabs() {
      if (!fixCurrentTab()) return;
      $timeout(function () {
        if (showTabs()) {
          $scope.$broadcast('changeTab', self.currentTab);
          $log.log('$broadcast changeTab', self.currentTab);
        }
      }, 10);
    }

    function changeTab() {
      $log.log('changeTab signal', self.currentTab);
      $scope.$broadcast('changeTab', self.currentTab);
    }

    function showTabs() {
      return !!self.selected &&
        self.selected.hasOwnProperty('tabs') && !!self.selected.tabs.length;
    }

    function answered() {
      self.answered = true;
    }

    function selectItem(item) {
      self.selected = item;
    }

    function isSelected(item) {
      return self.selected && self.selected.hasOwnProperty('url') && self.selected.url === item.url;
    }

    function toggleList() {
      $mdSidenav('left').toggle();
    }

    function closeSidenav() {
      $timeout(function () {
        var left = $mdSidenav('left', true);
        if (left.hasOwnProperty('$$mdHandle') && left.isOpen())
          left.close();

      }, 50);
    }

    function getCurrSelected() {
      var searchIndex,
        path = $location.path();

      searchIndex = _.findIndex(self.items, function (element) {
        return element.hasOwnProperty('url') && element.url === path;
      });

      return searchIndex;
    }

    function getFirstIndex() {
      var searchIndex;
      searchIndex = _.findIndex(self.items, function (element) {
        return element.hasOwnProperty('first') && element.first;
      });

      return searchIndex === -1 ? 0 : searchIndex;
    }

    function logout() {
      authService.logout(function () {
        self.user = authService.user;
      });
    }

    function openSearch() {
      self.searchString = '';
      self.hasSearch = true;
      $timeout(function () {
        angular.element('.search-input').focus();
      }, 50);
    }

    function setMetaTags(selected) {
      var s = selected || {},
        desc = self.metas.filter('[name="description"]');
      if (angular.isDefined(selected.descr)) desc.attr('content', selected.descr);
      else desc.attr('content', 'Pingvin.online поможет найти рядом людей, которым нужна помощь, и людей, готовых помочь.');

      initOgTags(selected);
    }

    function setOg(name, value) {
      self.metas.filter('[property="og:' + name + '"]').attr('content', value);
    }

    function initOgTags(selected) {
      var s = selected || {};

      if (selected.name)    setOg('title', title());
      else                  setOg('title', 'Pingvin.online — поиск помощника');

      if (selected.type)    setOg('type', selected.type);
      else                  setOg('type', 'website');

      if (selected.ogUrl)   setOg('url', selected.ogUrl);
      else                  setOg('url', $window.location.href);

      if (selected.ogImg)   setOg('image', selected.ogImg);
      else                  setOg('image', 'https://pingvin.online/public/img/logo_email.png');

      if (selected.descr)   setOg('description', selected.descr);
      else                  setOg('description', 'Pingvin.online поможет найти рядом людей, которым нужна помощь, и людей, готовых помочь.');

    }

    function closeSearch() {
      self.hasSearch = false;
    }

    function search() {
      $log.log('search', self.searchString);
      if (!self.searchString)
        return;

      if ($location.path() !== '/search/') {
        var context = getSearchContext();
        $location.path('/search/');
        $location.search('context', context);
      }
      $location.search('search', self.searchString);
      closeSearch();
    }

    function getSearchContext() {
      switch ($location.path()) {
        case '/events/': return 2;
        case '/users/': return 3;
        default: return 1;
      }
    }

    function openMenu($mdMenu, ev) {
      $mdMenu.open(ev);
    }

    function showSignup(ev) {
      $log.log('showLogin add login in signup');

      self.isOpenSignupDlg = true;
      self.isOpenDialog = true;

      $mdDialog.show({
        controller: 'SignupController',
        templateUrl: '/public/html/signup.html',
        controllerAs: '$ctrl',
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        fullscreen: true,
        escapeToClose: true,
        disableParentScroll: true,
        scope: $scope.$new(),
        onRemoving: function () {
          self.isOpenSignupDlg = false;
          self.isOpenDialog = false;
          $location.search('signup', null);
        }
      });
    }

    function showForgot(ev) {
      $log.log('showForgot');
      self.isOpenSignupDlg = true;
      self.isOpenDialog = true;

      $mdDialog.show({
        controller: 'SignupController',
        templateUrl: '/public/html/forgot-password.html',
        controllerAs: '$ctrl',
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        fullscreen: true,
        escapeToClose: true,
        disableParentScroll: true,
        scope: $scope.$new(),
        onRemoving: function () {
          self.isOpenSignupDlg = false;
          self.isOpenDialog = false;
          $location.search('forgot', null);
        }
      });
    }

    function showLogin(ev) {
      $log.log('showLogin add login in search');

      self.isOpenDialog = true;
      self.isOpenLoginDlg = true;

      $mdDialog.show({
        controller: 'SignupController',
        templateUrl: '/public/html/login.html',
        controllerAs: '$ctrl',
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        fullscreen: true,
        escapeToClose: true,
        disableParentScroll: true,
        scope: $scope.$new(),
        onRemoving: function () {
          $log.log('showLogin remove login in search');
          self.isOpenLoginDlg = false;
          self.isOpenDialog = false;
          $location.search('login', null);
        }
      });
    }
  }

}());