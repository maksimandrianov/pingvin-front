/**
 * Created by maks on 27.08.17.
 */

(function () {
  'use strict';

  angular.module('pingvinApp').component('privacy', {
    templateUrl: '/public/html/privacy.html',
    controller: ['$rootScope', PrivacyController]
  });


  function PrivacyController($rootScope) {

    var self = this;

    init();

    function init() {
      $rootScope.showProgress = false;
      $rootScope.$broadcast('reloadCtrl', {
        state: 'url-privacy',
        name: 'Пользовательское соглашение',
        descr: 'Пользовательское соглашение Pingvin.online.'
      });
    }

  }
}());