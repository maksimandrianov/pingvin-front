/**
 * Created by maks on 21.08.16.
 */

(function () {
  'use strict';

  angular.module('pingvinApp').component('userList', {
    templateUrl: '/public/html/user-list.html',
    controller: ['$scope', '$rootScope', '$log', '$timeout',
      '$controller', 'utilService', 'dataService', PhoneListController],
    bindings: {
      options: '<'
    }
  });


  function PhoneListController($scope, $rootScope, $log, $timeout,
                               $controller, utilService, dataService) {

    var self          = this;

    angular.extend(self, $controller('BaseListController', {
      $scope: $scope,
      $rootScope: $rootScope,
      $log: $log,
      $timeout: $timeout
    }));

    self.openUser     = utilService.openUser;
    self.fullName     = utilService.fullName;
    self.avatar       = utilService.getUMed;

    self.$onInit = init;

    function init() {
      var options   = self.options || {};
      var dataUsers = options.src || dataService.users();
      var isView    = options.isView || false;
      var msgNoInfo = options.msg || 'Пока нет никаких пользователей';

      self.BaseListController({
        data: dataUsers,
        isView: isView
      });
      if (!isView)
        $rootScope.$broadcast('reloadCtrl', {state: 'url-list'});
    }
  }
}());