/**
 * Created by maks on 26.07.17.
 */


(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('BaseActivityController', ['$scope', '$rootScope', '$log', '$timeout',
      '$controller', '$mdDialog', '$location', 'utilService', 'dataService',
      BaseActivityController
    ]);

  function BaseActivityController($scope, $rootScope, $log, $timeout, $controller,
                                  $mdDialog, $location, utilService, dataService) {
    var self = this;

    angular.extend(self, $controller('BaseListController', {
      $scope: $scope,
      $rootScope: $rootScope,
      $log: $log,
      $timeout: $timeout
    }));

    self.isOpenDialog           = false;
    self.showDetail             = showDetail;
    self.fdate                  = utilService.fdate;
    self.BaseActivityController = BaseActivityController;
    $log                        = $log.getInstance('BaseActivityController');


    function BaseActivityController(params) {
      self.BaseListController({
        data: params.data,
        preShowMoreAsync: params.preShowMoreAsync,
        preShowMore: params.preShowMore || null
      });

      initPostFromUrl();
      $scope.$on('$routeUpdate', function () {
        var urlParams = $location.search();
        if (!self.isOpenDialog) {
          initPostFromUrl();
        } else if (!urlParams.hasOwnProperty('post') &&
          !urlParams.hasOwnProperty('event')) {
          $mdDialog.cancel();
        }
      });
      $scope.$on('postClose', function (e, el) {
        utilService.removeUnit(el.id, self.collection);
      });
    }

    function initPostFromUrl() {
      var urlParams = $location.search();
      if (urlParams.post) {
        showDetail(null, urlParams.post, 0);
      } else if (urlParams.event) {
        showDetail(null, urlParams.event, 1);
      }
    }

    function showDetail(ev, id, t) {
      var type, data;
      if (t == 0) {
        type = 'post';
        data = dataService.posts();
      } else if (t == 1) {
        type = 'event';
        data = dataService.events();
      }
      utilService.dialog({
        controller: 'PostDetailController',
        templateUrl: '/public/html/post-detail.html',
        scope: $scope.$new(),
        locals: {
          option: {
            postId: id,
            type: type,
            pData: data
          }
        },
        onShowing: function () {
          self.isOpenDialog = true;
          $location.search(type, id);
        },
        onRemoving: function () {
          self.isOpenDialog = false;
          $location.search(type, null);
        }
      });
    }

  }
}());