/**
 * Created by maks on 12.06.17.
 */

(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .component('pingvinInfo', {
      templateUrl: '/public/html/pingvin-info.html',
      controller: ['$scope', '$log', '$rootScope', '$timeout', 'utilService',
        PingvinInfo]
    });

  function PingvinInfo($scope, $log, $rootScope, $timeout, utilService) {

    var self = this;

    self.items1 = [
      {
        header: 'Публиковать',
        text: 'события и объявления о помощи',
        img: 'lp1-1.png'
      },

      {
        header: 'Помогать',
        text: 'и откликаться на публикации',
        img: 'lp1-2.png'
      },

      {
        header: 'Искать рядом',
        text: 'людей, которым нужна помощь',
        img: 'lp1-3.png'
      },

      {
        header: 'Делегировать',
        text: 'различные поручения',
        img: 'lp1-4.png'
      },

      {
        header: 'Зарабатывать,',
        text: 'выполняя платные поручения',
        img: 'lp1-5.png'
      },

      {
        header: 'Наслаждаться',
        text: 'результатами и отзывами от людей, которым помогли',
        img: 'lp1-6.png'
      },

      {
        header: 'Быть активным',
        text: 'и участвовать в событиях',
        img: 'lp1-7.png'
      },

      {
        header: 'Экономить',
        text: 'деньги, выбирая выгодные предложения откликов',
        img: 'lp1-8.png'
      },

      {
        header: 'Находить',
        text: 'новых друзей',
        img: 'lp1-9.png'
      }
    ];

    self.items2 = [
      {
        header: 'Работает везде',
        text: 'Pingvin работает в любых городах',
        img: 'lp2-1.png'
      },

      {
        header: 'Отличная поддержка',
        text: 'оперативно ответит на все Ваши вопросы',
        img: 'lp2-2.png'
      },

      {
        header: 'Без комиссии',
        text: 'Сайт не берет комиссии и не показывает рекламу',
        img: 'lp2-3.png'
      },

      {
        header: 'Легкая регистрация',
        text: 'Нужно только имя, почта и пароль или аккаунт в социальной сети',
        img: 'lp2-4.png'
      },

      {
        header: 'Удобное отслеживание',
        text: 'всех статусов помощников и публикаций',
        img: 'lp2-5.png'
      },

      {
        header: 'Почтовые оповещения',
        text: 'Pingvin оповестит Вас о новых откликах помощников',
        img: 'lp2-6.png'
      }
    ];

    $rootScope.showProgress = true;

    $log                     = $log.getInstance('PingvinInfo');

    init();

    function init() {
      $rootScope.$broadcast('reloadCtrl', {
        state: 'url-info',
        name: ''
      });
      $rootScope.showProgress = false;
    }

  }
}());