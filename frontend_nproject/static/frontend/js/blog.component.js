/**
 * Created by maks on 20.07.17.
 */

(function () {
  'use strict';

  angular
    .module('pingvinApp')
    .component('blog', {
      templateUrl: '/public/html/blog.html',
      controller: ['$rootScope', '$scope', '$mdMedia', '$log',
        '$mdDialog', '$routeParams', '$location', '$timeout',
        '$window', '$controller', '$anchorScroll', 'authService', 'dataService',
        'utilService', '$filter', BlogController]
    });

  function BlogController($rootScope, $scope, $mdMedia, $log,
                          $mdDialog, $routeParams, $location, $timeout,
                          $window, $controller, $anchorScroll, authService,
                          dataService, utilService, $filter) {

    var self = this;

    angular.extend(self, $controller('BaseListController', {
      $scope: $scope,
      $rootScope: $rootScope,
      $log: $log,
      $timeout: $timeout
    }));

    var MAX_SHORT_TEXT   = 150;

    init();

    function init() {
      authService.ifAuthOnce.then(function () {
        if (authService.isa()) {
          $rootScope.addNewButton = true;
        }
      });

      $rootScope.$broadcast('reloadCtrl', {
        state: 'url-blog',
        name: 'Блог',
        descr: 'Блог сайта Pingvin.online. Будьте в курсе последних новостей Pingvin.online'
      });
      $rootScope.showProgress = false;

      self.BaseListController({
        data: dataService.blogPosts(),
        isView: true,
        preShowMore: preShowMore
      });

      $scope.$on('$destroy', function () {
        $rootScope.addNewButton = false;
      });

      $scope.$on('addNewButtonClick', function (e, data) {
        showNew();
      });

      $scope.$on('removePostBlog', function (e, data) {
        removeBlogPost(data);
      });
    }

    function showNew() {
      $location.path('/blog/new/');
    }

    function removeBlogPost(post) {
      var confirm = $mdDialog.confirm({
        multiple: true,
        title: 'Удалить пост блога',
        textContent: 'Вы хотите удалить пост ' + post.header + '?',
        ok: 'Удалить',
        cancel: 'Отмена',
        clickOutsideToClose: true,
        fullscreen:          true,
        escapeToClose:       true,
        disableParentScroll: true,
        skipHide:            true
      });

      $mdDialog.show(confirm).then(function () {
        dataService.blogPosts().del(post.link)
          .then(function (data) {
            utilService.removeUnit(post.link, self.collection, 'link');
            $mdDialog.cancel();
            self.toast('Пост блога успешно удален');
          }, function (data) {
            self.toast('Ошибка удаления');
          });
      });
    }

    function preparePostText(post, maxShortText) {
      var isCat = post.stext.length > maxShortText;
      $log.log('+', post.stext);
      post.stext = isCat ?
        $filter('cut')(post.stext, true, maxShortText) :
        $filter('cut')(post.stext, true, Math.round(post.stext.length / 2));
      $log.log('-', post.stext);
      return post;
    }

    function preShowMore(posts) {
      for (var i = 0; i < posts.length; ++i) {
        preparePostText(posts[i], MAX_SHORT_TEXT);
      }
      return posts;
    }
  }
}());
