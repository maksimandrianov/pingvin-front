/**
 * Created by maks on 20.08.16.
 */
(function () {
  'use strict';


  angular.module('pingvinApp')
    .constant('DEBUG', false)
    .constant('_', window._)
    .constant('GOOGLE_API_KEY', 'AIzaSyCY0x5dsaDnRLvtCEyRQRiCVjFpZVKiyYQ');

  angular.module('pingvinApp')
    .config(['$ocLazyLoadProvider', '$qProvider', '$locationProvider', '$routeProvider', '$httpProvider',
      '$mdDateLocaleProvider', '$animateProvider', '$mdThemingProvider', '$provide',
      'uiGmapGoogleMapApiProvider', 'GOOGLE_API_KEY', 'logExProvider', 'DEBUG',
      function ($ocLazyLoadProvider, $qProvider, $locationProvider, $routeProvider, $httpProvider,
                $mdDateLocaleProvider, $animateProvider, $mdThemingProvider, $provide,
                uiGmapGoogleMapApiProvider, GOOGLE_API_KEY, logExProvider, DEBUG) {

        $ocLazyLoadProvider.config({
          debug: true,
          events: true,
          modules: [{
            name: 'textAngular',
            serie: true,
            files: [
              '/public/css/lib/text-angular/text-angular.min.css',
              '/public/js/lib/text-angular/textAngular-rangy.min.js',
              '/public/js/lib/text-angular/textAngular.js',
              '/public/js/lib/text-angular/textAngularSetup.js',
              '/public/js/lib/text-angular/blog-new.app.js'
            ]
          }]
        });

        $routeProvider
          .when('', {
            template: '<pingvin-info></pingvin-info>',
            reloadOnSearch: false
          })
          .when('/', {
            template: '<pingvin-info></pingvin-info>',
            reloadOnSearch: false
          })
          .when('/posts/', {
            template: '<post-list></post-list>',
            reloadOnSearch: false
          })
          .when('/users/', {
            template: '<user-list></user-list>',
            reloadOnSearch: false
          })
          .when('/events/', {
            template: '<event-list></event-list>',
            reloadOnSearch: false
          })
          .when('/about-me/', {
            template: '<user-detail></user-detail>',
            reloadOnSearch: false
          })
          .when('/settings/', {
            template: '<settings></settings>',
            reloadOnSearch: false
          })
          .when('/about-me-edit/', {
            template: '<about-me-edit></about-me-edit>',
            reloadOnSearch: false
          })
          .when('/user/:userId/', {
            template: '<user-detail></user-detail>',
            reloadOnSearch: false
          })
          .when('/activity/', {
            template: '<activity></activity>',
            reloadOnSearch: false
          })
          .when('/feedback/', {
            template: '<feedback></feedback>',
            reloadOnSearch: false
          })
          .when('/search/', {
            template: '<search></search>',
            reloadOnSearch: false
          })
          .when('/help/', {
            template: '<help></help>',
            reloadOnSearch: false
          })
          .when('/blog/', {
            template: '<blog></blog>',
            reloadOnSearch: false
          })
          .when('/blog/new/', {
            templateUrl: '/public/html/blog-new.html',
            resolve: {
              loadTextAngular: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load('textAngular');
              }]
            }
          })
          .when('/blog/:blogId/', {
            template: '<blog-detail></blog-detail>',
            reloadOnSearch: false,
            resolve: {
              loadTextAngular: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load('textAngular');
              }]
            }
          })
          .when('/admin/', {
            template: '<admin></admin>',
            reloadOnSearch: false
          })
          .when('/password/', {
            template: '<reset-password></reset-password>',
            reloadOnSearch: false
          })
          .when('/privacy/', {
            template: '<privacy></privacy>',
            reloadOnSearch: false
          })
          .otherwise('/', {
          });

        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('');

        $qProvider.errorOnUnhandledRejections(false);

        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

        $mdDateLocaleProvider.formatDate = function (date) {
          return date ? moment(date).format('DD.MM.YYYY') : null;
        };
        $mdDateLocaleProvider.parseDate = function (dateString) {
          var m = moment(dateString, 'DD.MM.YYYY', true);
          return m.isValid() ? m.toDate() : new Date(NaN);
        };
        $mdDateLocaleProvider.months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль',
          'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
        $mdDateLocaleProvider.shortMonths = ['Янв', 'Февр', 'Март', 'Апр', 'Май', 'Июнь', 'Июль',
          'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'];
        $mdDateLocaleProvider.days = ['Понедельник', 'Вторник', 'Среда', 'Четверг',
          'Пятница', 'Суббота', 'Воскресенье'];
        $mdDateLocaleProvider.shortDays = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];

        uiGmapGoogleMapApiProvider.configure({
          key: GOOGLE_API_KEY,
          language: 'ru-RU'
        });

        $mdThemingProvider.theme('th1')
          .primaryPalette('indigo')
          .accentPalette('indigo')
          .dark();
        //.warnPalette('indigo')




        logExProvider.enableLogging(DEBUG);

      }]);


  angular.module('pingvinApp')
    .run(['$templateCache', '$http', '$rootScope', '$location', '$log', '$mdDialog', 'authService',
      function ($templateCache, $http, $rootScope, $location, $log, $mdDialog, authService) {
        $log = $log.getInstance('run');

        $http.get('/public/html/like-panel.html', {cache : $templateCache})
          .then(function(data) {
            $log.log('preload like-panel.html');
          });
        $http.get('/public/html/post-list-item.html', {cache : $templateCache})
          .then(function(data) {
            $log.log('preload post-list-item.html');
          });


        $rootScope.$on('$routeChangeStart', function (event, next, current) {
          $log.log('$routeChangeStart');
          authService.ifAuth(function () {
              //var nexUrl = next.$$route.originalPath;
              //  if(nexUrl=== '/plogin/') {
              //    event.preventDefault();
              //  }
            },
            function () {
              //$log.log('authService.redirect');
              //authService.redirect();
            });

          if (angular.equals({}, $location.search())) {
            $mdDialog.cancel();
            $log.log('dialog close');
          }
        });

        $rootScope.$on('$locationChangeSuccess', function () {
          authService.ifAuth(null,
            function () {
              $log.log('authService.redirect');
              authService.redirect();
            });
        })
      }]);


}());

