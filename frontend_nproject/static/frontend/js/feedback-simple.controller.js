/**
 * Created by maks on 04.09.17.
 */


(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('FeedbackSimpleController', ['$scope', '$rootScope', '$mdDialog', '$timeout', '$log',
      'dataService', 'utilService', 'feedbackConst', 'rating',
      FeedbackSimpleController]);


  function FeedbackSimpleController($scope, $rootScope, $mdDialog, $timeout,
                                    $log, dataService, utilService, feedbackConst, rating) {

    var self = this;

    self.sendFeedback = sendFeedback;
    self.message      = '';
    self.rating       = rating || 5;
    self.toast        = utilService.initToast();

    $scope.cancel = function () {
      $mdDialog.cancel();
    };


    function sendFeedback() {
      var data = {
        type: feedbackConst.simple
      };
      var tmp = self.message.trim();
      if (tmp) {
        data.message = tmp;
      }
      data.rating = self.rating;
      dataService.feedback().create
        .post(data).then(function success(data) {
        self.toast('Благодарим, Ваш отзыв отправлен');
        $rootScope.$broadcast('simpleFeedbackOk');
        $mdDialog.cancel();
        if (self.rating >= 4) {
          $timeout(function () {
            utilService.shareDlg(null);
          }, 100);
        }
      }, function fail(response) {
        self.toast(response.code);
      });
    }

  }

}());