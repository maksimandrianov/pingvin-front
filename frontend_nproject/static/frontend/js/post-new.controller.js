/**
 * Created by maks on 21.10.16.
 */

(function () {
  'use strict';

  angular.module('pingvinApp')
    .controller('PostNewController', ['$scope', '$rootScope', '$log',
      '$mdDialog', '$timeout', '$location', 'Upload', 'conditionService', 'categoryService',
      'utilService', 'dataService', 'posts', 'subDialog', 'data', 'type', PostNewController]);

  function PostNewController($scope, $rootScope, $log, $mdDialog, $timeout, $location, Upload,
                             conditionService, categoryService, utilService, dataService,
                             posts, subDialog, data, type) {

    var self = this;

    self.post               = { header: '', text: '' };
    self.dialogTitle        = undefined;
    self.isEvent            = undefined;
    self.toastPosting       = '';
    self.postPosting        = function () {};
    self.getTitle           = categoryService.getTitle;
    self.openMenu           = openMenu;
    self.showMap            = showMap;
    self.showMotivat        = showMotivation;
    self.showCategory       = showCategory;
    self.showDate           = showDate;
    self.showHStyle         = showHStyle;
    self.motivation         = function () { return conditionService.mtoString(self.post.condition); };
    self.addNewPost         = addNewPost;
    self.clearLocation      = clearLocation;
    self.clearCategory      = clearCategory;
    self.clearDate          = clearDate;
    self.clearCondition     = clearCondition;
    self.clearStyle         = clearStyle;
    self.parentDialogIsOpen = false;
    self.toast              = utilService.initToast();
    self.imgPreview         = utilService.getPostPreviewLg;
    $scope.cancel           = function () { $mdDialog.cancel();};

    $log                    = $log.getInstance('PostNewController');

    init();

    function init() {

      if (type === 'post') {
        self.dialogTitle = 'Добавить новое объявление';
        self.isEvent = false;
        self.toastPosting = 'Ваше объявление опубликовано';
        self.postPosting = function (data) {
          posts.unshift(data);
          posts[0].isVisible = true;
          $timeout(function () { $rootScope.$broadcast('masonry.reload');}, 50);
        }
      } else if (type === 'event') {
        self.post.organization = '';
        self.post.req_helpers = 5;
        self.isEvent = true;
        self.dialogTitle = 'Добавить новое событие';
        self.toastPosting = 'Ваше событие отправлено на модерацию'
      } else {
        $log.warn('Post new bad initialize.');
      }

      $timeout(function() {
        if (subDialog === 'location') showMap();
        else if (subDialog === 'motivation') showMotivation();
        else if (subDialog === 'category') showCategory();
        else if (subDialog === 'date-end') showDate();
        else if (subDialog === 'style') showHStyle();
      });

      $scope.$on('locationIsReady', function (ev, data) {
        self.post.location = data;
        self.toast('Геолокация добавлена');
        $mdDialog.cancel();
      });

      $scope.$on('motivationIsReady', function (ev, data) {
        self.post.condition = angular.copy(data);
        conditionService.prepareCondition(self.post.condition);
        self.toast('Мотивация добавлена');
        $mdDialog.cancel();
      });

      $scope.$on('categoryIsReady', function (ev, data) {
        self.post.category = data;
        self.toast('Категория добавлена');
        $mdDialog.cancel();
      });

      $scope.$on('dateIsReady', function (ev, data) {
        self.post.date_end = moment(data).format('YYYY-MM-DD');
        self.toast('Дата окончания добавлена');
        $mdDialog.cancel();
      });

      $scope.$on('hStyleIsReady', function (ev, data) {
        self.post.h_style = data;
        $log.log(self.post.h_style);
        self.toast('Стиль добавлен');
        $mdDialog.cancel();
      });

      $scope.$on('$locationChangeStart', function (ev, newUrl, oldUrl) {
        var newAUrl = utilService.parseURL(newUrl),
          oldAUrl =  utilService.parseURL(oldUrl);

        if (self.parentDialogIsOpen && newAUrl.queryKey.new === '' &&
          (oldAUrl.queryKey.new === 'location' ||
          oldAUrl.queryKey.new === 'motivation' ||
          oldAUrl.queryKey.new === 'category' ||
          oldAUrl.queryKey.new === 'date-end' ||
          oldAUrl.queryKey.new === 'style')) {
          $mdDialog.cancel();
        }

        if (!self.parentDialogIsOpen && oldAUrl.queryKey.new === '') {
          if (newAUrl.queryKey.new === 'location') showMap();
          else if (newAUrl.queryKey.new === 'motivation') showMotivation();
          else if (newAUrl.queryKey.new === 'category') showCategory();
          else if (newAUrl.queryKey.new === 'date-end') showDate();
          else if (newAUrl.queryKey.new === 'style') showHStyle();
        }
      });
    }

    function clearStyle() {
      if (self.post.hasOwnProperty('h_style')) delete self.post.h_style;
    }

    function clearLocation() {
      if (self.post.hasOwnProperty('location')) delete self.post.location;
    }

    function clearCategory() {
      if (self.post.hasOwnProperty('category')) delete self.post.category;
    }

    function clearDate() {
      if (self.post.hasOwnProperty('date_end')) delete self.post.date_end;
    }

    function clearCondition() {
      if (self.post.hasOwnProperty('condition')) delete self.post.condition;
    }

    function showDate(ev) {
      var parentElem = angular.element('.post-new').parent();

      utilService.dialog({
        multiple: true,
        controller: 'PostNewDateController',
        templateUrl: '/public/html/post-new-date.html',
        skipHide: true,
        scope: $scope.$new(),
        onShowing: function () {
          $location.search('new', 'date-end');
          self.parentDialogIsOpen = true;
          parentElem.addClass('second_plan_dialog');
        },
        onRemoving: function () {
          $location.search('new', true);
          self.parentDialogIsOpen = false;
          parentElem.removeClass('second_plan_dialog');
        }
      });
    }

    function showHStyle(ev) {
      var parentElem = angular.element('.post-new').parent();

      utilService.dialog({
        multiple: true,
        controller: 'PostNewHeaderStyleController',
        locals: { type: type },
        templateUrl: '/public/html/post-new-header-style.html',
        skipHide: true,
        scope: $scope.$new(),
        onShowing: function () {
          $location.search('new', 'style');
          self.parentDialogIsOpen = true;
          parentElem.addClass('second_plan_dialog');
        },
        onRemoving: function () {
          $location.search('new', true);
          self.parentDialogIsOpen = false;
          parentElem.removeClass('second_plan_dialog');
        }
      });
    }

    function showCategory(ev) {
      var parentElem = angular.element('.post-new').parent();

      utilService.dialog({
        multiple: true,
        controller: 'PostNewCategoryController',
        templateUrl: '/public/html/post-new-category.html',
        skipHide: true,
        scope: $scope.$new(),
        onShowing: function () {
          $location.search('new', 'category');
          self.parentDialogIsOpen = true;
          parentElem.addClass('second_plan_dialog');
        },
        onRemoving: function () {
          $location.search('new', true);
          self.parentDialogIsOpen = false;
          parentElem.removeClass('second_plan_dialog');
        }
      });
    }

    function showMotivation(ev) {
      var parentElem = angular.element('.post-new').parent();

      utilService.dialog({
        multiple: true,
        controller: 'PostNewMotivationController',
        templateUrl: '/public/html/post-new-motivation.html',
        skipHide: true,
        scope: $scope.$new(),
        onShowing: function () {
          $location.search('new', 'motivation');
          self.parentDialogIsOpen = true;
          parentElem.addClass('second_plan_dialog');
        },
        onRemoving: function () {
          $location.search('new', true);
          self.parentDialogIsOpen = false;
          parentElem.removeClass('second_plan_dialog');
        }
      });
    }

    function showMap(ev) {
      var parentElem = angular.element('.post-new').parent();

      utilService.dialog({
        multiple: true,
        controller: 'NewPostGoogleMapCtrl',
        templateUrl: '/public/html/post-new-map.html',
        controllerAs: 'mctrl',
        skipHide: true,
        scope: $scope.$new(),
        onShowing: function () {
          $location.search('new', 'location');
          self.parentDialogIsOpen = true;
          parentElem.addClass('second_plan_dialog');
        },
        onRemoving: function () {
          $location.search('new', true);
          self.parentDialogIsOpen = false;
          parentElem.removeClass('second_plan_dialog');
        }
      });
    }

    function openMenu($mdOpenMenu, ev) {
      $mdOpenMenu(ev);
    }

    function addNewPost() {
      if (self.post.date_end) self.post.date_end = moment(self.post.date_end).format('YYYY-MM-DD');
      $log.log('add post', self.post);
      data.create
        .post(self.post)
        .then(function (data) {
          self.postPosting(data);
          self.toast(self.toastPosting);
          $mdDialog.cancel();
        }, utilService.dHandleError);
    }
  }
}());
