/**
 * Created by maks on 26.07.17.
 */

(function () {
  'use strict';

  angular.module('pingvinApp').component('activityNews', {
    templateUrl: '/public/html/post-list-one-col.html',
    controller: ['$scope', '$log', '$rootScope', '$mdDialog',
      '$controller', '$timeout', '$location', '$routeParams', 'dataService',
      'utilService',  ActivityNewsController]

  });

  function ActivityNewsController($scope, $log, $rootScope, $mdDialog, $controller,
                                  $timeout, $location, $routeParams, dataService, utilService) {

    var self = this;

    self.hasHeader              = false;
    self.currDescription        = true;
    self.isActivity             = true;
    self.msgNoInfo              = 'Пока нет никаких актуальных обновлений';

    angular.extend(self, $controller('BaseActivityController', {
      $scope: $scope,
      $rootScope: $rootScope,
      $log: $log,
      $timeout: $timeout,
      $controller: $controller,
      $mdDialog: $mdDialog,
      $location: $location,
      utilService: utilService,
      dataService: dataService
    }));


    init();

    function init() {
      var params = {
        data: dataService.activity(),
        preShowMoreAsync: function() {
          $scope.$emit('changeActivityCount');
          $log.log('$emit changeActivityCount');
        },
        preShowMore: function (data) {
          var result = [], currElement;
          var lastPost = self.collection.length ?
            self.collection[self.collection.length-1] : null;
          for (var i = 0; i < data.length; ++i) {
            if (lastPost === null) {
              currElement = data[i];
              currElement.units = [currElement];
              lastPost = currElement;
            } else {
              if (data[i].post.id === lastPost.post.id) {
                lastPost.units.push(data[i]);
              } else {
                if (angular.isDefined(currElement))
                  result.push(currElement);
                currElement = data[i];
                currElement.units = [currElement];
                lastPost = currElement;
              }
            }
          }
          var lastP = result[result.length - 1];
          if (angular.isDefined(currElement) && currElement !== lastP)
            result.push(currElement);

          return result;
        }
      };
      self.BaseActivityController(params);
    }
  }

}());
