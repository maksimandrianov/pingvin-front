/**
 * Created by maks on 08.07.17.
 */

(function () {
  'use strict';

  angular.module('pingvinApp').component('searchMenuUser', {
    templateUrl: '/public/html/search-menu-user.html',
    controller: ['$scope', '$rootScope', '$log', '$timeout', '$location',
      '$mdMenu', 'utilService', 'dataService', SearchMenuUserController]
  });

  function SearchMenuUserController($scope, $rootScope, $log, $timeout, $location,
                                    $mdMenu, utilService, dataService) {

    var self                = this;

    var allCountries = [
      {
        country: 'Россия',
        cities: [
          {city: 'Москва'},
          {city: 'Санкт-Петербург'},
          {city: 'Новосибирск'},
          {city: 'Екатеринбург'},
          {city: 'Нижний Новгород'},
          {city: 'Казань'},
          {city: 'Челябинск'},
          {city: 'Омск'},
          {city: 'Самара'},
          {city: 'Ростов-на-Дону'}
        ]
      },
      {
        country: 'Белоруссия',
        cities: [
          {city: 'Минск'},
          {city: 'Брест'},
          {city: 'Гродно'},
          {city: 'Гомель'},
          {city: 'Витебск'},
          {city: 'Могилёв'},
          {city: 'Бобруйск'},
          {city: 'Барановичи'},
          {city: 'Новополоцк'},
          {city: 'Пинск'}
        ]
      }
    ];

    self.selectedCountry       = undefined;
    self.searchTextCountry     = '';
    self.selectedCity          = undefined;
    self.searchTextCity        = '';
    self.countries             = loadCountries();
    self.cities                = [];
    self.querySearchCity       = querySearchCity;
    self.querySearchCountry    = querySearchCountry;
    self.selectedCountryChange = selectedCountryChange;
    self.searchCountryChange   = searchCountryChange;
    self.searchCityChange      = searchCityChange;
    self.selectedCityChange    = selectedCityChange;
    self.cityDisable           = true;

    $log                        = $log.getInstance('SearchMenuUserController');


    init();

    function init() {
      initFromUrl();
      $scope.$on('$routeUpdate', function () {
        initFromUrl();
      });
    }

    function initFromUrl() {
      var params = $location.search(),
        search;
      $log.log('initFromUrl', params);
      if (params.city && !params.country) {
        $location.search('city', null);
      } else if (params.country) {
        search = querySearchCountry(params.country);
        if (search.length === 1 && search[0].display === params.country) {
          self.selectedCountry = search[0];
          self.searchTextCountry = params.country;
          self.cities = loadCities(search[0].cities);
          self.cityDisable = false;
          if (params.city) {
            search = querySearchCity(params.city);
            if (search.length === 1 && search[0].display === params.city) {
              self.selectedCity = search[0];
              self.searchTextCity = params.city;
            } else {
              $location.search('city', null);
            }
          }
        } else {
          $location.search('country', null);
        }
      }

      if (self.selectedCountry && !params.country) {
        self.cities = [];
        self.searchTextCity = '';
        self.searchTextCountry = '';
        self.cityDisable = true;
        self.selectedCountry = undefined;
        self.selectedCity = undefined;
      }

      if (self.selectedCity && !params.city) {
        self.selectedCity = undefined;
        self.searchTextCity = '';
      }
    }

    function querySearch(list, query) {
      return angular.isUndefined(list) ? [] : list.filter(createFilterFor(query));
    }

    function querySearchCity(query) {
      var res = querySearch(self.cities, query);
      if (!res.length)
        self.searchTextCity = '';
      return res;
    }

    function querySearchCountry(query) {
      var res = querySearch(self.countries, query);
      if (!res.length)
        self.searchTextCountry = '';
      return res;
    }

    function searchCountryChange(text) {
      querySearchCountry(text);
    }

    function selectedCountryChange(item) {
      $log.log('Country changed to ' + JSON.stringify(item));
      if (angular.isUndefined(item)) {
        self.cities = [];
        self.searchTextCity = '';
        self.searchTextCountry = '';
        self.cityDisable = true;
        $location.search('country', null);
        return;
      }
      self.cities = loadCities(item.cities);
      self.cityDisable = false;
      $location.search('country', item.display);
    }

    function searchCityChange(text) {
      querySearchCity(text);
    }

    function selectedCityChange(item) {
      $log.log('City changed to ' + JSON.stringify(item));
      if (angular.isUndefined(item)) {
        self.searchTextCity = '';
        $location.search('city', null);
        return;
      }
      $location.search('city',  item.display);
    }

    function loadCountries() {
      return allCountries.map(function (state) {
        state.value = state.country.toLowerCase();
        state.display = state.country;
        return state;
      });
    }

    function loadCities(arr) {
      return arr.map(function (state) {
        state.value = state.city.toLowerCase();
        state.display = state.city;
        return state;
      });
    }

    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(state) {
        return (state.value.indexOf(lowercaseQuery) === 0);
      };
    }
  }
}());