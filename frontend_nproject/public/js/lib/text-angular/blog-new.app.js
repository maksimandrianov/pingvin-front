/**
 * Created by maks on 24.09.17.
 */

(function () {
  'use strict';

  var module = angular.module("textAngularModule", ["pingvinApp", "textAngular"]);

  module.config(["$provide", function ($provide) {

    $provide.decorator("taTools", [
      "$delegate",
      function(taTools) {
        console.log("taTools");
        taTools.h1.display = '<md-button aria-label="Heading 1">H1</md-button>';
        taTools.h2.display = '<md-button aria-label="Heading 2">H2</md-button>';
        taTools.h3.display = '<md-button aria-label="Heading 3">H3</md-button>';
        taTools.p.display = '<md-button aria-label="Paragraph">P</md-button>';
        taTools.pre.display = '<md-button aria-label="Pre">pre</md-button>';
        taTools.quote.display =
          '<md-button class="md-icon-button" aria-label="Quote"><md-icon md-font-set="material-icons">format_quote</md-icon></md-button>';
        taTools.bold.display =
          '<md-button class="md-icon-button" aria-label="Bold"><md-icon md-font-set="material-icons">format_bold</md-icon></md-button>';
        taTools.italics.display =
          '<md-button class="md-icon-button" aria-label="Italic"><md-icon md-font-set="material-icons">format_italic</md-icon></md-button>';
        taTools.underline.display =
          '<md-button class="md-icon-button" aria-label="Underline"><md-icon md-font-set="material-icons">format_underlined</md-icon></md-button>';
        taTools.ul.display =
          '<md-button class="md-icon-button" aria-label="Buletted list"><md-icon md-font-set="material-icons">format_list_bulleted</md-icon></md-button>';
        taTools.ol.display =
          '<md-button class="md-icon-button" aria-label="Numbered list"><md-icon md-font-set="material-icons">format_list_numbered</md-icon></md-button>';
        taTools.undo.display =
          '<md-button class="md-icon-button" aria-label="Undo"><md-icon md-font-set="material-icons">undo</md-icon></md-button>';
        taTools.redo.display =
          '<md-button class="md-icon-button" aria-label="Redo"><md-icon md-font-set="material-icons">redo</md-icon></md-button>';
        taTools.justifyLeft.display =
          '<md-button class="md-icon-button" aria-label="Align left"><md-icon md-font-set="material-icons">format_align_left</md-icon></md-button>';
        taTools.justifyRight.display =
          '<md-button class="md-icon-button" aria-label="Align right"><md-icon md-font-set="material-icons">format_align_right</md-icon></md-button>';
        taTools.justifyCenter.display =
          '<md-button class="md-icon-button" aria-label="Align center"><md-icon md-font-set="material-icons">format_align_center</md-icon></md-button>';
        taTools.justifyFull.display =
          '<md-button class="md-icon-button" aria-label="Justify"><md-icon md-font-set="material-icons">format_align_justify</md-icon></md-button>';
        taTools.clear.display =
          '<md-button class="md-icon-button" aria-label="Clear formatting"><md-icon md-font-set="material-icons">format_clear</md-icon></md-button>';
        taTools.html.display =
          '<md-button class="md-icon-button" aria-label="Show HTML"><md-icon md-font-set="material-icons">code</md-icon></md-button>';
        taTools.insertLink.display =
          '<md-button class="md-icon-button" aria-label="Insert link"><md-icon md-font-set="material-icons">insert_link</md-icon></md-button>';
        taTools.insertImage.display =
          '<md-button class="md-icon-button" aria-label="Insert photo"><md-icon md-font-set="material-icons">insert_photo</md-icon></md-button>';
        taTools.insertVideo.display =
          '<md-button class="md-icon-button" aria-label="Insert video"><md-icon md-font-set="material-icons">ondemand_video</md-icon></md-button>';

        return taTools;
      }
    ]);

  }]);

  module.controller("TextAngularController", ["$scope", "$rootScope", "$log",
    "$mdDialog", "$location", "$timeout", "utilService", "dataService", "authService",
    TextAngularController]);

  function TextAngularController($scope, $rootScope, $log, $mdDialog,
                                 $location,$timeout, utilService, dataService, authService) {

    var self = this;
    var cancel = function () { $mdDialog.cancel();};

    self.toast              = utilService.initToast();
    self.post               = { header: '', text: '' };
    self.showHStyle         = showHStyle;
    self.addImage           = addImage;
    self.clearStyle         = clearStyle;
    self.imgPreview         = utilService.getBlogPreviewLg;
    self.removeImg          = removeImg;
    self.addNew             = addNew;

    self.imageLinks         = [];

    $scope.cancel           = cancel;

    init();

    function init() {

      $rootScope.showProgress = true;
      authService.ifAuthOnce.then(function () {
        if (authService.isa()) {
          $rootScope.showProgress = false;
          $rootScope.$broadcast('reloadCtrl', {
            state: 'url-blog',
            name: 'Новая статья',
            descr: 'Добавление новой статьи в блог'
          });
        } else {
          $rootScope.$broadcast('error', {error: 403, description: 'Доступ ограничен'});
          $timeout(function () {
            $rootScope.showProgress = false;
          }, 100);
        }
      }, function () {
        $rootScope.$broadcast('error', {error: 403, description: 'Доступ ограничен'});
        $timeout(function () {
          $rootScope.showProgress = false;
        }, 100);
      });


      $scope.$on('hStyleIsReady', function (ev, data) {
        self.post.h_style = data;
        self.toast('Стиль добавлен');
        cancel();
      });

      $scope.$on('imgUploaded', function (ev, data) {
        self.imageLinks.push(data);
        self.toast('Изображение добавлено');
        cancel();
      });

    }

    function showHStyle(ev) {
      utilService.dialog({
        multiple: true,
        controller: 'PostNewHeaderStyleController',
        locals: { type: 'blog' },
        templateUrl: '/public/html/post-new-header-style.html',
        skipHide: true,
        scope: $scope.$new()
      });
    }

    function addImage(ev) {
      utilService.dialog({
        multiple: true,
        controller: 'ImageSimpleController',
        locals: { type: '' },
        templateUrl: '/public/html/img-simple-dialog.html',
        skipHide: true,
        scope: $scope.$new()
      });
    }

    function clearStyle() {
      delete self.post.h_style;
    }

    function removeImg(index) {
      var deletedLink =  self.imageLinks.splice(index, 1);
    }


    function addNew() {
      dataService.blogPosts().create
        .post(self.post)
        .then(function (data) {
          self.toast('Пост успешно опубликован');
          $timeout(function () {
            $location.path('/blog/');
          }, 50);
        }, utilService.dHandleError);
    }

  }
}());

