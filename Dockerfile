FROM python:3.5
ADD requirements.txt /app/requirements.txt
WORKDIR /app/
RUN apt-get update && apt-get install -y \
    build-essential \
    libturbojpeg1-dev
# for ubuntu for libturbojpeg
# RUN ln -s /usr/lib/x86_64-linux-gnu/libturbojpeg.so.0.1.0 /usr/lib/x86_64-linux-gnu/libturbojpeg.so
RUN pip install cffi
RUN pip install -r requirements.txt

