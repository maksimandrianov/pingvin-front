#!/bin/bash

API_URL="https://backend1.pingvin.online"
MY_URL="https://pingvin.online"
HOST='web'

cd frontend_nproject

python server/main.py --port=8000 --host=$HOST --api=$API_URL --my_url=$MY_URL &
python server/main.py --port=8001 --host=$HOST --api=$API_URL --my_url=$MY_URL &
python server/main.py --port=8002 --host=$HOST --api=$API_URL --my_url=$MY_URL &
python server/main.py --port=8003 --host=$HOST --api=$API_URL --my_url=$MY_URL

